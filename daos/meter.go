package daos

import (
	"context"
	"errors"
	"strconv"
	"time"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MeterDAO persists meter data in database
type MeterDAO struct{}

// NewMeterDAO creates a new MeterDAO
func NewMeterDAO() *MeterDAO {
	return &MeterDAO{}
}

// Get reads the meter with the specified ID from the database.
func (dao *MeterDAO) Get(rs app.RequestScope, id int) (*models.MeterDetails, error) {
	var meter = models.MeterDetails{}
	var priceplan = models.PricePlan{}
	err := rs.Tx().Select("meter_details.id", "meter_details.tariff_id", "gateway_id", "meter_number", "meter_short_number", "meter_type",
		"type_name AS meter_type_name", "gateway_name", "plan_tariff_name", "modbus_id", "current_reading", "previous_reading",
		"remaining_units", "last_seen", "balance").
		LeftJoin("gateway_details", dbx.NewExp("gateway_details.id = meter_details.gateway_id")).
		LeftJoin("price_plan_tariff", dbx.NewExp("price_plan_tariff.id = meter_details.tariff_id")).
		LeftJoin("meter_types", dbx.NewExp("meter_types.id = meter_type")).
		Where(dbx.HashExp{"meter_details.id": id}).One(&meter)

	if err != nil {
		return &meter, err
	}

	err = rs.Tx().Select().Where(dbx.HashExp{"tariff_id": meter.TariffID}).One(&priceplan)
	if err != nil {
		return &meter, err
	}

	meter.PricePlan = priceplan

	// calculate amount payable
	units := meter.CurrentReading - meter.PreviousReading
	if meter.MeterType <= 2 {
		meter.AmountPayable, _ = app.CalculatePostPaidElectricCostings(float64(units), &priceplan)
	}
	meter.CurrentConsumption = units
	return &meter, err
}

// GetMeterDetails id by meter number.
func (dao *MeterDAO) GetMeterDetails(rs app.RequestScope, number string) (*models.MeterDetails, error) {
	var id int
	query := rs.Tx().Select("id").From("meter_details").Where(dbx.HashExp{"meter_number": number})
	if err := query.Row(&id); err != nil {
		return nil, err
	}

	return dao.Get(rs, id)
}

// Create saves a new meter record in the database.
// The Meter.Id field will be populated with an automatically generated ID upon successful saving.
func (dao *MeterDAO) Create(rs app.RequestScope, meter *models.MeterDetails) error {
	meter.ID = 0
	meter.AddedBy = rs.UserID()
	meter.CompanyID = rs.CompanyID()
	meter.LastSeen = time.Now()

	// Check if meter number exists
	exists, err := IsMeterExistsByMeterNo(rs, meter.MeterNumber)
	if err != nil {
		return err
	}
	if exists > 0 {
		return errors.New("Meter Number already exist")
	}

	return rs.Tx().Model(meter).Exclude("GatewayName", "TariffName", "MeterTypeName").Insert()
}

// Update saves the changes to an meter in the database.
func (dao *MeterDAO) Update(rs app.RequestScope, id int, meter *models.MeterDetails) error {
	if _, err := dao.Get(rs, id); err != nil {
		return err
	}
	meter.ID = id
	meter.AddedBy = rs.UserID()
	return rs.Tx().Model(meter).Exclude("Id", "GatewayName", "TariffName", "MeterTypeName").Update()
}

// Delete deletes an meter with the specified ID from the database.
func (dao *MeterDAO) Delete(rs app.RequestScope, id int) error {
	meter, err := dao.Get(rs, id)
	if err != nil {
		return err
	}
	return rs.Tx().Model(meter).Delete()
}

// Count returns the number of the meter records in the database.
func (dao *MeterDAO) Count(rs app.RequestScope) (int, error) {
	var count int
	query := rs.Tx().Select("COUNT(*)").From("meter_details")
	if rs.RoleID() > 10001 && rs.CompanyID() > 0 {
		query.Where(dbx.HashExp{"company_id": rs.CompanyID()})
	}
	err := query.Row(&count)
	return count, err
}

// Query retrieves the meter records with the specified offset and limit from the database.
func (dao *MeterDAO) Query(rs app.RequestScope, offset, limit int) ([]models.MeterDetails, error) {
	meters := []models.MeterDetails{}

	query := rs.Tx().Select("meter_details.id", "tariff_id", "gateway_id", "meter_details.company_id AS company_id", "meter_number", "meter_short_number", "meter_type",
		"type_name AS meter_type_name", "modbus_id", "previous_reading", "current_reading", "remaining_units", "last_seen", "meter_details.added_by AS added_by",
		"coalesce(gateway_name, '') AS gateway_name", "coalesce(plan_tariff_name, '') AS plan_tariff_name").From("meter_details").
		LeftJoin("gateway_details", dbx.NewExp("gateway_details.id = meter_details.gateway_id")).
		LeftJoin("price_plan_tariff", dbx.NewExp("price_plan_tariff.id = meter_details.tariff_id")).
		LeftJoin("meter_types", dbx.NewExp("meter_types.id = meter_type"))

	if rs.RoleID() > 10001 && rs.CompanyID() > 0 {
		query.Where(dbx.HashExp{"meter_details.company_id": rs.CompanyID()})
	}
	err := query.OrderBy("id").Offset(int64(offset)).Limit(int64(limit)).All(&meters)
	return meters, err
}

/*
	IsMeterExists check if a meter exists
	check by meter id
*/
func IsMeterExists(rs app.RequestScope, meterid string) (int, error) {
	var exists int
	q := rs.Tx().NewQuery("SELECT EXISTS(SELECT 1 FROM meter_details WHERE id='" + meterid + "' LIMIT 1) AS exist")
	err := q.Row(&exists)
	return exists, err
}

/*
	IsMeterExistsByMeterNo check if a meter exists
	check by meter no
*/
func IsMeterExistsByMeterNo(rs app.RequestScope, meterno string) (int, error) {
	var exists int
	q := rs.Tx().NewQuery("SELECT EXISTS(SELECT 1 FROM meter_details WHERE meter_number='" + meterno + "' LIMIT 1) AS exist")
	err := q.Row(&exists)
	return exists, err
}

// GetMeterLogs  returns the meters with the specified logs.
func (dao *MeterDAO) GetMeterLogs(db *mongo.Database, id, offset, limit int) ([]models.DeviceData, error) {
	//meters := []models.DeviceData{}
	// err := rs.Tx().Select("id", "energy_reading", "remaining_units", "created_on").
	// 	Where(dbx.HashExp{"modbus_id": id}).
	// 	OrderBy("id DESC").Offset(int64(offset)).Limit(int64(limit)).All(&meters)
	// return meters, err

	var datalog []models.DeviceData

	// Get collection
	collection := db.Collection("data_" + strconv.FormatInt(int64(id), 10))
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	app.CreateIndexMongo("data_" + strconv.FormatInt(int64(id), 10))

	findOptions := options.Find()
	// Sort by `price` field descending
	// if order == "asc" {
	// 	findOptions.SetSort(bson.D{{"datetimestamp", 1}})
	// } else {
	findOptions.SetSort(bson.D{{"datetimestamp", -1}})
	// }

	findOptions.SetSkip(int64(offset))
	findOptions.SetLimit(int64(limit))

	filter := bson.D{}
	// if filterfrom > 0 && filterto > 0 {
	// 	filter = bson.D{{"datetimestamp", bson.D{{"$gte", filterfrom}}}, {"datetimestamp", bson.D{{"$lte", filterto}}}}
	// }

	cur, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		return datalog, err
	}
	defer cur.Close(ctx)

	for cur.Next(context.Background()) {
		item := models.DeviceData{}
		err := cur.Decode(&item)
		if err != nil {
			continue
		}
		datalog = append(datalog, item)

		// fmt.Println("Found a document: ", item)

	}
	if err := cur.Err(); err != nil {
		return datalog, err
	}

	return datalog, err
}
