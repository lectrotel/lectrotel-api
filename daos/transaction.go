package daos

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// TransactionDAO persists transaction data in database
type TransactionDAO struct{}

// NewTransactionDAO creates a new TransactionDAO
func NewTransactionDAO() *TransactionDAO {
	return &TransactionDAO{}
}

// Get reads the transaction with the specified ID from the database.
func (dao *TransactionDAO) Get(rs app.RequestScope, id int) (*models.RequestToken, error) {
	var transaction models.RequestToken
	err := rs.Tx().Select().Model(id, &transaction)
	return &transaction, err
}

// CountTokens returns the number of tokens generated records in the database.
func (dao *TransactionDAO) CountTokens(rs app.RequestScope, meterno string) (int, error) {
	var count int
	if meterno == "0" {
		err := rs.Tx().Select("COUNT(*)").From("request_token").Row(&count)
		return count, err
	}
	err := rs.Tx().Select("COUNT(*)").From("request_token").
		LeftJoin("meter_details", dbx.NewExp("meter_details.id = meter_id")).
		Where(dbx.HashExp{"meter_number": meterno}).Row(&count)
	return count, err
}

// GetTokenList retrieves the number of tokens generated records with the specified offset and limit from the database.
func (dao *TransactionDAO) GetTokenList(rs app.RequestScope, offset, limit int, meterno string) ([]models.ListTokens, error) {
	meters := []models.ListTokens{}
	if meterno == "0" {
		err := rs.Tx().Select("request_token.id", "serial_id", "trans_id", "meter_id", "units", "token_type", "amount",
			"total_units_amount", "result_code", "reason", "result", "request_token.created_on", "coalesce(meter_number, '') AS meter_number").From("request_token").
			LeftJoin("meter_details", dbx.NewExp("meter_details.id = meter_id")).
			OrderBy("id DESC").Offset(int64(offset)).Limit(int64(limit)).All(&meters)
		return meters, err
	}
	err := rs.Tx().Select("request_token.id", "serial_id", "trans_id", "meter_id", "units", "token_type", "amount",
		"total_units_amount", "result_code", "reason", "result", "request_token.created_on", "coalesce(meter_number, '') AS meter_number").From("request_token").
		LeftJoin("meter_details", dbx.NewExp("meter_details.id = meter_id")).
		Where(dbx.HashExp{"meter_number": meterno}).
		OrderBy("id DESC").Offset(int64(offset)).Limit(int64(limit)).All(&meters)
	return meters, err
}

// CountPayments returns the number of payment records in the database.
func (dao *TransactionDAO) CountPayments(rs app.RequestScope, meterno string) (int, error) {
	var count int
	query := rs.Tx().Select("COUNT(*)").From("payments")
	meterval, _ := strconv.Atoi(meterno)
	if meterval > 0 {
		query.Where(dbx.HashExp{"meter_id": meterno})
	}

	err := query.Row(&count)
	return count, err
}

// GetPaymentsList retrieves the number of payment records with the specified offset and limit from the database.
func (dao *TransactionDAO) GetPaymentsList(rs app.RequestScope, offset, limit int, meterno string) ([]models.C2BResponse, error) {
	meters := []models.C2BResponse{}

	query := rs.Tx().Select("payments.id", "transaction_type", "trans_id", "trans_time", "trans_amount", "business_short_code",
		"bill_ref_number", "invoice_number", "third_party_trans_id", "msisdn", "status", "result_desc", "payments.company_id AS company_id",
		"payments.added_by AS added_by", "coalesce(meter_number, '') AS meter_number", "coalesce(meter_type, 0) AS meter_type").From("payments").
		LeftJoin("meter_details", dbx.NewExp("meter_details.id = bill_ref_number"))

	meterval, _ := strconv.Atoi(meterno)
	if meterval > 0 {
		query.Where(dbx.HashExp{"meter_id": meterno})
	}

	err := query.OrderBy("id DESC").Offset(int64(offset)).Limit(int64(limit)).All(&meters)
	return meters, err
}

// GetPaymentDetails retrieves the payment details from the database.
func (dao *TransactionDAO) GetPaymentDetails(rs app.RequestScope, transid, meterno string) (models.PaymentDetails, error) {
	details := models.PaymentDetails{}
	// err := rs.Tx().Select("id", "trans_id").From("payments").
	// 	Where(dbx.HashExp{"trans_id": transid}).Limit(1).One(&details)
	query := "SELECT payments.id, payments.trans_id, bill_ref_number AS meter_id, meter_number, meter_short_number, meter_type, trans_amount, coalesce(full_name, '') AS full_name, trans_time, "
	query += " coalesce(phone_no, '') AS phone, payments.status, meter_details.balance"
	query += " FROM `payments` LEFT JOIN meter_details ON (meter_details.meter_number = " + meterno + ")"
	query += " LEFT JOIN customers ON (customers.meter_no = meter_details.meter_number)"
	query += " WHERE payments.trans_id='" + transid + "'"
	err := rs.Tx().NewQuery(query).One(&details)
	return details, err
}

// SaveToken saves a new transaction record in the database.
// The Transaction.ID field will be populated with an automatically generated ID upon successful saving.
func (dao *TransactionDAO) SaveToken(rs app.RequestScope, m *models.RequestToken, t *models.ResponseData) error {
	_, err := app.DBCon.Insert("request_token", dbx.Params{
		"amount":             m.Amount,
		"units":              m.Units,
		"total_units_amount": m.TotalUnitsAmount,
		"created_on":         time.Now().Format("2006-01-02 15:04:05"),
		"meter_id":           m.MeterID,
		"reason":             t.Reason,
		"result":             t.Result,
		"result_code":        t.ResultCode,
		"serial_id":          t.SerialID,
		"token_type":         m.MeterType,
		"trans_id":           m.PaymentID,
	}).Execute()
	return err
}

// SaveTransaction saves a new transaction record in the database.
func (dao *TransactionDAO) SaveTransaction(rs app.RequestScope, m *models.C2BResponse) error {
	m.CompanyID = rs.CompanyID()
	m.AddedBy = rs.UserID()

	_, err := rs.Tx().Insert("payments", dbx.Params{
		"transaction_type":     m.TransactionType,
		"trans_id":             m.TransID,
		"trans_time":           m.TransTime,
		"trans_amount":         m.TransAmount,
		"business_short_code":  m.BusinessShortCode,
		"bill_ref_number":      m.BillRefNumber,
		"invoice_number":       m.InvoiceNumber,
		"org_account_bance":    m.OrgAccountBalance,
		"third_party_trans_id": m.ThirdPartyTransID,
		"msisdn":               m.MSISDN,
		"first_name":           m.FirstName,
		"middle_name":          m.MiddleName,
		"last_name":            m.LastName,
		"company_id":           m.CompanyID,
		"added_by":             m.AddedBy,
	}).Execute()
	return err
}

// IsTransactionExists check if a transaction exists
func (dao *TransactionDAO) IsTransactionExists(rs app.RequestScope, transID, transTime string) (int, error) {
	var exists int
	q := rs.Tx().NewQuery("SELECT EXISTS(SELECT 1 FROM payments WHERE trans_id='" + transID + "' AND trans_time='" + transTime + "' LIMIT 1) AS exist")
	err := q.Row(&exists)
	return exists, err
}

// MpesaSTKCheckout Mpesa STKPush checkout
func (dao *TransactionDAO) MpesaSTKCheckout(rs app.RequestScope, model *models.TransInvoices, c chan models.ProcessTransJobs) error {
	dt := time.Now()
	svc, err := app.New(app.APPKEY, app.APPSECRET, app.SANDBOX)
	if err != nil {
		return err
	}

	res, err := svc.Simulation(models.Express{
		BusinessShortCode: app.SHORTCODE,
		Password:          app.PASSWORD,
		Timestamp:         app.TIMESTAMP,
		TransactionType:   "CustomerPayBillOnline",
		// Amount:            pay.Amount,
		Amount:           "1",
		PartyA:           model.PhoneNumber,
		PartyB:           app.SHORTCODE,
		PhoneNumber:      model.PhoneNumber,
		CallBackURL:      app.CALLBACKURL,
		AccountReference: model.TransID,
		TransactionDesc:  "Payment",
	})

	if err != nil {
		return err
	}

	in := []byte(res)
	var response map[string]string
	json.Unmarshal(in, &response)

	if response["ResponseCode"] != "0" {
		return errors.New("An error has occured")
	}

	model.RequestCheckOutID = response["CheckoutRequestID"]
	model.CompanyID = rs.CompanyID()
	model.AddedBy = rs.UserID()
	_, err = rs.Tx().Insert("payments", dbx.Params{
		"transaction_type":     model.PaymentOption,
		"trans_id":             model.TransID,
		"trans_time":           dt.Format("01-02-2006 15:04:05"),
		"trans_amount":         model.Amount,
		"business_short_code":  app.SHORTCODE,
		"bill_ref_number":      model.MeterID,
		"invoice_number":       model.TransID,
		"org_account_bance":    0,
		"third_party_trans_id": model.RequestCheckOutID,
		"msisdn":               model.PhoneNumber,
		"first_name":           model.PhoneNumber,
		"middle_name":          model.PhoneNumber,
		"last_name":            model.PhoneNumber,
		"meter_id":             model.MeterID,
		"status":               "Pending",
		"result_code":          response["ResponseCode"],
		"result_desc":          "Incomplete Payment",
		"company_id":           model.CompanyID,
		"added_by":             model.AddedBy,
	}).Execute()

	if model.RequestCheckOutID != "" {
		transinvoice := models.NewTransInvoices(model.ID, model.MeterID, rs.CompanyID(), rs.UserID(), model.Amount, model.TransID, model.PaymentOption, model.PhoneNumber, "Electricity Payment", model.RequestCheckOutID, model.MeterType)
		c <- models.ProcessTransJobs{
			ProcessJobs: transinvoice,
		}
	}

	return err

}

// SaveTransactionPayments save water Payments
func (dao *TransactionDAO) SaveTransactionPayments(rs app.RequestScope, model *models.TransInvoices) error {
	dt := time.Now()
	status := "Pending"
	result := "Incomplete Payment"
	if model.PaymentOption == "Cash" {
		status = "Paid"
		result = "The service request is processed successfully."
	}
	if _, err := rs.Tx().Insert("payments", dbx.Params{
		"transaction_type":     model.PaymentOption,
		"trans_id":             model.TransID,
		"meter_id":             model.MeterID,
		"trans_time":           dt.Format("01-02-2006 15:04:05"),
		"trans_amount":         model.Amount,
		"business_short_code":  app.SHORTCODE,
		"bill_ref_number":      model.MeterID,
		"invoice_number":       model.TransID,
		"org_account_bance":    0,
		"third_party_trans_id": model.RequestCheckOutID,
		"msisdn":               model.PhoneNumber,
		"first_name":           model.PhoneNumber,
		"middle_name":          model.PhoneNumber,
		"last_name":            model.PhoneNumber,
		"status":               status,
		"result_code":          0,
		"result_desc":          result,
		"company_id":           model.CompanyID,
		"added_by":             model.AddedBy,
	}).Execute(); err != nil {
		return err
	}

	if model.MeterType == 2 || model.MeterType == 4 {
		amount := fmt.Sprintf("%f", model.Amount)
		q := rs.Tx().NewQuery("UPDATE meter_details SET balance = balance - " + amount)
		_, err := q.Execute()
		if err != nil {
			rs.Tx().Rollback()
			return err
		}
	}

	// if model.RequestCheckOutID != "" {
	// 	mo := models.NewTransInvoices(model.ID, model.MeterID, model.Amount, model.TransID, model.PaymentOption, model.PhoneNumber, model.TransDescription, model.RequestCheckOutID)
	// 	c <- models.ProcessTransJobs{mo}
	// }

	return nil

}

// MpesaCheckoutConfirmation ...
func (dao *TransactionDAO) MpesaCheckoutConfirmation(rs app.RequestScope, checkout chan models.ProcessTransJobs, token chan models.TokenTopupJob) error {
	for {
		clientJob := <-checkout
		<-time.After(30 * time.Second)

		svc, err := app.New(app.APPKEY, app.APPSECRET, app.SANDBOX)
		if err != nil {
			return err
		}

		res, err := svc.TransactionStatus(models.Status{
			BusinessShortCode: app.SHORTCODE,
			Password:          app.PASSWORD,
			Timestamp:         app.TIMESTAMP,
			CheckoutRequestID: clientJob.ProcessJobs.RequestCheckOutID,
		})

		if err != nil {
			return err
		}

		in := []byte(res)
		var response map[string]interface{}
		json.Unmarshal(in, &response)
		if response["errorCode"] == "500.001.1001" {
			return errors.New(response["errorMessage"].(string))
		}

		// update transaction after mpesa feedback
		err = dao.updateMpesaMerchantDetails(rs, clientJob.ProcessJobs, response, token)
		if err != nil {
			return err
		}
	}
}

// PostPaidMpesaCheckoutConfirmation ...
func (dao *TransactionDAO) PostPaidMpesaCheckoutConfirmation(rs app.RequestScope, checkout chan models.ProcessTransJobs) error {
	for {
		clientJob := <-checkout
		<-time.After(30 * time.Second)

		svc, err := app.New(app.APPKEY, app.APPSECRET, app.SANDBOX)
		if err != nil {
			return err
		}

		res, err := svc.TransactionStatus(models.Status{
			BusinessShortCode: app.SHORTCODE,
			Password:          app.PASSWORD,
			Timestamp:         app.TIMESTAMP,
			CheckoutRequestID: clientJob.ProcessJobs.RequestCheckOutID,
		})

		if err != nil {
			return err
		}

		in := []byte(res)
		var response map[string]interface{}
		json.Unmarshal(in, &response)

		// update transaction after mpesa feedback
		err = dao.updatePostPaidMpesaMerchantDetails(rs, clientJob.ProcessJobs, response)
		if err != nil {
			return err
		}
	}
}

// updateMpesaMerchantDetails update transaction after mpesa feedback
func (dao *TransactionDAO) updateMpesaMerchantDetails(rs app.RequestScope, model models.TransInvoices, details map[string]interface{}, t chan models.TokenTopupJob) error {
	status := "Cancelled"
	if details["ResultCode"] == "0" {
		status = "Paid"
	}

	_, err := app.DBCon.Update("payments", dbx.Params{
		"status":              status,
		"merchant_request_id": details["MerchantRequestID"],
		"result_code":         details["ResultCode"],
		"result_desc":         details["ResultDesc"],
	}, dbx.HashExp{"third_party_trans_id": details["CheckoutRequestID"]}).Execute()
	if err != nil {
		return err
	}

	if details["ResultCode"] == "0" {
		// claculate costing
		tokendetails, err := dao.CalculatePrepaidCosting(rs, model.MeterID, model.Amount)
		if err != nil {
			return err
		}

		// get meter no
		mno, _ := dao.GetMeterNumberByID(rs, model.MeterID)
		tokentopup := models.NewTokenTopup(model.TransID, tokendetails.GatewayID, float32(tokendetails.Units), 3, 17, tokendetails.ModBusID, 0, mno)
		t <- models.TokenTopupJob{
			TokenTopup:       tokentopup,
			TransInvoices:    model,
			UnitsTotalAmount: tokendetails.UnitsAmount,
		}
	}

	return err
}

// updatePostPaidMpesaMerchantDetails update transaction after mpesa feedback
func (dao *TransactionDAO) updatePostPaidMpesaMerchantDetails(rs app.RequestScope, model models.TransInvoices, details map[string]interface{}) error {
	status := "Cancelled"
	if details["ResultCode"] == "0" {
		status = "Paid"
	}

	_, err := app.DBCon.Update("payments", dbx.Params{
		"status":              status,
		"merchant_request_id": details["MerchantRequestID"],
		"result_code":         details["ResultCode"],
		"result_desc":         details["ResultDesc"],
	}, dbx.HashExp{"third_party_trans_id": details["CheckoutRequestID"]}).Execute()
	if err != nil {
		return err
	}

	if model.MeterType == 2 || model.MeterType == 4 {
		amount := fmt.Sprintf("%f", model.Amount)
		q := app.DBCon.NewQuery("UPDATE meter_details SET balance = balance - " + amount)
		_, err = q.Execute()
		if err != nil {
			fmt.Printf("error is")
			return err
		}
	}

	return err
}

// CalculatePrepaidCosting ....
func (dao *TransactionDAO) CalculatePrepaidCosting(rs app.RequestScope, meterid int32, amount float64) (tokenDetails models.TokenDetails, err error) {
	// get pricepaln details by meterid
	var pplan models.PricePlan
	err = app.DBCon.Select("price_plan.id", "meter_details.tariff_id", "meter_number", "meter_type", "modbus_id", "gateway_id", "fixed_charge", "energy_charge", "fuel_cost_charge",
		"forex_adj", "erc_levy", "inflation_adj", "warma_levy", "rep_levy", "power_factor_surcharge", "vat").
		From("price_plan").
		LeftJoin("price_plan_tariff", dbx.NewExp("price_plan_tariff.id = price_plan.tariff_id")).
		LeftJoin("meter_details", dbx.NewExp("meter_details.tariff_id = price_plan.tariff_id")).
		Where(dbx.HashExp{"meter_details.id": meterid}).One(&pplan)
	if err != nil {
		return tokenDetails, err
	}

	// get amount per unit
	amountperunit, _ := app.CalculatePrepaidCostings(&pplan)
	token := app.ToFixed((float64(amount) / amountperunit), 2)

	tokenDetails.Units = float32(token)
	// tokenDetails.UnitsAmount = amountperunit * token
	tokenDetails.UnitsAmount = amountperunit
	tokenDetails.MeterNumber = pplan.MeterNumber
	tokenDetails.ModBusID = pplan.ModbusID
	tokenDetails.GatewayID = pplan.GatewayID
	tokenDetails.MeterType = pplan.MeterType
	return tokenDetails, err
}

// CalculateWaterPrepaidCosting ....
func (dao *TransactionDAO) CalculateWaterPrepaidCosting(rs app.RequestScope, meterid int32, amount float64) (tokenDetails models.TokenDetails, err error) {
	// get pricepaln details by meterid
	var pplan models.WaterPricePlan

	err = app.DBCon.Select("price_plan.id", "meter_number", "meter_type", "modbus_id", "meter_details.tariff_id", "fixed_charge", "charge_per_unit", "sewage", "vat").
		From("price_plan").
		LeftJoin("price_plan_tariff", dbx.NewExp("price_plan_tariff.id = price_plan.tariff_id")).
		LeftJoin("meter_details", dbx.NewExp("meter_details.tariff_id = price_plan.tariff_id")).
		Where(dbx.HashExp{"meter_details.id": meterid}).One(&pplan)
	if err != nil {
		return tokenDetails, err
	}

	// get amount and units
	unitsamount, units := app.CalculateWaterCostings(amount, &pplan)
	tokenDetails.Units = units
	tokenDetails.UnitsAmount = unitsamount
	tokenDetails.MeterNumber = pplan.MeterNumber
	tokenDetails.ModBusID = pplan.ModbusID
	return tokenDetails, err
}

// WaterMpesaCheckoutConfirmation ...
func (dao *TransactionDAO) WaterMpesaCheckoutConfirmation(rs app.RequestScope, checkout chan models.ProcessTransJobs, transid chan string) error {
	for {
		clientJob := <-checkout
		<-time.After(30 * time.Second)

		svc, err := app.New(app.APPKEY, app.APPSECRET, app.SANDBOX)
		if err != nil {
			return err
		}

		res, err := svc.TransactionStatus(models.Status{
			BusinessShortCode: app.SHORTCODE,
			Password:          app.PASSWORD,
			Timestamp:         app.TIMESTAMP,
			CheckoutRequestID: clientJob.ProcessJobs.RequestCheckOutID,
		})

		if err != nil {
			return err
		}

		in := []byte(res)
		var response map[string]interface{}
		json.Unmarshal(in, &response)

		// update transaction after mpesa feedback
		err = dao.WaterUpdateMpesaMerchantDetails(rs, clientJob.ProcessJobs, response)
		if err != nil {
			return err
		}

		if response["ResultCode"] == "0" {
			transid <- clientJob.ProcessJobs.TransID
		}

	}

}

// WaterUpdateMpesaMerchantDetails update transaction after mpesa feedback
func (dao *TransactionDAO) WaterUpdateMpesaMerchantDetails(rs app.RequestScope, model models.TransInvoices, details map[string]interface{}) error {
	status := "Cancelled"
	if details["ResultCode"] == "0" {
		status = "Paid"
	}

	_, err := app.DBCon.Update("payments", dbx.Params{
		"status":              status,
		"merchant_request_id": details["MerchantRequestID"],
		"result_code":         details["ResultCode"],
		"result_desc":         details["ResultDesc"],
	}, dbx.HashExp{"third_party_trans_id": details["CheckoutRequestID"]}).Execute()

	return err
}

/*
	GetMeterNumberByID
	get meter number by given meter id.
*/
func (dao *TransactionDAO) GetMeterNumberByID(rs app.RequestScope, meterid int32) (string, error) {
	var mno string
	query := rs.Tx().Select("meter_number").From("meter_details").Where(dbx.HashExp{"id": meterid})
	err := query.Row(&mno)

	return mno, err
}

// SavePublishMessageMQTT ...
func (dao *TransactionDAO) SavePublishMessageMQTT(rs app.RequestScope, m *models.PublishedMessages) error {
	// return rs.Tx().Model(m).Insert()
	_, err := app.DBCon.Insert("published_messages", dbx.Params{
		"byte_length":   m.ByteLength,
		"system_code":   m.SystemCode,
		"message_id":    m.MessageID,
		"modbus_id":     m.ModbusID,
		"gateway_id":    m.GatewayID,
		"meter_number":  m.MeterNumber,
		"recharge_unit": m.RechargeUnit,
		"token_rand_id": m.TokenRandID,
		"checksum":      m.Checksum,
	}).Execute()
	return err
}
