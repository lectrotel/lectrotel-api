package daos

import (
	"strconv"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// GetInvoice get the invoice with the specified ID from the database.
func (dao *TransactionDAO) GetInvoice(rs app.RequestScope, id int) (*models.InvoiceDetails, error) {
	var invoice models.InvoiceDetails
	if err := rs.Tx().Select().From("invoices").Model(id, &invoice); err != nil {
		rs.Tx().Rollback()
		return &invoice, err
	}

	var mdetails models.MeterDetails
	if err := rs.Tx().Select("meter_details.id", "meter_details.tariff_id", "gateway_id", "meter_number", "meter_short_number", "meter_type",
		"gateway_name", "plan_tariff_name", "modbus_id", "current_reading", "previous_reading",
		"remaining_units", "last_seen", "balance").
		LeftJoin("gateway_details", dbx.NewExp("gateway_details.id = meter_details.gateway_id")).
		LeftJoin("price_plan_tariff", dbx.NewExp("price_plan_tariff.id = meter_details.tariff_id")).
		Where(dbx.HashExp{"meter_details.id": invoice.MeterID}).One(&mdetails); err != nil {
		return &invoice, err
	}

	var priceplan models.PricePlan
	rs.Tx().Select().Where(dbx.HashExp{"tariff_id": mdetails.TariffID}).One(&priceplan)

	var inv models.InvoiceItems
	rs.Tx().Select().From("invoice_items").Where(dbx.HashExp{"invoice_id": invoice.ID}).One(&inv)

	invoice.MeterDetails = mdetails
	invoice.InvoiceItems = inv
	invoice.MeterDetails.PricePlan = priceplan

	return &invoice, nil
}

// ListInvoices retrieves the number of tokens generated records with the specified offset and limit from the database.
func (dao *TransactionDAO) ListInvoices(rs app.RequestScope, offset, limit int, meterno string) ([]models.Invoices, error) {
	invoices := []models.Invoices{}
	query := rs.Tx().Select()

	meterval, _ := strconv.Atoi(meterno)
	if meterval > 0 {
		query.Where(dbx.HashExp{"meter_number": meterno})
	}

	err := query.OrderBy("id DESC").Offset(int64(offset)).Limit(int64(limit)).All(&invoices)

	return invoices, err
}

// CountInvoices returns the number of invoices records in the database.
func (dao *TransactionDAO) CountInvoices(rs app.RequestScope, meterno string) (int, error) {
	var count int
	query := rs.Tx().Select("COUNT(*)").From("invoices")

	meterval, _ := strconv.Atoi(meterno)
	if meterval > 0 {
		query.Where(dbx.HashExp{"meter_number": meterno})
	}

	err := query.Row(&count)
	return count, err
}

// AddInvoice save invoice
func (dao *TransactionDAO) AddInvoice(rs app.RequestScope, model *models.Invoices) error {

	err := rs.Tx().Model(model).Exclude().Insert()
	if err != nil {
		rs.Tx().Rollback()
		return err
	}

	// update balance and previous reading
	_, err = rs.Tx().Update("meter_details", dbx.Params{
		"balance":          model.InvoiceAmount + model.PreviousBalance,
		"previous_reading": model.CurrentReading,
	}, dbx.HashExp{"id": model.MeterID}).Execute()
	if err != nil {
		rs.Tx().Rollback()
		return err
	}

	model.InvoiceItems.InvoiceNumber = model.InvoiceNumber
	model.InvoiceItems.InvoiceID = model.ID

	// Add Invoice Items
	err = rs.Tx().Model(&model.InvoiceItems).Exclude().Insert()
	if err != nil {
		rs.Tx().Rollback()
		return err
	}

	return err
}

// AddInvoiceItems save invoice items
func (dao *TransactionDAO) AddInvoiceItems(rs app.RequestScope, model *models.InvoiceItems) error {
	return rs.Tx().Model(model).Exclude().Insert()
}
