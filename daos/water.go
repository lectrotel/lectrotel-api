package daos

import (
	"fmt"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// WaterDAO persists water data in database
type WaterDAO struct{}

// NewWaterDAO creates a new WaterDAO
func NewWaterDAO() *WaterDAO {
	return &WaterDAO{}
}

// CountWaterPricePlan returns the number of the water price plan records in the database.
func (dao *WaterDAO) CountWaterPricePlan(rs app.RequestScope) (int, error) {
	var count int
	err := rs.Tx().Select("COUNT(*)").From("price_plan_water").Row(&count)
	return count, err
}

// QueryWaterPricePlan retrieves the water pricing plan records with the specified offset and limit from the database.
func (dao *WaterDAO) QueryWaterPricePlan(rs app.RequestScope, offset, limit int) ([]models.PricePlanWater, error) {
	m := []models.PricePlanWater{}
	err := rs.Tx().Select("price_plan_water.id", "tariff_id", "plan_tariff_name AS tariff_name", "fixed_charge", "sewage", "regulatory_levy", "charge_per_unit", "vat").
		LeftJoin("price_plan_tariff", dbx.NewExp("price_plan_tariff.id = price_plan_water.tariff_id")).
		OrderBy("price_plan_water.id").Offset(int64(offset)).Limit(int64(limit)).All(&m)
	return m, err
}

// UpdateWaterPricePlan update water pricing tariff record in the database.
func (dao *WaterDAO) UpdateWaterPricePlan(rs app.RequestScope, m *models.PricePlanWater) (*models.PricePlanWater, error) {
	fmt.Println(m.ID)
	err := rs.Tx().Model(m).Exclude("TariffName").Update()
	return m, err
}
