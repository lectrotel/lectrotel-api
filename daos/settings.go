package daos

import (
	dbx "github.com/go-ozzo/ozzo-dbx"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// SettingDAO persists setting data in database
type SettingDAO struct{}

// NewSettingDAO creates a new SettingDAO
func NewSettingDAO() *SettingDAO {
	return &SettingDAO{}
}

// GetPricingTariff reads the pricing tariff with the specified ID from the database.
func (dao *SettingDAO) GetPricingTariff(rs app.RequestScope, id int) (*models.PricingTariff, error) {
	var m models.PricingTariff
	err := rs.Tx().Select().Model(id, &m)
	return &m, err
}

// CreatePricingTariff saves a new pricing tariff record in the database.
func (dao *SettingDAO) CreatePricingTariff(rs app.RequestScope, m *models.PricingTariff) (*models.PricingTariff, error) {
	m.ID = 0
	_, err := rs.Tx().Insert("price_plan_tariff", dbx.Params{
		"plan_tariff_name":        m.TariffName,
		"plan_tariff_description": m.TariffDescription}).Execute()
	return m, err
}

// CountPricingTariff returns the number of the pricing tariff records in the database.
func (dao *SettingDAO) CountPricingTariff(rs app.RequestScope) (int, error) {
	var count int
	err := rs.Tx().Select("COUNT(*)").From("price_plan_tariff").Where(dbx.HashExp{"plan_status": 1}).Row(&count)
	return count, err
}

// QueryPricingTariff retrieves the pricing tariff records with the specified offset and limit from the database.
func (dao *SettingDAO) QueryPricingTariff(rs app.RequestScope, offset, limit int) ([]models.PricingTariff, error) {
	settings := []models.PricingTariff{}
	err := rs.Tx().Select().From("price_plan_tariff").OrderBy("id").
		Where(dbx.HashExp{"plan_status": 1}).
		Offset(int64(offset)).Limit(int64(limit)).All(&settings)
	return settings, err
}

// CreatePricePlan saves a new pricing tariff record in the database.
func (dao *SettingDAO) CreatePricePlan(rs app.RequestScope, m *models.PricePlan) (*models.PricePlan, error) {
	q, err := rs.Tx().Insert("price_plan_tariff", dbx.Params{
		"plan_tariff_name":        m.TariffName,
		"plan_tariff_description": m.TariffName,
		"plan_tariff_type":        m.PalnTariffType}).Execute()

	lastid, _ := q.LastInsertId()
	m.TariffID = int(lastid)

	err = rs.Tx().Model(m).Exclude("TariffName", "PalnTariffType", "GatewayID", "MeterNumber", "ModbusID", "MeterType").Insert()
	return m, err
}

// UpdatePricePlan update elec pricing tariff record in the database.
func (dao *SettingDAO) UpdatePricePlan(rs app.RequestScope, m *models.PricePlan) (*models.PricePlan, error) {
	err := rs.Tx().Model(m).Exclude("TariffName", "PalnTariffType", "GatewayID", "MeterNumber", "ModbusID", "MeterType").Update()
	return m, err
}

// CountPricePlan returns the number of the pricing plan records in the database.
func (dao *SettingDAO) CountPricePlan(rs app.RequestScope) (int, error) {
	var count int
	err := rs.Tx().Select("COUNT(*)").From("price_plan").Row(&count)
	return count, err
}

// QueryPricePlan retrieves the pricing plan records with the specified offset and limit from the database.
func (dao *SettingDAO) QueryPricePlan(rs app.RequestScope, offset, limit, id int) ([]models.PricePlan, error) {
	m := []models.PricePlan{}
	q := rs.Tx().Select("price_plan.id", "tariff_id", "plan_tariff_type", "plan_tariff_name AS tariff_name", "fixed_charge", "energy_charge", "fuel_cost_charge",
		"forex_adj", "erc_levy", "inflation_adj", "warma_levy", "rep_levy", "power_factor_surcharge", "vat", "charge_per_unit", "sewage", "regulatory_levy").
		LeftJoin("price_plan_tariff", dbx.NewExp("price_plan_tariff.id = price_plan.tariff_id"))
	if id > 0 {
		q.Where(dbx.HashExp{"price_plan.id": id})
	}
	err := q.OrderBy("price_plan.id").Offset(int64(offset)).Limit(int64(limit)).All(&m)
	return m, err
}
