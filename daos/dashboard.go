package daos

import (
	dbx "github.com/go-ozzo/ozzo-dbx"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// DashboardDAO persists dashboard data in database
type DashboardDAO struct{}

// NewDashboardDAO creates a new DashboardDAO
func NewDashboardDAO() *DashboardDAO {
	return &DashboardDAO{}
}

// GetUserDashboardStats ...
func (dao *DashboardDAO) GetUserDashboardStats(rs app.RequestScope) (*models.UserDashboardDetails, error) {
	var u models.UserDashboardDetails
	query1 := rs.Tx().Select("COUNT(*)").From("meter_details")
	if rs.RoleID() > 10001 && rs.CompanyID() > 0 {
		query1.Where(dbx.HashExp{"company_id": rs.CompanyID()})
	}
	query1.Row(&u.NumberOfMeters)
	query2 := rs.Tx().Select("COUNT(*)").From("gateway_details")
	if rs.RoleID() > 10001 && rs.CompanyID() > 0 {
		query2.Where(dbx.HashExp{"company_id": rs.CompanyID()})
	}
	query2.Row(&u.NumberOfGateways)
	return &u, nil
}
