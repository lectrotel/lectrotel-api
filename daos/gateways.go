package daos

import (
	dbx "github.com/go-ozzo/ozzo-dbx"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// GatewayDAO persists gateway data in database
type GatewayDAO struct{}

// NewGatewayDAO creates a new GatewayDAO
func NewGatewayDAO() *GatewayDAO {
	return &GatewayDAO{}
}

// Get reads the gateway with the specified ID from the database.
func (dao *GatewayDAO) Get(rs app.RequestScope, id int) (*models.GatewayDetails, error) {
	var gateway models.GatewayDetails
	err := rs.Tx().Select().Model(id, &gateway)
	return &gateway, err
}

// Create saves a new gateway record in the database.
// The Gateway.Id field will be populated with an automatically generated ID upon successful saving.
func (dao *GatewayDAO) Create(rs app.RequestScope, gateway *models.GatewayDetails) error {
	gateway.ID = 0
	gateway.AddedBy = rs.UserID()
	gateway.CompanyID = rs.CompanyID()
	return rs.Tx().Model(gateway).Exclude("SectionName").Insert()
}

// Update saves the changes to an gateway in the database.
func (dao *GatewayDAO) Update(rs app.RequestScope, id int, gateway *models.GatewayDetails) error {
	if _, err := dao.Get(rs, id); err != nil {
		return err
	}
	gateway.ID = id
	gateway.AddedBy = rs.UserID()
	return rs.Tx().Model(gateway).Exclude("Id").Update()
}

// Delete deletes an gateway with the specified ID from the database.
func (dao *GatewayDAO) Delete(rs app.RequestScope, id int) error {
	gateway, err := dao.Get(rs, id)
	if err != nil {
		return err
	}
	return rs.Tx().Model(gateway).Delete()
}

// Count returns the number of the gateway records in the database.
func (dao *GatewayDAO) Count(rs app.RequestScope) (int, error) {
	var count int
	query := rs.Tx().Select("COUNT(*)").From("gateway_details")
	if rs.RoleID() > 10001 && rs.CompanyID() > 0 {
		query.Where(dbx.HashExp{"company_id": rs.CompanyID()})
	}
	err := query.Row(&count)
	return count, err
}

// Query retrieves the gateway records with the specified offset and limit from the database.
func (dao *GatewayDAO) Query(rs app.RequestScope, offset, limit int) ([]models.GatewayDetails, error) {
	gateways := []models.GatewayDetails{}
	query := rs.Tx().Select("gateway_details.id AS id", "section_id", "gateway_details.company_id AS company_id", "gateway_name", "gateway_description", "gateway_details.added_by AS added_by", "coalesce(section_name, '') AS section_name").
		From("gateway_details").
		LeftJoin("section_details", dbx.NewExp("section_details.id = gateway_details.section_id"))

	if rs.RoleID() > 10001 && rs.CompanyID() > 0 {
		query.Where(dbx.HashExp{"gateway_details.company_id": rs.CompanyID()})
	}

	err := query.OrderBy("gateway_details.id ASC").Offset(int64(offset)).Limit(int64(limit)).All(&gateways)
	return gateways, err
}

// IsGatewayExists check if a gateway exists
func IsGatewayExists(rs app.RequestScope, gatewayid string) (int, error) {
	var exists int
	q := rs.Tx().NewQuery("SELECT EXISTS(SELECT 1 FROM gateway_details WHERE id='" + gatewayid + "' LIMIT 1) AS exist")
	err := q.Row(&exists)
	return exists, err
}
