package daos

import (
	"strconv"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// PermissionDAO persists permission data in database
type PermissionDAO struct{}

// NewPermissionDAO creates a new PermissionDAO
func NewPermissionDAO() *PermissionDAO {
	return &PermissionDAO{}
}

// QueryRoles retrieves the roles records from the database.
func (dao *PermissionDAO) QueryRoles(rs app.RequestScope, roleid uint32) ([]models.Roles, error) {
	roles := []models.Roles{}
	err := rs.Tx().Select().Where(dbx.NewExp("role_id>=" + strconv.Itoa(int(roleid)))).All(&roles)
	return roles, err
}
