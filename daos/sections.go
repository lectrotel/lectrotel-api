package daos

import (
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// SectionDAO persists section data in database
type SectionDAO struct{}

// NewSectionDAO creates a new SectionDAO
func NewSectionDAO() *SectionDAO {
	return &SectionDAO{}
}

// Get reads the section with the specified ID from the database.
func (dao *SectionDAO) Get(rs app.RequestScope, id int) (*models.SectionDetails, error) {
	var section models.SectionDetails
	err := rs.Tx().Select().Model(id, &section)
	return &section, err
}

// Create saves a new section record in the database.
// The Section.Id field will be populated with an automatically generated ID upon successful saving.
func (dao *SectionDAO) Create(rs app.RequestScope, section *models.SectionDetails) error {
	section.ID = 0
	return rs.Tx().Model(section).Insert()
}

// Update saves the changes to an section in the database.
func (dao *SectionDAO) Update(rs app.RequestScope, id int, section *models.SectionDetails) error {
	if _, err := dao.Get(rs, id); err != nil {
		return err
	}
	section.ID = id
	return rs.Tx().Model(section).Exclude("Id").Update()
}

// Delete deletes an section with the specified ID from the database.
func (dao *SectionDAO) Delete(rs app.RequestScope, id int) error {
	section, err := dao.Get(rs, id)
	if err != nil {
		return err
	}
	return rs.Tx().Model(section).Delete()
}

// Count returns the number of the section records in the database.
func (dao *SectionDAO) Count(rs app.RequestScope) (int, error) {
	var count int
	err := rs.Tx().Select("COUNT(*)").From("section_details").Row(&count)
	return count, err
}

// Query retrieves the section records with the specified offset and limit from the database.
func (dao *SectionDAO) Query(rs app.RequestScope, offset, limit int) ([]models.SectionDetails, error) {
	sections := []models.SectionDetails{}
	err := rs.Tx().Select().OrderBy("id").Offset(int64(offset)).Limit(int64(limit)).All(&sections)
	return sections, err
}

// IsSectionExists check if a section exists
func IsSectionExists(rs app.RequestScope, sectionid string) (int, error) {
	var exists int
	q := rs.Tx().NewQuery("SELECT EXISTS(SELECT 1 FROM section_details WHERE section_number='" + sectionid + "' LIMIT 1) AS exist")
	err := q.Row(&exists)
	return exists, err
}
