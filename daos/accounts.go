package daos

import (
	dbx "github.com/go-ozzo/ozzo-dbx"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// AccountDAO persists account data in database
type AccountDAO struct{}

// NewAccountDAO creates a new AccountDAO
func NewAccountDAO() *AccountDAO {
	return &AccountDAO{}
}

// Get reads the account with the specified ID from the database.
func (dao *AccountDAO) Get(rs app.RequestScope, id int) (*models.AccountDetails, error) {
	var account models.AccountDetails
	err := rs.Tx().Select().Model(id, &account)
	return &account, err
}

// Create saves a new account record in the database.
// The Account.Id field will be populated with an automatically generated ID upon successful saving.
func (dao *AccountDAO) Create(rs app.RequestScope, account *models.AccountDetails) error {
	account.ID = 0
	return rs.Tx().Model(account).Exclude("GatewayName").Insert()
}

// Update saves the changes to an account in the database.
func (dao *AccountDAO) Update(rs app.RequestScope, id int, account *models.AccountDetails) error {
	if _, err := dao.Get(rs, id); err != nil {
		return err
	}
	account.ID = id
	return rs.Tx().Model(account).Exclude("Id").Update()
}

// Delete deletes an account with the specified ID from the database.
func (dao *AccountDAO) Delete(rs app.RequestScope, id int) error {
	account, err := dao.Get(rs, id)
	if err != nil {
		return err
	}
	return rs.Tx().Model(account).Delete()
}

// Count returns the number of the account records in the database.
func (dao *AccountDAO) Count(rs app.RequestScope) (int, error) {
	var count int
	err := rs.Tx().Select("COUNT(*)").From("account_details").Row(&count)
	return count, err
}

// Query retrieves the account records with the specified offset and limit from the database.
func (dao *AccountDAO) Query(rs app.RequestScope, offset, limit int) ([]models.AccountDetails, error) {
	accounts := []models.AccountDetails{}
	err := rs.Tx().Select("account_details.id", "tariff_id", "gateway_id", "account_number", "account_short_number",
		"coalesce(gateway_name, '') AS gateway_name", "coalesce(plan_tariff_name, '') AS plan_tariff_name").From("account_details").
		LeftJoin("gateway_details", dbx.NewExp("gateway_details.id = account_details.gateway_id")).
		LeftJoin("price_plan_tariff", dbx.NewExp("price_plan_tariff.id = account_details.tariff_id")).
		OrderBy("id").Offset(int64(offset)).Limit(int64(limit)).All(&accounts)
	return accounts, err
}

// IsAccountExists check if a account exists
func IsAccountExists(rs app.RequestScope, accountid string) (int, error) {
	var exists int
	q := rs.Tx().NewQuery("SELECT EXISTS(SELECT 1 FROM account_details WHERE id='" + accountid + "' LIMIT 1) AS exist")
	err := q.Row(&exists)
	return exists, err
}
