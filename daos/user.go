package daos

import (
	dbx "github.com/go-ozzo/ozzo-dbx"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// UserDAO persists user data in database
type UserDAO struct{}

// NewUserDAO creates a new UserDAO
func NewUserDAO() *UserDAO {
	return &UserDAO{}
}

// GetUser reads the full user details with the specified ID from the database.
func (dao *UserDAO) GetUser(rs app.RequestScope, id uint64) (*models.UserDetails, error) {
	usr := &models.UserDetails{}
	err := rs.Tx().
		Select("user_id", "u.role_id", "role_name", "password", "salt", "company_id", "company_name", "first_name", "last_name", "mobile_number", "email", "username", "is_verified", "is_prepaid_verified",
			"is_postpaid_verified", "created_on", "is_postpaid_water_verified", "is_prepaid_water_verified").
		LeftJoin("roles", dbx.NewExp("roles.role_id = u.role_id")).
		LeftJoin("companies", dbx.NewExp("companies.id = u.company_id")).
		From("user_details").Model(id, &usr)
	if err != nil {
		return nil, err
	}

	return usr, err
}

// // GetUserByEmail reads the user with the specified email from the database.
// func (dao *UserDAO) GetUserByEmail(rs app.RequestScope, email string) (*models.UserDetails, error) {
// 	var usr models.UserDetails
// 	err := rs.Tx().Select("first_name", "last_name", "user_id", "mobile_number", "email", "password", "username", "is_verified", "salt").
// 		Where(dbx.HashExp{"email": email}).One(&usr)

// 	return &usr, err
// }

// GetUserByEmail reads the user with the specified email from the database.
func (dao *UserDAO) GetUserByEmail(rs app.RequestScope, email string) (*models.UserDetails, error) {
	usr := &models.UserDetails{}
	err := rs.Tx().
		Select("user_id", "u.role_id", "role_name", "password", "salt", "company_id", "company_name", "first_name", "last_name", "mobile_number", "email", "username", "is_verified", "is_prepaid_verified",
			"is_postpaid_verified", "created_on", "is_postpaid_water_verified", "is_prepaid_water_verified").
		LeftJoin("roles", dbx.NewExp("roles.role_id = u.role_id")).
		LeftJoin("companies", dbx.NewExp("companies.id = u.company_id")).
		From("user_details AS u").Where(dbx.HashExp{"email": email}).One(&usr)

	if err != nil {
		return nil, err
	}

	return usr, err
}

// Register saves a new user record in the database.
// The User.ID field will be populated with an automatically generated ID upon successful saving.
func (dao *UserDAO) Register(rs app.RequestScope, usr *models.UserDetails) error {
	return rs.Tx().Model(usr).Insert("UserID", "CompanyID", "RoleID", "FirstName", "LastName", "Email", "Password", "Username", "DOB", "MobileNumber", "Salt", "VerificationCode")
}

// SubmitUserRole submit user role
func (dao *UserDAO) SubmitUserRole(rs app.RequestScope, ur *models.UserRoles) error {
	if ur.RoleID == 0 {
		ur.RoleID = 10001
	}

	return rs.Tx().Model(ur).Exclude().Insert()
}

// Count returns the number of user records in the database.
func (dao *UserDAO) Count(rs app.RequestScope) (int, error) {
	var count int
	err := rs.Tx().Select("COUNT(*)").From("user_details").Row(&count)
	return count, err
}

// Query retrieves the meter records with the specified offset and limit from the database.
func (dao *UserDAO) Query(rs app.RequestScope, offset, limit int) ([]models.UserDetails, error) {
	users := []models.UserDetails{}
	err := rs.Tx().
		Select("user_id", "u.role_id", "role_name", "company_id", "company_name", "first_name", "last_name", "mobile_number", "email", "username", "is_verified", "created_on").
		LeftJoin("roles", dbx.NewExp("roles.role_id = u.role_id")).
		LeftJoin("companies", dbx.NewExp("companies.id = u.company_id")).
		From("user_details AS u").All(&users)
	return users, err
}

// Delete deletes user with the specified ID from the database.
func (dao *UserDAO) Delete(rs app.RequestScope, id uint64) error {
	_, err := rs.Tx().Delete("user_details", dbx.HashExp{"user_id": id}).Execute()
	return err
}

// CreateNewEmailVerification - Create a new user
func (dao *UserDAO) CreateNewEmailVerification(rs app.RequestScope, con *models.ConfirmationEmailDetails) error {

	if err := con.VerifyConfirmationEmail(); err != nil {
		return err
	}

	_, err := rs.Tx().Update("user_details", dbx.Params{
		"verification_code": con.VerificationCode},
		dbx.HashExp{"user_id": con.UserID}).Execute()

	return err
}

// CreateLoginSession creates a new one-time-use login token
func (dao *UserDAO) CreateLoginSession(rs app.RequestScope, ls *models.UserLoginSessions) error {
	return rs.Tx().Model(ls).Exclude().Insert()
}
