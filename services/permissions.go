package services

import (
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// permissionDAO specifies the interface of the permission DAO needed by PermissionService.
type permissionDAO interface {
	QueryRoles(rs app.RequestScope, roleid uint32) ([]models.Roles, error)
}

// PermissionService provides services related with permissions.
type PermissionService struct {
	dao permissionDAO
}

// NewPermissionService creates a new PermissionService with the given permission DAO.
func NewPermissionService(dao permissionDAO) *PermissionService {
	return &PermissionService{dao}
}

// QueryRoles returns the permissions with the specified offset and limit.
func (s *PermissionService) QueryRoles(rs app.RequestScope, roleid uint32) ([]models.Roles, error) {
	return s.dao.QueryRoles(rs, roleid)
}
