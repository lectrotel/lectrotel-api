package services

import (
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// GetInvoice returns the invoice with the specified ID.
func (s *TransactionService) GetInvoice(rs app.RequestScope, id int) (*models.InvoiceDetails, error) {
	return s.dao.GetInvoice(rs, id)
}

// ListInvoices returns the number of  invoices with the specified offset and limit.
func (s *TransactionService) ListInvoices(rs app.RequestScope, offset, limit int, meterno string) ([]models.Invoices, error) {
	return s.dao.ListInvoices(rs, offset, limit, meterno)
}

// CountInvoices returns the number of invoices.
func (s *TransactionService) CountInvoices(rs app.RequestScope, meterno string) (int, error) {
	return s.dao.CountInvoices(rs, meterno)
}

// AddInvoice ...
func (s *TransactionService) AddInvoice(rs app.RequestScope, model *models.Invoices) error {
	model.Prepare()
	model.InvoiceItems.Prepare()

	if err := model.Validate(); err != nil {
		return err
	}

	// if err := model.InvoiceItems.Validate(); err != nil {
	// 	return err
	// }

	// if exists == 1 {
	// 	return errors.New("Transaction Exists")
	// }

	if err := s.dao.AddInvoice(rs, model); err != nil {
		return err
	}

	return nil
}
