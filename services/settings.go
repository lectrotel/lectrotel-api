package services

import (
	"fmt"

	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// settingDAO specifies the interface of the setting DAO needed by SettingService.
type settingDAO interface {
	// Count returns the number of settings.
	CountPricingTariff(rs app.RequestScope) (int, error)
	// Query returns the list of settings with the given offset and limit.
	QueryPricingTariff(rs app.RequestScope, offset, limit int) ([]models.PricingTariff, error)
	// Create saves a new setting in the storage.
	CreatePricingTariff(rs app.RequestScope, setting *models.PricingTariff) (*models.PricingTariff, error)

	QueryPricePlan(rs app.RequestScope, offset, limit, id int) ([]models.PricePlan, error)
	CountPricePlan(rs app.RequestScope) (int, error)
	CreatePricePlan(rs app.RequestScope, model *models.PricePlan) (*models.PricePlan, error)
	UpdatePricePlan(rs app.RequestScope, model *models.PricePlan) (*models.PricePlan, error)
}

// SettingService provides services related with settings.
type SettingService struct {
	dao settingDAO
}

// NewSettingService creates a new SettingService with the given setting DAO.
func NewSettingService(dao settingDAO) *SettingService {
	return &SettingService{dao}
}

// CreatePricingTariff creates a new pricing tariff.
func (s *SettingService) CreatePricingTariff(rs app.RequestScope, model *models.PricingTariff) (*models.PricingTariff, error) {
	if err := model.ValidatePricingTariff(); err != nil {
		return nil, err
	}

	if model.TariffDescription == "" {
		model.TariffDescription = model.TariffName
	}

	return s.dao.CreatePricingTariff(rs, model)
}

// CountPricingTariff returns the number of pricing tariff.
func (s *SettingService) CountPricingTariff(rs app.RequestScope) (int, error) {
	return s.dao.CountPricingTariff(rs)
}

// QueryPricingTariff returns the pricing tariff with the specified offset and limit.
func (s *SettingService) QueryPricingTariff(rs app.RequestScope, offset, limit int) ([]models.PricingTariff, error) {
	return s.dao.QueryPricingTariff(rs, offset, limit)
}

// CountPricePlan returns the number of pricing plan.
func (s *SettingService) CountPricePlan(rs app.RequestScope) (int, error) {
	return s.dao.CountPricePlan(rs)
}

// QueryPricePlan returns the pricing plan with the specified offset and limit.
func (s *SettingService) QueryPricePlan(rs app.RequestScope, offset, limit, id int) ([]models.PricePlan, error) {
	return s.dao.QueryPricePlan(rs, offset, limit, id)
}

// CreatePricePlan creates a new pricing plan.
func (s *SettingService) CreatePricePlan(rs app.RequestScope, model *models.PricePlan) (*models.PricePlan, error) {
	if err := model.ValidatePricePlan(); err != nil {
		return nil, err
	}

	return s.dao.CreatePricePlan(rs, model)
}

// UpdatePricePlan update pricing plan.
func (s *SettingService) UpdatePricePlan(rs app.RequestScope, model *models.PricePlan) (*models.PricePlan, error) {
	if err := model.ValidatePricePlan(); err != nil {
		return nil, err
	}

	return s.dao.UpdatePricePlan(rs, model)
}

func calculateCostings(model *models.PricePlan) (float64, error) {
	var units float64
	var totalAmount float64
	units = 1

	if model.FixedCharge > 0.00 {
		totalAmount = totalAmount + app.ToFixed(model.FixedCharge, 4)
	}

	if model.EnergyCharge > 0.00 {
		totalAmount = totalAmount + app.ToFixed((units*model.EnergyCharge), 4)
		fmt.Println(app.ToFixed(units*model.EnergyCharge, 4))
	}

	if model.FuelCostCharge > 0.00 {
		totalAmount = totalAmount + app.ToFixed((units*model.FuelCostCharge), 4)
		fmt.Println(app.ToFixed(units*model.FuelCostCharge, 4))
	}

	if model.ForexAdj > 0.00 {
		totalAmount = totalAmount + app.ToFixed((units*model.ForexAdj), 4)
		fmt.Println(app.ToFixed(units*model.ForexAdj, 4))
	}

	if model.InflationAdj > 0.00 {
		totalAmount = totalAmount + app.ToFixed((units*model.InflationAdj), 4)
		fmt.Println(app.ToFixed(units*model.InflationAdj, 4))
	}

	if model.ERCLevy > 0.00 {
		totalAmount = totalAmount + app.ToFixed((units*model.ERCLevy), 4)
		fmt.Println(app.ToFixed(units*model.ERCLevy, 4))
	}

	if model.WarmaLevy > 0.00 {
		totalAmount = totalAmount + app.ToFixed((units*model.WarmaLevy), 4)
		fmt.Println(app.ToFixed(units*model.WarmaLevy, 4))
	}

	if model.REPLevy > 0.00 {
		fmt.Println("rep")
		totalAmount = totalAmount + app.ToFixed((model.REPLevy*model.EnergyCharge*units/100), 4)
		fmt.Println(app.ToFixed((model.REPLevy * model.EnergyCharge * units / 100), 4))
	}

	if model.PowerFactorSurcharge > 0.00 {
		totalAmount = totalAmount + app.ToFixed(model.PowerFactorSurcharge, 4)
	}

	if model.VAT > 0.00 {
		fmt.Println(app.ToFixed(totalAmount*model.VAT/100, 4))
		totalAmount = totalAmount + app.ToFixed((totalAmount*model.VAT/100), 4)
	}

	fmt.Println(totalAmount)

	return totalAmount, nil
}
