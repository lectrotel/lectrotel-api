package services

import (
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// accountDAO specifies the interface of the account DAO needed by AccountService.
type accountDAO interface {
	// Get returns the account with the specified account ID.
	Get(rs app.RequestScope, id int) (*models.AccountDetails, error)
	// Count returns the number of accounts.
	Count(rs app.RequestScope) (int, error)
	// Query returns the list of accounts with the given offset and limit.
	Query(rs app.RequestScope, offset, limit int) ([]models.AccountDetails, error)
	// Create saves a new account in the storage.
	Create(rs app.RequestScope, account *models.AccountDetails) error
	// Update updates the account with given ID in the storage.
	Update(rs app.RequestScope, id int, account *models.AccountDetails) error
	// Delete removes the account with given ID from the storage.
	Delete(rs app.RequestScope, id int) error
}

// AccountService provides services related with accounts.
type AccountService struct {
	dao accountDAO
}

// NewAccountService creates a new AccountService with the given account DAO.
func NewAccountService(dao accountDAO) *AccountService {
	return &AccountService{dao}
}

// Get returns the account with the specified the account ID.
func (s *AccountService) Get(rs app.RequestScope, id int) (*models.AccountDetails, error) {
	return s.dao.Get(rs, id)
}

// Create creates a new account.
func (s *AccountService) Create(rs app.RequestScope, model *models.AccountDetails) (*models.AccountDetails, error) {
	if err := model.ValidateAccountDetails(); err != nil {
		return nil, err
	}
	if err := s.dao.Create(rs, model); err != nil {
		return nil, err
	}
	return s.dao.Get(rs, model.ID)
}

// Update updates the account with the specified ID.
func (s *AccountService) Update(rs app.RequestScope, id int, model *models.AccountDetails) (*models.AccountDetails, error) {
	if err := model.ValidateAccountDetails(); err != nil {
		return nil, err
	}
	if err := s.dao.Update(rs, id, model); err != nil {
		return nil, err
	}
	return s.dao.Get(rs, id)
}

// Delete deletes the account with the specified ID.
func (s *AccountService) Delete(rs app.RequestScope, id int) (*models.AccountDetails, error) {
	account, err := s.dao.Get(rs, id)
	if err != nil {
		return nil, err
	}
	err = s.dao.Delete(rs, id)
	return account, err
}

// Count returns the number of accounts.
func (s *AccountService) Count(rs app.RequestScope) (int, error) {
	return s.dao.Count(rs)
}

// Query returns the accounts with the specified offset and limit.
func (s *AccountService) Query(rs app.RequestScope, offset, limit int) ([]models.AccountDetails, error) {
	return s.dao.Query(rs, offset, limit)
}
