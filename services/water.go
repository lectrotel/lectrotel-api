package services

import (
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// waterDAO specifies the interface of the water DAO needed by WaterService.
type waterDAO interface {
	QueryWaterPricePlan(rs app.RequestScope, offset, limit int) ([]models.PricePlanWater, error)
	CountWaterPricePlan(rs app.RequestScope) (int, error)
	UpdateWaterPricePlan(rs app.RequestScope, model *models.PricePlanWater) (*models.PricePlanWater, error)
}

// WaterService provides services related with waters.
type WaterService struct {
	dao waterDAO
}

// NewWaterService creates a new WaterService with the given water DAO.
func NewWaterService(dao waterDAO) *WaterService {
	return &WaterService{dao}
}

// CountWaterPricePlan returns the number of water price plan.
func (s *WaterService) CountWaterPricePlan(rs app.RequestScope) (int, error) {
	return s.dao.CountWaterPricePlan(rs)
}

// QueryWaterPricePlan returns the water price plan with the specified offset and limit.
func (s *WaterService) QueryWaterPricePlan(rs app.RequestScope, offset, limit int) ([]models.PricePlanWater, error) {
	return s.dao.QueryWaterPricePlan(rs, offset, limit)
}

// UpdateWaterPricePlan updates water pricing plan.
func (s *WaterService) UpdateWaterPricePlan(rs app.RequestScope, model *models.PricePlanWater) (*models.PricePlanWater, error) {
	return s.dao.UpdateWaterPricePlan(rs, model)
}
