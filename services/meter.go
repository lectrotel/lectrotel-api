package services

import (
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
	"go.mongodb.org/mongo-driver/mongo"
)

// meterDAO specifies the interface of the meter DAO needed by MeterService.
type meterDAO interface {
	// Get returns the meter with the specified meter ID.
	Get(rs app.RequestScope, id int) (*models.MeterDetails, error)
	GetMeterDetails(rs app.RequestScope, meternumber string) (*models.MeterDetails, error)
	// Count returns the number of meters.
	Count(rs app.RequestScope) (int, error)
	// Query returns the list of meters with the given offset and limit.
	Query(rs app.RequestScope, offset, limit int) ([]models.MeterDetails, error)
	// Create saves a new meter in the storage.
	Create(rs app.RequestScope, meter *models.MeterDetails) error
	// Update updates the meter with given ID in the storage.
	Update(rs app.RequestScope, id int, meter *models.MeterDetails) error
	// Delete removes the meter with given ID from the storage.
	Delete(rs app.RequestScope, id int) error
	GetMeterLogs(db *mongo.Database, id int, offset, limit int) ([]models.DeviceData, error)
}

// MeterService provides services related with meters.
type MeterService struct {
	dao meterDAO
}

// NewMeterService creates a new MeterService with the given meter DAO.
func NewMeterService(dao meterDAO) *MeterService {
	return &MeterService{dao}
}

// Get returns the meter with the specified the meter ID.
func (m *MeterService) Get(rs app.RequestScope, id int) (*models.MeterDetails, error) {
	return m.dao.Get(rs, id)
}

// GetMeterDetails get meter details by meter number
func (m *MeterService) GetMeterDetails(rs app.RequestScope, number string) (*models.MeterDetails, error) {
	return m.dao.GetMeterDetails(rs, number)
}

// Create creates a new meter.
func (m *MeterService) Create(rs app.RequestScope, model *models.MeterDetails) (*models.MeterDetails, error) {
	if err := model.Validate(); err != nil {
		return nil, err
	}
	if err := m.dao.Create(rs, model); err != nil {
		return nil, err
	}
	return m.dao.Get(rs, model.ID)
}

// Update updates the meter with the specified ID.
func (m *MeterService) Update(rs app.RequestScope, id int, model *models.MeterDetails) (*models.MeterDetails, error) {
	if err := model.Validate(); err != nil {
		return nil, err
	}
	if err := m.dao.Update(rs, id, model); err != nil {
		return nil, err
	}
	return m.dao.Get(rs, id)
}

// Delete deletes the meter with the specified ID.
func (m *MeterService) Delete(rs app.RequestScope, id int) (*models.MeterDetails, error) {
	meter, err := m.dao.Get(rs, id)
	if err != nil {
		return nil, err
	}
	err = m.dao.Delete(rs, id)
	return meter, err
}

// Count returns the number of meters.
func (m *MeterService) Count(rs app.RequestScope) (int, error) {
	return m.dao.Count(rs)
}

// Query returns the meters with the specified offset and limit.
func (m *MeterService) Query(rs app.RequestScope, offset, limit int) ([]models.MeterDetails, error) {
	return m.dao.Query(rs, offset, limit)
}

// GetMeterLogs returns the meters with the specified logs.
func (m *MeterService) GetMeterLogs(db *mongo.Database, id, offset, limit int) ([]models.DeviceData, error) {
	return m.dao.GetMeterLogs(db, id, offset, limit)
}
