package services

import (
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// gatewayDAO specifies the interface of the gateway DAO needed by GatewayService.
type gatewayDAO interface {
	// Get returns the gateway with the specified gateway ID.
	Get(rs app.RequestScope, id int) (*models.GatewayDetails, error)
	// Count returns the number of gateways.
	Count(rs app.RequestScope) (int, error)
	// Query returns the list of gateways with the given offset and limit.
	Query(rs app.RequestScope, offset, limit int) ([]models.GatewayDetails, error)
	// Create saves a new gateway in the storage.
	Create(rs app.RequestScope, gateway *models.GatewayDetails) error
	// Update updates the gateway with given ID in the storage.
	Update(rs app.RequestScope, id int, gateway *models.GatewayDetails) error
	// Delete removes the gateway with given ID from the storage.
	Delete(rs app.RequestScope, id int) error
}

// GatewayService provides services related with gateways.
type GatewayService struct {
	dao gatewayDAO
}

// NewGatewayService creates a new GatewayService with the given gateway DAO.
func NewGatewayService(dao gatewayDAO) *GatewayService {
	return &GatewayService{dao}
}

// Get returns the gateway with the specified the gateway ID.
func (s *GatewayService) Get(rs app.RequestScope, id int) (*models.GatewayDetails, error) {
	return s.dao.Get(rs, id)
}

// Create creates a new gateway.
func (s *GatewayService) Create(rs app.RequestScope, model *models.GatewayDetails) (*models.GatewayDetails, error) {
	if err := model.ValidateGatewayDetails(); err != nil {
		return nil, err
	}
	if err := s.dao.Create(rs, model); err != nil {
		return nil, err
	}
	return s.dao.Get(rs, model.ID)
}

// Update updates the gateway with the specified ID.
func (s *GatewayService) Update(rs app.RequestScope, id int, model *models.GatewayDetails) (*models.GatewayDetails, error) {
	if err := model.ValidateGatewayDetails(); err != nil {
		return nil, err
	}
	if err := s.dao.Update(rs, id, model); err != nil {
		return nil, err
	}
	return s.dao.Get(rs, id)
}

// Delete deletes the gateway with the specified ID.
func (s *GatewayService) Delete(rs app.RequestScope, id int) (*models.GatewayDetails, error) {
	gateway, err := s.dao.Get(rs, id)
	if err != nil {
		return nil, err
	}
	err = s.dao.Delete(rs, id)
	return gateway, err
}

// Count returns the number of gateways.
func (s *GatewayService) Count(rs app.RequestScope) (int, error) {
	return s.dao.Count(rs)
}

// Query returns the gateways with the specified offset and limit.
func (s *GatewayService) Query(rs app.RequestScope, offset, limit int) ([]models.GatewayDetails, error) {
	return s.dao.Query(rs, offset, limit)
}
