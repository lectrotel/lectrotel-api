package services

import (
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"math/rand"
	"strconv"
	"time"

	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// transactionDAO specifies the interface of the transaction DAO needed by TransactionService.
type transactionDAO interface {
	AddInvoice(rs app.RequestScope, model *models.Invoices) error
	AddInvoiceItems(rs app.RequestScope, model *models.InvoiceItems) error
	CountInvoices(rs app.RequestScope, meterno string) (int, error)
	ListInvoices(rs app.RequestScope, offset, limit int, meterno string) ([]models.Invoices, error)
	GetInvoice(rs app.RequestScope, id int) (*models.InvoiceDetails, error)

	Get(rs app.RequestScope, id int) (*models.RequestToken, error)
	// GenerateWaterToken saves a new transaction in the storage.
	SaveToken(rs app.RequestScope, m *models.RequestToken, t *models.ResponseData) error
	CountTokens(rs app.RequestScope, meterno string) (int, error)
	GetTokenList(rs app.RequestScope, offset, limit int, meterno string) ([]models.ListTokens, error)
	CountPayments(rs app.RequestScope, meterno string) (int, error)
	GetPaymentsList(rs app.RequestScope, offset, limit int, meterno string) ([]models.C2BResponse, error)
	GetPaymentDetails(rs app.RequestScope, transid, meterno string) (models.PaymentDetails, error)
	SaveTransaction(rs app.RequestScope, model *models.C2BResponse) error
	IsTransactionExists(rs app.RequestScope, transID, transTime string) (int, error)
	MpesaSTKCheckout(rs app.RequestScope, model *models.TransInvoices, c chan models.ProcessTransJobs) error
	MpesaCheckoutConfirmation(rs app.RequestScope, checkoutid chan models.ProcessTransJobs, token chan models.TokenTopupJob) error
	PostPaidMpesaCheckoutConfirmation(rs app.RequestScope, checkoutid chan models.ProcessTransJobs) error

	WaterMpesaCheckoutConfirmation(rs app.RequestScope, checkoutid chan models.ProcessTransJobs, token chan string) error
	CalculateWaterPrepaidCosting(rs app.RequestScope, meterid int32, amount float64) (models.TokenDetails, error)
	SaveTransactionPayments(rs app.RequestScope, model *models.TransInvoices) error
	CalculatePrepaidCosting(rs app.RequestScope, meterid int32, amount float64) (models.TokenDetails, error)
	GetMeterNumberByID(rs app.RequestScope, meterid int32) (string, error)
	SavePublishMessageMQTT(rs app.RequestScope, data *models.PublishedMessages) error
}

// TransactionService provides services related with transactions.
type TransactionService struct {
	dao transactionDAO
}

// NewTransactionService creates a new TransactionService with the given transaction DAO.
func NewTransactionService(dao transactionDAO) *TransactionService {
	return &TransactionService{dao}
}

// Get returns the transaction with the specified the transaction ID.
func (s *TransactionService) Get(rs app.RequestScope, id int) (*models.RequestToken, error) {
	return s.dao.Get(rs, id)
}

// CountTokens returns the number of tokens generated.
func (s *TransactionService) CountTokens(rs app.RequestScope, meterno string) (int, error) {
	return s.dao.CountTokens(rs, meterno)
}

// GetTokenList returns the number of  tokens generated with the specified offset and limit.
func (s *TransactionService) GetTokenList(rs app.RequestScope, offset, limit int, meterno string) ([]models.ListTokens, error) {
	return s.dao.GetTokenList(rs, offset, limit, meterno)
}

// CountPayments returns the number of payments.
func (s *TransactionService) CountPayments(rs app.RequestScope, meterno string) (int, error) {
	return s.dao.CountPayments(rs, meterno)
}

// GetPaymentsList returns the number of payments with the specified offset and limit.
func (s *TransactionService) GetPaymentsList(rs app.RequestScope, offset, limit int, meterno string) ([]models.C2BResponse, error) {
	return s.dao.GetPaymentsList(rs, offset, limit, meterno)
}

// GetPaymentDetails get payment details
func (s *TransactionService) GetPaymentDetails(rs app.RequestScope, transid, meterno string) (models.PaymentDetails, error) {
	return s.dao.GetPaymentDetails(rs, transid, meterno)
}

// SaveCashPostPaidPayments ...
func (s *TransactionService) SaveCashPostPaidPayments(rs app.RequestScope, model *models.TransInvoices) error {
	// save transaction
	model.TransID = app.GenerateNewStringID()
	transinvoice := models.NewTransInvoices(0, model.MeterID, rs.CompanyID(), rs.UserID(), model.Amount, model.TransID, model.PaymentOption, model.PhoneNumber, "Electricity Payment", "", model.MeterType)

	err := s.dao.SaveTransactionPayments(rs, &transinvoice)

	return err
}

// SaveMpesaPostPaidPayments ...
func (s *TransactionService) SaveMpesaPostPaidPayments(rs app.RequestScope, model *models.TransInvoices) error {
	// save transaction
	model.TransID = app.GenerateNewStringID()
	// transinvoice := models.NewTransInvoices(0, model.MeterID, model.Amount, model.TransID, model.PaymentOption, model.PhoneNumber, "Electricity Payment", "")

	c := make(chan models.ProcessTransJobs)
	go s.dao.PostPaidMpesaCheckoutConfirmation(rs, c)

	model.TransID = app.GenerateNewStringID()
	err := s.dao.MpesaSTKCheckout(rs, model, c)

	return err
}

// ElecTokensCashPurchase ...
func (s *TransactionService) ElecTokensCashPurchase(rs app.RequestScope, model *models.TransInvoices) (resp models.ResponseData, err error) {
	// calculate costings
	tokendetails, err := s.dao.CalculatePrepaidCosting(rs, model.MeterID, model.Amount)
	if err != nil {
		return resp, err
	}

	// save transaction
	model.TransID = app.GenerateNewStringID()
	transinvoice := models.NewTransInvoices(0, model.MeterID, rs.CompanyID(), rs.UserID(), model.Amount, model.TransID, model.PaymentOption, model.PhoneNumber, "Electricity Payment", "", model.MeterType)
	err = s.dao.SaveTransactionPayments(rs, &transinvoice)
	if err != nil {
		return resp, err
	}

	// save token details
	requestToken := models.NewRequestToken(0, model.MeterID, model.Amount, tokendetails.Units, tokendetails.UnitsAmount, model.PhoneNumber, model.TransID, "", model.MeterType)
	responsedata := models.NewResponseData(model.RequestCheckOutID, "0", "Pending", "Pending")
	s.dao.SaveToken(rs, &requestToken, &responsedata)

	// get meter no
	mno, _ := s.dao.GetMeterNumberByID(rs, model.MeterID)
	tokentopup := models.NewTokenTopup(model.TransID, tokendetails.GatewayID, float32(tokendetails.Units), 3, 17, tokendetails.ModBusID, 0, mno)
	var tokenjob = models.TokenTopupJob{
		TokenTopup:       tokentopup,
		TransInvoices:    transinvoice,
		UnitsTotalAmount: tokendetails.UnitsAmount,
	}

	if model.MeterType == 0 {
		go s.TokensTopup(rs, &tokenjob)
	}

	resp.ResultCode = "00"
	return resp, err
}

// ElecPostPaidCashPurchase ...
func (s *TransactionService) ElecPostPaidCashPurchase(rs app.RequestScope, model *models.TransInvoices) (resp models.ResponseData, err error) {
	// calculate costings
	tokendetails, err := s.dao.CalculatePrepaidCosting(rs, model.MeterID, model.Amount)
	if err != nil {
		return resp, err
	}

	// save transaction
	model.TransID = app.GenerateNewStringID()
	transinvoice := models.NewTransInvoices(0, model.MeterID, rs.CompanyID(), rs.UserID(), model.Amount, model.TransID, model.PaymentOption, model.PhoneNumber, "Electricity Payment", "", model.MeterType)
	err = s.dao.SaveTransactionPayments(rs, &transinvoice)
	if err != nil {
		return resp, err
	}

	// save token details
	requestToken := models.NewRequestToken(0, model.MeterID, model.Amount, tokendetails.Units, tokendetails.UnitsAmount, model.PhoneNumber, model.TransID, "", model.MeterType)
	responsedata := models.NewResponseData(model.RequestCheckOutID, "0", "Pending", "Pending")
	s.dao.SaveToken(rs, &requestToken, &responsedata)

	// get meter no
	mno, _ := s.dao.GetMeterNumberByID(rs, model.MeterID)
	tokentopup := models.NewTokenTopup(model.TransID, tokendetails.GatewayID, float32(tokendetails.Units), 3, 17, tokendetails.ModBusID, 0, mno)
	var tokenjob = models.TokenTopupJob{
		TokenTopup:       tokentopup,
		TransInvoices:    transinvoice,
		UnitsTotalAmount: tokendetails.UnitsAmount,
	}

	if model.MeterType == 0 {
		s.TokensTopup(rs, &tokenjob)
	}

	resp.ResultCode = "00"
	return resp, err
}

// ElecTokensMpesaPurchase ...
func (s *TransactionService) ElecTokensMpesaPurchase(rs app.RequestScope, model *models.TransInvoices) (models.ResponseData, error) {
	var res models.ResponseData
	token := make(chan models.TokenTopupJob)
	go s.TopupTokensChannel(rs, token)

	c := make(chan models.ProcessTransJobs)
	go s.dao.MpesaCheckoutConfirmation(rs, c, token)

	model.TransID = app.GenerateNewStringID()
	err := s.dao.MpesaSTKCheckout(rs, model, c)

	res.ResultCode = "00"
	return res, err
}

// TopupTokensChannel ...
func (s *TransactionService) TopupTokensChannel(rs app.RequestScope, model chan models.TokenTopupJob) (tk bool, err error) {
	for {
		tokenJob := <-model
		// save token details
		// unitsamount := tokenJob.TransInvoices.Amount * float64(tokenJob.TokenTopup.RechargeUnit)
		requestToken := models.NewRequestToken(0, tokenJob.TransInvoices.MeterID, tokenJob.TransInvoices.Amount, tokenJob.TokenTopup.RechargeUnit, tokenJob.UnitsTotalAmount, tokenJob.TransInvoices.PhoneNumber, tokenJob.TransInvoices.TransID, "", tokenJob.TransInvoices.MeterType)
		responsedata := models.NewResponseData(tokenJob.TransInvoices.RequestCheckOutID, "0", "Pending", "Pending")
		s.dao.SaveToken(rs, &requestToken, &responsedata)

		if tokenJob.TransInvoices.MeterType == 0 {
			s.TokensTopup(rs, &tokenJob)
		}
	}
}

func (s *TransactionService) getMeterNumberByIDAsync(rs app.RequestScope, model *models.TokenTopupJob) chan string {
	r := make(chan string)
	fmt.Println("Warming up ...")
	go func() {
		mno, _ := s.dao.GetMeterNumberByID(rs, model.TransInvoices.MeterID)
		time.Sleep(2 * time.Second)
		r <- mno
		model.TokenTopup.MeterNumber = mno
		fmt.Println("Done ...")
	}()
	return r
}

// TokensTopup ...
func (s *TransactionService) TokensTopup(rs app.RequestScope, model *models.TokenTopupJob) (bool, error) {

	if err := model.TokenTopup.Validate(); err != nil {
		fmt.Println(err)
		return false, err
	}

	byteLength := 25

	buf := make([]byte, byteLength)
	buf[0] = 'L'
	buf[1] = 'E'
	buf[2] = 'M'
	buf[3] = 'O'
	buf[4] = 'N'
	buf[5] = byte(model.TokenTopup.MessageID)
	buf[6] = byte(byteLength)
	binary.BigEndian.PutUint32(buf[7:], uint32(model.TokenTopup.GatewayID))

	if model.TokenTopup.ModbusID == 0 {
		model.TokenTopup.ModbusID = 1
	}
	buf[11] = byte(model.TokenTopup.ModbusID)
	binary.BigEndian.PutUint32(buf[12:], math.Float32bits(model.TokenTopup.RechargeUnit))

	meterno, _ := strconv.Atoi(model.TokenTopup.MeterNumber)
	binary.BigEndian.PutUint32(buf[16:], uint32(meterno))

	rand.Seed(time.Now().UnixNano())
	min := 1000
	max := 9999999
	randval := rand.Intn(max-min+1) + min
	binary.BigEndian.PutUint32(buf[20:], uint32(randval))

	buf[24] = 0

	for i := 5; i < 24; i++ {
		buf[24] += buf[i]
	}

	fmt.Println("*****************************************************")
	fmt.Println(model.TokenTopup)

	// save send data
	data := models.PublishedMessages{
		SystemCode:   "LEMON",
		MessageID:    model.TokenTopup.MessageID,
		GatewayID:    model.TokenTopup.GatewayID,
		ModbusID:     model.TokenTopup.ModbusID,
		MeterNumber:  meterno,
		RechargeUnit: model.TokenTopup.RechargeUnit,
		TokenRandID:  randval,
		ByteLength:   byteLength,
		Checksum:     int(buf[24]),
	}
	if err := s.dao.SavePublishMessageMQTT(rs, &data); err != nil {
		fmt.Println(err)
		return false, err
	}

	topic := "gw/pgw/" + strconv.Itoa(model.TokenTopup.GatewayID) + "/b"
	// publish to broker
	go app.PublishMessageMQTT(topic, buf, model)
	fmt.Println(buf)

	return true, nil
}

// WaterMpesaTokensPurchase ...
func (s *TransactionService) WaterMpesaTokensPurchase(rs app.RequestScope, model *models.RequestToken) (models.ResponseData, error) {
	var res models.ResponseData
	transid := make(chan string)
	go s.TopupWaterTokensChannel(rs, transid, model)

	c := make(chan models.ProcessTransJobs)
	go s.dao.WaterMpesaCheckoutConfirmation(rs, c, transid)

	transinvoice := models.NewTransInvoices(0, model.MeterID, rs.CompanyID(), rs.UserID(), model.Amount, app.GenerateNewStringID(), model.PaymentOption, model.ToNumber, "Water Payment", "", model.MeterType)
	err := s.dao.MpesaSTKCheckout(rs, &transinvoice, c)

	res.ResultCode = "00"
	return res, err
}

// TopupWaterTokensChannel ...
func (s *TransactionService) TopupWaterTokensChannel(rs app.RequestScope, transid chan string, model *models.RequestToken) (rd models.ResponseData, err error) {
	for {
		transid := <-transid
		model.PaymentID = transid

		return s.GenerateWaterToken(rs, model)
	}
}

// GenerateWaterToken creates a new transaction.
func (s *TransactionService) GenerateWaterToken(rs app.RequestScope, model *models.RequestToken) (rd models.ResponseData, err error) {

	tokenDetails, err := s.dao.CalculateWaterPrepaidCosting(rs, model.MeterID, model.Amount)
	if model.MeterType == 1 {
		tokenDetails, err = s.dao.CalculatePrepaidCosting(rs, model.MeterID, model.Amount)
	}
	if err != nil {
		return rd, err
	}
	model.TotalUnitsAmount = tokenDetails.UnitsAmount
	model.Units = tokenDetails.Units

	// save transaction
	if model.PaymentID == "" {
		model.PaymentID = app.GenerateNewStringID()
	}

	if model.PaymentOption == "Cash" {
		transinvoice := models.NewTransInvoices(0, model.MeterID, rs.CompanyID(), rs.UserID(), model.Amount, model.PaymentID, model.PaymentOption, model.ToNumber, "Water Payment", "", model.MeterType)
		err = s.dao.SaveTransactionPayments(rs, &transinvoice)
		if err != nil {
			return rd, err
		}
	}

	serialid := app.GenerateNewStringID()
	resp, err := app.GetToken2(tokenDetails.MeterNumber, tokenDetails.Units, model.MeterType)
	if err != nil {
		return rd, err
	}

	requestToken := models.NewResponseData(serialid, strconv.Itoa(resp.ResultCode), resp.Reason, resp.Result.Token)
	err = s.dao.SaveToken(rs, model, &requestToken)
	if err != nil {
		return rd, err
	}

	// // send message
	if resp.Reason == "OK" {
		// amount := fmt.Sprintf("%f", app.ToFixed(model.Amount, 2))
		// units := fmt.Sprintf("%f", model.Units)
		amount := strconv.FormatFloat(model.Amount, 'f', 2, 64)
		units := strconv.FormatFloat(float64(model.Units), 'f', 2, 64)

		message := "Confirmed. Tokens \nMtr No. " + tokenDetails.MeterNumber + " \nToken: " + resp.Result.Token
		message += " \nDate: " + time.Now().Format("2006-01-02 15:04")
		message += " \nAmount: Kes. " + amount
		message += " \nUnits: " + units
		// fmt.Println(message)
		app.Message <- models.MessageDetails{
			MessageID: model.PaymentID,
			Message:   message,
			ToNumber:  "+" + model.ToNumber,
		}
	}

	return requestToken, err
}

// RegisterURL Register C2B Confirmation and Validation URLs
func (s *TransactionService) RegisterURL(rs app.RequestScope, model models.C2BRegisterURL) (string, error) {
	if err := model.ValidateRegisterURL(); err != nil {
		return "", err
	}

	svc, err := app.New(app.APPKEY, app.APPSECRET, app.SANDBOX)
	if err != nil {
		return "", err
	}

	res, err := svc.C2BRegisterURL(model)
	if err != nil {
		return "", err
	}
	return res, nil
}

// SimulateMpesaTransaction ...
func (s *TransactionService) SimulateMpesaTransaction(rs app.RequestScope, model models.C2B) (n models.C2BSimulateResponse, err error) {
	if err := model.ValidateC2B(); err != nil {
		return n, err
	}

	svc, err := app.New(app.APPKEY, app.APPSECRET, app.SANDBOX)
	if err != nil {
		return n, err
	}

	res, err := svc.C2BSimulation(model)
	if err != nil {
		return n, err
	}

	var dat models.C2BSimulateResponse
	if err := json.Unmarshal(res, &dat); err != nil {
		panic(err)
	}
	dat.Status = 200

	return dat, nil
}

// SaveTransaction ...
func (s *TransactionService) SaveTransaction(rs app.RequestScope, model *models.C2BResponse) error {
	// if err := model.ValidateVResponse(); err != nil {
	// 	return err
	// }

	exists, err := s.dao.IsTransactionExists(rs, model.TransID, model.TransTime)
	if err != nil {
		return err
	}

	if exists == 1 {
		return errors.New("Transaction Exists")
	}

	if err := s.dao.SaveTransaction(rs, model); err != nil {
		return err
	}

	return nil
}
