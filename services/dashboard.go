package services

import (
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// dashboardDAO specifies the interface of the dashboard DAO needed by DashboardService.
type dashboardDAO interface {
	// Get returns the dashboard with the specified dashboard ID.
	GetUserDashboardStats(rs app.RequestScope) (*models.UserDashboardDetails, error)
}

// DashboardService provides services related with dashboards.
type DashboardService struct {
	dao dashboardDAO
}

// NewDashboardService creates a new DashboardService with the given dashboard DAO.
func NewDashboardService(dao dashboardDAO) *DashboardService {
	return &DashboardService{dao}
}

// GetUserDashboardStats ...
func (m *DashboardService) GetUserDashboardStats(rs app.RequestScope) (*models.UserDashboardDetails, error) {
	return m.dao.GetUserDashboardStats(rs)
}
