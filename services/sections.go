package services

import (
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

// sectionDAO specifies the interface of the section DAO needed by SectionService.
type sectionDAO interface {
	// Get returns the section with the specified section ID.
	Get(rs app.RequestScope, id int) (*models.SectionDetails, error)
	// Count returns the number of sections.
	Count(rs app.RequestScope) (int, error)
	// Query returns the list of sections with the given offset and limit.
	Query(rs app.RequestScope, offset, limit int) ([]models.SectionDetails, error)
	// Create saves a new section in the storage.
	Create(rs app.RequestScope, section *models.SectionDetails) error
	// Update updates the section with given ID in the storage.
	Update(rs app.RequestScope, id int, section *models.SectionDetails) error
	// Delete removes the section with given ID from the storage.
	Delete(rs app.RequestScope, id int) error
}

// SectionService provides services related with sections.
type SectionService struct {
	dao sectionDAO
}

// NewSectionService creates a new SectionService with the given section DAO.
func NewSectionService(dao sectionDAO) *SectionService {
	return &SectionService{dao}
}

// Get returns the section with the specified the section ID.
func (s *SectionService) Get(rs app.RequestScope, id int) (*models.SectionDetails, error) {
	return s.dao.Get(rs, id)
}

// Create creates a new section.
func (s *SectionService) Create(rs app.RequestScope, model *models.SectionDetails) (*models.SectionDetails, error) {
	if err := model.ValidateSectionDetails(); err != nil {
		return nil, err
	}
	if err := s.dao.Create(rs, model); err != nil {
		return nil, err
	}
	return s.dao.Get(rs, model.ID)
}

// Update updates the section with the specified ID.
func (s *SectionService) Update(rs app.RequestScope, id int, model *models.SectionDetails) (*models.SectionDetails, error) {
	if err := model.ValidateSectionDetails(); err != nil {
		return nil, err
	}
	if err := s.dao.Update(rs, id, model); err != nil {
		return nil, err
	}
	return s.dao.Get(rs, id)
}

// Delete deletes the section with the specified ID.
func (s *SectionService) Delete(rs app.RequestScope, id int) (*models.SectionDetails, error) {
	section, err := s.dao.Get(rs, id)
	if err != nil {
		return nil, err
	}
	err = s.dao.Delete(rs, id)
	return section, err
}

// Count returns the number of sections.
func (s *SectionService) Count(rs app.RequestScope) (int, error) {
	return s.dao.Count(rs)
}

// Query returns the sections with the specified offset and limit.
func (s *SectionService) Query(rs app.RequestScope, offset, limit int) ([]models.SectionDetails, error) {
	return s.dao.Query(rs, offset, limit)
}
