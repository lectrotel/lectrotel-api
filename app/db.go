package app

import (
	"os"

	dbx "github.com/go-ozzo/ozzo-dbx"
)

// DBCon ...
var DBCon *dbx.DB

// InitializeDB initialize DB conn
func InitializeDB() *dbx.DB {
	db, err := dbx.MustOpen("mysql", getDNS())
	if err != nil {
		panic(err)
	}

	DBCon = db
	return db
}

func getDNS() string {
	if os.Getenv("GO_ENV") == "production" {
		return Config.ServerDSN
	}

	return Config.LocalDSN
}

