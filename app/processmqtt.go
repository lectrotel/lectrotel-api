package app

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/lectrotel-api/models"
)

// ProcessPrepaidRequest ...
func ProcessPrepaidRequest(client MQTT.Client, b []byte, topic string, clientJobs chan models.ClientJob) {
	var deviceData models.DeviceData
	byteReader := bytes.NewReader(b)

	// system code – 5 byte
	scode := make([]byte, 5)
	byteReader.Read(scode)
	deviceData.SystemCode = string(scode)

	// Message id – 1 byte
	mid := processSeeked(byteReader, 1, 5)
	deviceData.MessageID = int(mid[0])

	// byte count – 1 byte
	bcount := processSeeked(byteReader, 1, 6)
	deviceData.ByteCount = int(bcount[0])

	// GatewayID - 4bytes
	gid := processSeeked(byteReader, 4, 7)
	deviceData.GatewayID = binary.BigEndian.Uint32(gid)

	// ModbusID - 1 byte
	modbus := processSeeked(byteReader, 1, 11)
	deviceData.ModbusID = int(modbus[0])

	meterno := processSeeked(byteReader, 4, 12)
	deviceData.MeterID = binary.BigEndian.Uint32(meterno)
	if deviceData.MeterID == 0 {
		return
	}

	// EnergyReading - 4bytes
	er := processSeeked(byteReader, 4, 16)
	deviceData.EnergyReading = float32frombytes(er)

	// RemainingUints - 4bytes
	ru := processSeeked(byteReader, 4, 20)
	deviceData.RemainingUnits = float32frombytes(ru)

	// Online - 1 byte
	online := processSeeked(byteReader, 1, 24)
	deviceData.Online = int8(online[0])

	// RelayStatus - 1 byte
	relay := processSeeked(byteReader, 1, 25)
	deviceData.RelayStatus = int8(relay[0])

	// Date
	res := processSeeked(byteReader, 1, 26)
	deviceData.UTCTimeSeconds = int(res[0])

	res = processSeeked(byteReader, 1, 27)
	deviceData.UTCTimeMinutes = int(res[0])

	res = processSeeked(byteReader, 1, 28)
	deviceData.UTCTimeHours = int(res[0])

	res = processSeeked(byteReader, 1, 29)
	deviceData.UTCTimeDay = int(res[0])

	mn := processSeeked(byteReader, 1, 30)
	deviceData.UTCTimeMonth = int(mn[0])

	yr := processSeeked(byteReader, 2, 31)
	deviceData.UTCTimeYear = int(binary.LittleEndian.Uint16(yr))

	checksum := processSeeked(byteReader, 1, 33)
	deviceData.Checksum = int(checksum[0])

	deviceData.DateTime = time.Date(deviceData.UTCTimeYear, time.Month(deviceData.UTCTimeMonth), deviceData.UTCTimeDay, deviceData.UTCTimeHours, deviceData.UTCTimeMinutes, deviceData.UTCTimeSeconds, 0, time.UTC)
	deviceData.DateTimeStamp = deviceData.DateTime.Unix()
	deviceData.CreatedOn = time.Now()

	chks := make([]byte, 1)
	for i := 5; i < 33; i++ {
		chks[0] += b[i]
	}

	fmt.Println("deviceData.Checksum")
	fmt.Println(deviceData.Checksum, deviceData.Checksum, chks)

	clientJobs <- models.ClientJob{
		DeviceData: deviceData,
		Client:     client,
		Topic:      topic,
	}
}

// ProcessPostpaidRequest ...
func ProcessPostpaidRequest(client MQTT.Client, b []byte, topic string, clientJobs chan models.ClientJob) {
	var deviceData models.DeviceData
	byteReader := bytes.NewReader(b)

	// system code – 5 byte
	scode := make([]byte, 5)
	byteReader.Read(scode)
	deviceData.SystemCode = string(scode)

	// Message id – 1 byte
	mid := processSeeked(byteReader, 1, 5)
	deviceData.MessageID = int(mid[0])

	// byte count – 1 byte
	bcount := processSeeked(byteReader, 1, 6)
	deviceData.ByteCount = int(bcount[0])

	// GatewayID - 4bytes
	gid := processSeeked(byteReader, 4, 7)
	deviceData.GatewayID = binary.BigEndian.Uint32(gid)

	// Meter ID - 4bytes
	meterid := processSeeked(byteReader, 4, 11)
	deviceData.MeterID = binary.BigEndian.Uint32(meterid)
	if deviceData.MeterID == 0 {
		return
	}

	v1 := processSeeked(byteReader, 4, 15)
	deviceData.VoltageLine1 = float32frombytes(v1)

	v2 := processSeeked(byteReader, 4, 19)
	deviceData.VoltageLine2 = float32frombytes(v2)

	v3 := processSeeked(byteReader, 4, 23)
	deviceData.VoltageLine3 = float32frombytes(v3)

	te := processSeeked(byteReader, 4, 27)
	deviceData.TotalLineCurrent = float32frombytes(te)

	sp := processSeeked(byteReader, 4, 31)
	deviceData.SystemPower = float32frombytes(sp)

	spf := processSeeked(byteReader, 4, 35)
	deviceData.SystemPowerFactor = float32frombytes(spf)

	sf := processSeeked(byteReader, 4, 39)
	deviceData.SystemFrequency = float32frombytes(sf)

	ia := processSeeked(byteReader, 4, 43)
	deviceData.ImportActiveEnergy = float32frombytes(ia)

	ir := processSeeked(byteReader, 4, 47)
	deviceData.ImportReactiveEnergy = float32frombytes(ir)
	if deviceData.ImportActiveEnergy > 0 {
		deviceData.EnergyReading = deviceData.ImportActiveEnergy
	}

	res := processSeeked(byteReader, 1, 51)
	deviceData.UTCTimeSeconds = int(res[0])

	res = processSeeked(byteReader, 1, 52)
	deviceData.UTCTimeMinutes = int(res[0])

	res = processSeeked(byteReader, 1, 53)
	deviceData.UTCTimeHours = int(res[0])

	res = processSeeked(byteReader, 1, 54)
	deviceData.UTCTimeDay = int(res[0])

	mn := processSeeked(byteReader, 1, 55)
	deviceData.UTCTimeMonth = int(mn[0])

	yr := processSeeked(byteReader, 2, 56)
	deviceData.UTCTimeYear = int(binary.BigEndian.Uint16(yr))

	checksum := processSeeked(byteReader, 1, 58)
	deviceData.Checksum = int(checksum[0])

	deviceData.DateTime = time.Date(deviceData.UTCTimeYear, time.Month(deviceData.UTCTimeMonth), deviceData.UTCTimeDay, deviceData.UTCTimeHours, deviceData.UTCTimeMinutes, deviceData.UTCTimeSeconds, 0, time.UTC)
	deviceData.DateTimeStamp = deviceData.DateTime.Unix()
	deviceData.CreatedOn = time.Now()

	chks := make([]byte, 1)
	for i := 5; i < 57; i++ {
		chks[0] += b[i]
	}

	if chks[0] != checksum[0] {
		return
	}

	clientJobs <- models.ClientJob{
		DeviceData: deviceData,
		Client:     client,
		Topic:      topic,
	}
}

func processSeeked(byteReader *bytes.Reader, bytesize, seek int64) []byte {
	byteReader.Seek(seek, 0)
	val := make([]byte, bytesize)
	byteReader.Read(val)
	return val
}
