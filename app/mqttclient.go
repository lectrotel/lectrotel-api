package app

import (
	"strconv"
	"time"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"github.com/lectrotel-api/models"
)

// PublishMessageMQTT publish a message to mqtt broker
func PublishMessageMQTT(topic string, message []byte, model *models.TokenTopupJob) {
	// client, qos, err := InitMQTTConnection()

	// MQTTClient.Publish(topic, byte(qos), false, message)
	MQTTClient.Publish(topic, 1, false, message)

	// update request token db
	DBCon.Update("request_token", dbx.Params{
		"result": "Success",
		"reason": "ok",
	}, dbx.HashExp{"trans_id": model.TokenTopup.TransID}).Execute()

	// // send message
	amount := strconv.FormatFloat(model.TransInvoices.Amount, 'f', 2, 64)
	units := strconv.FormatFloat(float64(model.TokenTopup.RechargeUnit), 'f', 2, 64)
	smsmessage := "Confirmed. Meter " + model.TokenTopup.MeterNumber + " Topup  \nTrans ID: " + model.TokenTopup.TransID
	smsmessage += " \nDate: " + time.Now().Format("2006-01-02 15:04")
	smsmessage += " \nAmount: Kes. " + amount
	smsmessage += " \nUnits: " + units
	// fmt.Println(smsmessage)
	Message <- models.MessageDetails{
		MessageID: model.TokenTopup.TransID,
		Message:   smsmessage,
		ToNumber:  "+" + model.TransInvoices.PhoneNumber,
	}
}
