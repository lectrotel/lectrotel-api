package app

import (
	"fmt"
	"time"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"github.com/lectrotel-api/models"
)

// Message ...
var Message = make(chan models.MessageDetails)

func init() {
	// message := <-Message
	go SendSMSMessages(Message)
}

// SendSMSMessages ...
func SendSMSMessages(message chan models.MessageDetails) {
	for {
		message := <-message
		//Call the Gateway, and pass the constants here!
		smsService := NewSMSService(Config.AfricaTalkingUsername, Config.AfricaTalkingAPIKey, Config.AfricaTalkingENV)

		//Send SMS - REPLACE Recipient and Message with REAL Values
		recipients, err := smsService.Send("", message.ToNumber, message.Message) //Leave blank, "", if you don't have one)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Println(recipients)
	}

}

// SMSCheck ...
type SMSCheck struct {
	MessageID int       `json:"message_id"`
	DateTime  time.Time `json:"date_time"`
	Message   string    `json:"message"`
}

// check for sent messages
func checkMessages(tonumber string) (SMSCheck, error) {
	var data SMSCheck
	q := DBCon.NewQuery("SELECT date_time, message_id, message FROM saved_messages WHERE `to`='" + tonumber + "' ORDER BY id DESC  LIMIT 1 ")
	err := q.One(&data)

	return data, err
}

// save sent messages
func saveSentMessages(m models.SaveMessageDetails) {
	DBCon.Insert("saved_messages", dbx.Params{
		"message_id":   m.MessageID,
		"message":      m.Message,
		"date_time":    m.DateTime,
		"from":         m.From,
		"to":           m.To,
		"date_created": m.DateCreated,
		"sid":          m.SID,
		"status":       m.Status,
	}).Execute()
}
