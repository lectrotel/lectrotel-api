package app

import (
	"crypto/tls"
	"fmt"
	"os"
	"strconv"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
)

// MQTTClient ...
var MQTTClient MQTT.Client
var qos = 2

// InitMQTTConnection ...
func InitMQTTConnection() (connOpts *MQTT.ClientOptions, client MQTT.Client, qos int, err error) {
	hostname, _ := os.Hostname()
	server := "tcp://74.207.254.39:1883"
	username := "lectrotel"
	password := "elec2030"
	clientid := hostname + strconv.Itoa(time.Now().Second())
	qos = 0
	topic := "gw/#"

	connOpts = MQTT.NewClientOptions()
	connOpts.AddBroker(server)
	connOpts.SetClientID(clientid)
	connOpts.SetCleanSession(false)
	if username != "" {
		connOpts.SetUsername(username)
		if password != "" {
			connOpts.SetPassword(password)
		}
	}
	tlsConfig := &tls.Config{InsecureSkipVerify: true, ClientAuth: tls.NoClientCert}
	connOpts.SetTLSConfig(tlsConfig)

	connOpts.OnConnect = func(c MQTT.Client) {
		if token := c.Subscribe(topic, byte(qos), onMessageReceived); token.Wait() && token.Error() != nil {
			fmt.Println(token.Error())
			return
		}
	}

	client = MQTT.NewClient(connOpts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		return connOpts, client, qos, token.Error()
	}

	MQTTClient = client

	fmt.Printf("Connected to %s\n", server)
	return connOpts, client, qos, err
}
