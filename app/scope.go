package app

import (
	"net/http"
	"strconv"
	"time"

	"github.com/Sirupsen/logrus"
	dbx "github.com/go-ozzo/ozzo-dbx"
)

// RequestScope contains the application-specific information that are carried around in a request.
type RequestScope interface {
	Logger
	// UserID returns the ID of the user for the current request
	UserID() int
	// CompanyID returns the Company ID of the user for the current request
	CompanyID() int
	// RoleID returns the Role ID of the user for the current request
	RoleID() int
	RoleName() string
	// SetUserID sets the ID of the currently authenticated user
	SetUserID(id string)
	// SetCompanyID sets the Company ID of the currently authenticated user
	SetCompanyID(id string)
	// SetRoleID sets the Role ID of the currently authenticated user
	SetRoleID(id string)
	SetRoleName(name string)
	// RequestID returns the ID of the current request
	RequestID() string
	// Tx returns the currently active database transaction that can be used for DB query purpose
	Tx() *dbx.Tx
	// SetTx sets the database transaction
	SetTx(tx *dbx.Tx)
	// Rollback returns a value indicating whether the current database transaction should be rolled back
	Rollback() bool
	// SetRollback sets a value indicating whether the current database transaction should be rolled back
	SetRollback(bool)
	// Now returns the timestamp representing the time when the request is being processed
	Now() time.Time
}

type requestScope struct {
	Logger              // the logger tagged with the current request information
	now       time.Time // the time when the request is being processed
	requestID string    // an ID identifying one or multiple correlated HTTP requests
	userID    int       // an ID identifying the current user
	companyID int       // an company ID identifying the current user
	roleID    int       // an role ID identifying the current user
	roleName  string
	rollback  bool    // whether to roll back the current transaction
	tx        *dbx.Tx // the currently active transaction
}

func (rs *requestScope) UserID() int {
	return rs.userID
}

func (rs *requestScope) CompanyID() int {
	return rs.companyID
}

func (rs *requestScope) RoleID() int {
	return rs.roleID
}

func (rs *requestScope) RoleName() string {
	return rs.roleName
}

func (rs *requestScope) SetUserID(id string) {
	rs.Logger.SetField("UserID", id)
	val, _ := strconv.Atoi(id)
	rs.userID = val
}

func (rs *requestScope) SetCompanyID(id string) {
	rs.Logger.SetField("CompanyID", id)
	val, _ := strconv.Atoi(id)
	rs.companyID = val
}

func (rs *requestScope) SetRoleID(id string) {
	rs.Logger.SetField("RoleID", id)
	val, _ := strconv.Atoi(id)
	rs.roleID = val
}

func (rs *requestScope) SetRoleName(name string) {
	rs.Logger.SetField("RoleName", name)
	rs.roleName = name
}

func (rs *requestScope) RequestID() string {
	return rs.requestID
}

func (rs *requestScope) Tx() *dbx.Tx {
	return rs.tx
}

func (rs *requestScope) SetTx(tx *dbx.Tx) {
	rs.tx = tx
}

func (rs *requestScope) Rollback() bool {
	return rs.rollback
}

func (rs *requestScope) SetRollback(v bool) {
	rs.rollback = v
}

func (rs *requestScope) Now() time.Time {
	return rs.now
}

// newRequestScope creates a new RequestScope with the current request information.
func newRequestScope(now time.Time, logger *logrus.Logger, request *http.Request) RequestScope {
	l := NewLogger(logger, logrus.Fields{})
	requestID := request.Header.Get("X-Request-Id")
	if requestID != "" {
		l.SetField("RequestID", requestID)
	}
	return &requestScope{
		Logger:    l,
		now:       now,
		requestID: requestID,
	}
}
