/*
 * Copyright (c) 2013 IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Seth Hoenig
 *    Allan Stockdill-Mander
 *    Mike Robertson
 */

package app

import (
	"context"
	"encoding/binary"
	"fmt"
	"math"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	//"log"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	dbx "github.com/go-ozzo/ozzo-dbx"
	"github.com/lectrotel-api/models"
	"go.mongodb.org/mongo-driver/mongo"
)

func onMessageReceived(client MQTT.Client, message MQTT.Message) {
	clientJobs := make(chan models.ClientJob)
	go generateResponses(clientJobs)

	// fmt.Printf("Received message on topic: %s\nMessage: %s\n", message.Topic(), message.Payload())

	if os.Getenv("GO_ENV") == "production" {
		processRequest(client, message.Payload(), message.Topic(), clientJobs)
	}

}

// SubscriptionMQTTServer ...
func SubscriptionMQTTServer() {
	topic := "gw/#"
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	connOpts, _, qos, err := InitMQTTConnection()
	if err != nil {
		return
	}
	connOpts.OnConnect = func(c MQTT.Client) {
		if token := c.Subscribe(topic, byte(qos), onMessageReceived); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}
	}

	<-c
}

func generateResponses(clientJobs chan models.ClientJob) {
	for {
		// Wait for the next job to come off the queue.
		clientJob := <-clientJobs

		// Do something thats keeps the CPU busy for a whole second.
		// for start := time.Now(); time.Now().Sub(start) < time.Second; {
		// core.SaveData(clientJob.DeviceData)
		// }
		fmt.Println(clientJob)
		// save device data
		saveDeviceData(clientJob.DeviceData, MongoDB)
		go saveStreamingDeviceData(clientJob)
	}
}

func processRequest(client MQTT.Client, b []byte, topic string, clientJobs chan models.ClientJob) {
	fmt.Println(topic)
	if topic != "gw/pgw1/b" {
		if strings.Contains(topic, "gw/pgw") {
			ProcessPrepaidRequest(client, b, topic, clientJobs)
		} else {
			ProcessPostpaidRequest(client, b, topic, clientJobs)
		}
	}
}

func float32frombytes(bytes []byte) float32 {
	bits := binary.BigEndian.Uint32(bytes)
	float := math.Float32frombits(bits)
	return float
}

func float64bytes(float float64) []byte {
	bits := math.Float64bits(float)
	bytes := make([]byte, 8)
	binary.LittleEndian.PutUint64(bytes, bits)
	return bytes
}

func hasBit(n int, pos uint) bool {
	val := n & (1 << pos)
	return (val > 0)
}

// SaveDeviceData ...
func saveDeviceData(d models.DeviceData, db *mongo.Database) error {
	meterid := d.MeterID
	collection := db.Collection("data_" + strconv.FormatInt(int64(meterid), 10))
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err := collection.InsertOne(ctx, d)
	return err
}

// save sent messages
func saveStreamingDeviceData(m models.ClientJob) {
	// fmt.Println(m)
	// DBCon.Insert("device_data", dbx.Params{
	// 	"message_id":      m.DeviceData.MessageID,
	// 	"byte_count":      m.DeviceData.ByteCount,
	// 	"gateway_id":      m.DeviceData.GatewayID,
	// 	"modbus_id":       m.DeviceData.ModbusID,
	// 	"energy_reading":  m.DeviceData.EnergyReading,
	// 	"remaining_units": m.DeviceData.RemainingUnits,
	// 	"online":          m.DeviceData.Online,
	// 	"relay_status":    m.DeviceData.RelayStatus,
	// }).Execute()
	// where := dbx.HashExp{"modbus_id": m.DeviceData.ModbusID}
	// if m.DeviceData.MeterID > 0 {
	where := dbx.HashExp{"meter_number": m.DeviceData.MeterID}
	if m.DeviceData.EnergyReading == 0 && m.DeviceData.ImportActiveEnergy > 0 {
		m.DeviceData.EnergyReading = m.DeviceData.ImportActiveEnergy
	}
	// }

	// Update meter last seen details
	DBCon.Update("meter_details", dbx.Params{
		"last_seen":       time.Now(),
		"current_reading": m.DeviceData.EnergyReading,
		"remaining_units": m.DeviceData.RemainingUnits,
	}, where).Execute()

}
