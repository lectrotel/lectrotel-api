package app

import (
	"crypto/sha1"
	"fmt"
	"io"
	"math"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/rs/xid"
	"github.com/sony/sonyflake"
	"github.com/speps/go-hashids"
	"github.com/spf13/viper"
)

// Config stores the application-wide configurations
var Config appConfig

type appConfig struct {
	// the path to the error message file. Defaults to "config/errors.yaml"
	ErrorFile string `mapstructure:"error_file"`
	// the server port. Defaults to 8080
	ServerPort      int `mapstructure:"server_port"`
	ServerPortHTTPS int `mapstructure:"server_port_https"`
	// the data source name (DSN) for connecting to the database. required.
	LocalDSN  string `mapstructure:"local_dsn"`
	ServerDSN string `mapstructure:"server_dns"`
	// the signing method for JWT. Defaults to "HS256"
	JWTSigningMethod string `mapstructure:"jwt_signing_method"`
	// JWT signing key. required.
	JWTSigningKey string `mapstructure:"jwt_signing_key"`
	// JWT verification key. required.
	JWTVerificationKey string `mapstructure:"jwt_verification_key"`
	// SMTP
	// MailHost = "webmail.digitaljungle.co.ke"
	MailHost          string `mapstructure:"mail_host"`
	MailPort          int    `mapstructure:"mail_port"`
	MailUsername      string `mapstructure:"mail_user"`
	MailPassword      string `mapstructure:"mail_password"`
	DefaultMailSender string `mapstructure:"default_mail_sender"`
	//VerificationLink - link to redirect to.
	VerificationLink string `mapstructure:"verification_link"`

	//africa_talking_api_key
	AfricaTalkingAPIKey   string `mapstructure:"africa_talking_api_key"`
	AfricaTalkingUsername string `mapstructure:"africa_talking_username"`
	AfricaTalkingENV      string `mapstructure:"africa_talking_env"`

	MongoDBURL  string `mapstructure:"mongo_db_url"`
	MongoDBName string `mapstructure:"mongo_db_name"`

	// calinhost
	CalinURL             string `mapstructure:"calin_url"`
	CalinElecURL         string `mapstructure:"calin_elec_url"`
	CalinCompanyName     string `mapstructure:"calin_company_name"`
	CalinElecCompanyName string `mapstructure:"calin_elec_company_name"`
	CalinUsername        string `mapstructure:"calin_user_name"`
	CalinPassword        string `mapstructure:"calin_password"`
	CalinPasswordVend    string `mapstructure:"calin_password_vend"`
	CalinIsVendByUnit    bool   `mapstructure:"calin_is_vend_by_unit"`
}

func (config appConfig) Validate() error {
	return validation.ValidateStruct(&config,
		validation.Field(&config.LocalDSN, validation.Required),
		validation.Field(&config.ServerDSN, validation.Required),
		validation.Field(&config.JWTSigningKey, validation.Required),
		validation.Field(&config.JWTVerificationKey, validation.Required),
	)
}

// LoadConfig loads configuration from the given list of paths and populates it into the Config variable.
// The configuration file(s) should be named as app.yaml.
// Environment variables with the prefix "RESTFUL_" in their names are also read automatically.
func LoadConfig(configPaths ...string) error {
	v := viper.New()
	v.SetConfigName("app")
	v.SetConfigType("yaml")
	v.SetEnvPrefix("restful")
	v.AutomaticEnv()
	v.SetDefault("error_file", "config/errors.yaml")
	v.SetDefault("server_port", 8088)
	v.SetDefault("jwt_signing_method", "HS256")
	for _, path := range configPaths {
		v.AddConfigPath(path)
	}
	if err := v.ReadInConfig(); err != nil {
		return fmt.Errorf("Failed to read the configuration file: %s", err)
	}
	if err := v.Unmarshal(&Config); err != nil {
		return err
	}
	return Config.Validate()
}

// GenerateNewID Generate new id using
// Note: this is base16, could shorten by encoding as base62 string
// fmt.Printf("github.com/sony/sonyflake:   %x\n", id)
func GenerateNewID() uint64 {
	flake := sonyflake.NewSonyflake(sonyflake.Settings{})
	id, _ := flake.NextID()
	return id
}

// GenerateNewStringID ...
func GenerateNewStringID() string {
	guid := xid.New()
	hd := hashids.NewData()
	hd.Salt = guid.String()
	hd.MinLength = 10
	hd.Alphabet = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789"
	h, _ := hashids.NewWithData(hd)
	e, _ := h.Encode([]int{45, 434, 1313})

	return e
}

// CalculatePassHash calculate password hash usin paswword and salt
func CalculatePassHash(pass, salt string) string {
	h := sha1.New()
	io.WriteString(h, salt)
	io.WriteString(h, pass)
	return fmt.Sprintf("%x", h.Sum(nil))
}

// Round ...
func Round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

// ToFixed ...
func ToFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(Round(num*output)) / output
}
