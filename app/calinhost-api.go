package app

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/lectrotel-api/models"
)

func httpPostJSON(url string, jsonStr []byte) ([]byte, error) {
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonStr))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

// GetToken ...
func GetToken(meterno, tokentype string, units float32, key, userid string) (rd models.ResponseData, err error) {
	url := "http://www.calinhost.com:6002/api/token"

	serialid := GenerateNewStringID()
	if userid == "" {
		userid = "BE"
	}

	if key == "" {
		key = "88dc79c2a59245379c3836cda14f7003"
	}

	timestamp := time.Now().UnixNano() / 1000000
	clearText := serialid + userid + meterno + tokentype + strconv.FormatFloat(float64(units), 'f', 0, 64) + strconv.FormatInt(timestamp, 10) + key
	ciphertext := getMD5Hash(clearText)
	fmt.Println(ciphertext)

	param := models.RequestData{
		SerialID:   serialid,
		UserID:     userid,
		MeterID:    meterno,
		TokenType:  tokentype,
		Amount:     strconv.FormatFloat(float64(units), 'f', 0, 64),
		Timestamp:  strconv.FormatInt(timestamp, 10),
		Ciphertext: ciphertext,
	}
	jsonStr, err := json.Marshal(param)
	if err != nil {
		return rd, err
	}
	resultArray, err := httpPostJSON(url, jsonStr)
	if err != nil {
		return rd, err
	}

	if err := json.Unmarshal(resultArray, &rd); err != nil {
		return rd, err
	}

	return rd, err
}

// GetToken2 ...
func GetToken2(meterno string, units float32, metertype int) (rd models.CalinResponseData, err error) {
	var company = Config.CalinCompanyName
	var url = Config.CalinURL
	if metertype == 1 {
		company = Config.CalinElecCompanyName
		url = Config.CalinElecURL
	}

	param := models.CalinRequestData{
		CompanyName:  company,
		Username:     Config.CalinUsername,
		Password:     Config.CalinPassword,
		PasswordVend: Config.CalinPasswordVend,
		Amount:       units,
		MeterNumber:  meterno,
		IsVendByUnit: true,
	}

	jsonStr, err := json.Marshal(param)
	if err != nil {
		return rd, err
	}
	resultArray, err := httpPostJSON(url, jsonStr)
	fmt.Println(metertype, err, url, param)
	if err != nil {
		return rd, err
	}

	if err := json.Unmarshal(resultArray, &rd); err != nil {
		return rd, err
	}

	return rd, err
}

func getMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}
