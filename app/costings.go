package app

import (
	"fmt"

	"github.com/lectrotel-api/models"
)

func CalculatePrepaidCostings(model *models.PricePlan) (float64, error) {
	var units float64
	var totalAmount float64
	units = 1

	if model.FixedCharge > 0.00 {
		totalAmount = totalAmount + ToFixed(model.FixedCharge, 4)
	}

	if model.EnergyCharge > 0.00 {
		totalAmount = totalAmount + ToFixed((units*model.EnergyCharge), 4)
		fmt.Println(ToFixed(units*model.EnergyCharge, 4))
	}

	if model.FuelCostCharge > 0.00 {
		totalAmount = totalAmount + ToFixed((units*model.FuelCostCharge), 4)
		fmt.Println(ToFixed(units*model.FuelCostCharge, 4))
	}

	if model.ForexAdj > 0.00 {
		totalAmount = totalAmount + ToFixed((units*model.ForexAdj), 4)
		fmt.Println(ToFixed(units*model.ForexAdj, 4))
	}

	if model.InflationAdj > 0.00 {
		totalAmount = totalAmount + ToFixed((units*model.InflationAdj), 4)
		fmt.Println(ToFixed(units*model.InflationAdj, 4))
	}

	if model.ERCLevy > 0.00 {
		totalAmount = totalAmount + ToFixed((units*model.ERCLevy), 4)
		fmt.Println(ToFixed(units*model.ERCLevy, 4))
	}

	if model.WarmaLevy > 0.00 {
		totalAmount = totalAmount + ToFixed((units*model.WarmaLevy), 4)
		fmt.Println(ToFixed(units*model.WarmaLevy, 4))
	}

	if model.REPLevy > 0.00 {
		totalAmount = totalAmount + ToFixed((model.REPLevy*model.EnergyCharge*units/100), 4)
		fmt.Println(ToFixed((model.REPLevy * model.EnergyCharge * units / 100), 4))
	}

	if model.PowerFactorSurcharge > 0.00 {
		totalAmount = totalAmount + ToFixed(model.PowerFactorSurcharge, 4)
	}

	if model.VAT > 0.00 {
		fmt.Println(ToFixed(totalAmount*model.VAT/100, 4))
		totalAmount = totalAmount + ToFixed((totalAmount*model.VAT/100), 4)
	}

	fmt.Println(totalAmount)

	return totalAmount, nil
}

// CalculatePostPaidElectricCostings ...
func CalculatePostPaidElectricCostings(units float64, model *models.PricePlan) (float64, error) {
	var totalAmount float64

	if model.FixedCharge > 0.00 {
		totalAmount = totalAmount + ToFixed(model.FixedCharge, 4)
	}

	if model.EnergyCharge > 0.00 {
		totalAmount = totalAmount + ToFixed((units*model.EnergyCharge), 4)
	}

	if model.FuelCostCharge > 0.00 {
		totalAmount = totalAmount + ToFixed((units*model.FuelCostCharge), 4)
	}

	if model.ForexAdj > 0.00 {
		totalAmount = totalAmount + ToFixed((units*model.ForexAdj), 4)
	}

	if model.InflationAdj > 0.00 {
		totalAmount = totalAmount + ToFixed((units*model.InflationAdj), 4)
	}

	if model.ERCLevy > 0.00 {
		totalAmount = totalAmount + ToFixed((units*model.ERCLevy), 4)
	}

	if model.WarmaLevy > 0.00 {
		totalAmount = totalAmount + ToFixed((units*model.WarmaLevy), 4)
	}

	if model.REPLevy > 0.00 {
		totalAmount = totalAmount + ToFixed((model.REPLevy*model.EnergyCharge*units/100), 4)
	}

	if model.PowerFactorSurcharge > 0.00 {
		totalAmount = totalAmount + ToFixed(model.PowerFactorSurcharge, 4)
	}

	if model.VAT > 0.00 {
		totalAmount = totalAmount + ToFixed((totalAmount*model.VAT/100), 4)
	}

	return ToFixed(totalAmount, 2), nil
}

// CalculateWaterCostings - calculate water costings
func CalculateWaterCostings(amount float64, model *models.WaterPricePlan) (unitamount float64, units float32) {
	//var units float64
	// var totalAmount float64
	if model.VAT > 0 {
		amount = amount - (model.VAT * amount / 100)
		fmt.Println("VAT=", amount)
	}

	if model.FixedCharge > 0.00 {
		amount = amount - model.FixedCharge
	}

	if model.Sewage > 0.00 {
		amount = amount - model.Sewage
	}

	if model.RegulatoryLevy > 0.00 {
		amount = amount - model.RegulatoryLevy
	}

	units = float32(ToFixed((amount / model.ChargePerUnit), 2))
	return amount, units
}
