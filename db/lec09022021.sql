-- --------------------------------------------------------
-- Host:                         178.62.75.148
-- Server version:               10.4.12-MariaDB-1:10.4.12+maria~bionic-log - mariadb.org binary distribution
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table lectrotel_portal.account_details
DROP TABLE IF EXISTS `account_details`;
CREATE TABLE IF NOT EXISTS `account_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(100) NOT NULL,
  `account_description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.account_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `account_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_details` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.companies
DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(200) NOT NULL,
  `company_alias` varchar(200) NOT NULL,
  `company_phone` varchar(100) NOT NULL,
  `company_address` varchar(250) NOT NULL,
  `company_email` varchar(200) NOT NULL,
  `company_location` varchar(200) NOT NULL,
  `company_logo_url` varchar(1000) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.companies: ~2 rows (approximately)
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` (`id`, `company_name`, `company_alias`, `company_phone`, `company_address`, `company_email`, `company_location`, `company_logo_url`, `updated_at`, `created_at`, `added_by`, `deleted_at`) VALUES
	(1, 'LECTROTEL M', 'LECTROTEL', '07', '', 'info@lectrotel.com', 'NAIROBI', 'lectrotel-small.png', '2020-04-21 21:00:35', '2020-04-21 21:00:35', 1, NULL),
	(4, 'ROVAS LTD', 'TWO RIVERS', '1234566', 'P.O. Box 1545 - 40100', 'info@rovas.com', 'Kisumu', 'rovas.png', '2020-07-07 19:44:51', '2020-07-07 19:44:51', 0, NULL);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.customers
DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) NOT NULL,
  `id_no` varchar(20) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `meter_no` varchar(20) DEFAULT NULL,
  `desc` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.customers: ~2 rows (approximately)
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`id`, `full_name`, `id_no`, `phone_no`, `email`, `created_on`, `status`, `meter_no`, `desc`) VALUES
	(1, 'omollo erick', '', '', '', '2019-06-27 06:48:38', 0, '47000807629', 'C4'),
	(2, 'Lectrotel', '', '', '', '2019-06-27 06:49:34', 0, '14253197645', 'C4');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.device_data
DROP TABLE IF EXISTS `device_data`;
CREATE TABLE IF NOT EXISTS `device_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL,
  `byte_count` int(11) NOT NULL,
  `gateway_id` int(11) NOT NULL,
  `modbus_id` int(11) NOT NULL,
  `energy_reading` decimal(10,2) NOT NULL,
  `remaining_units` decimal(10,2) NOT NULL,
  `online` tinyint(4) NOT NULL,
  `relay_status` tinyint(4) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.device_data: ~0 rows (approximately)
/*!40000 ALTER TABLE `device_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `device_data` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.gateway_details
DROP TABLE IF EXISTS `gateway_details`;
CREATE TABLE IF NOT EXISTS `gateway_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `section_id` int(10) unsigned NOT NULL,
  `gateway_name` varchar(100) NOT NULL,
  `gateway_description` varchar(45) DEFAULT NULL,
  `company_id` int(11) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_on` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.gateway_details: ~4 rows (approximately)
/*!40000 ALTER TABLE `gateway_details` DISABLE KEYS */;
INSERT INTO `gateway_details` (`id`, `section_id`, `gateway_name`, `gateway_description`, `company_id`, `added_by`, `created_on`) VALUES
	(1, 6, 'Gateway 1', 'Gateway 1', 4, 0, '2019-09-11 11:32:12'),
	(2, 6, 'Gateway 2', 'Gateway 2', 4, 0, '2019-09-11 12:31:46'),
	(6, 6, 'Gateway 3', 'Gateway 3', 4, 13, '2020-10-02 20:26:26'),
	(9, 6, '4', 'Lectrotel test', 1, 16, '2021-01-08 14:50:46');
/*!40000 ALTER TABLE `gateway_details` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.invoices
DROP TABLE IF EXISTS `invoices`;
CREATE TABLE IF NOT EXISTS `invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `meter_id` int(10) unsigned NOT NULL DEFAULT 0,
  `meter_number` varchar(50) NOT NULL DEFAULT '0',
  `invoice_number` varchar(50) NOT NULL DEFAULT '0',
  `invoice_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `previous_balance` decimal(10,2) NOT NULL DEFAULT 0.00,
  `previous_reading` decimal(10,2) NOT NULL DEFAULT 0.00,
  `current_reading` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total_consumption` decimal(10,2) NOT NULL DEFAULT 0.00,
  `invoice_status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 = Pending, 1= Paid, 2 = Cancelled',
  `created_on` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.invoices: ~5 rows (approximately)
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
INSERT INTO `invoices` (`id`, `meter_id`, `meter_number`, `invoice_number`, `invoice_amount`, `previous_balance`, `previous_reading`, `current_reading`, `total_consumption`, `invoice_status`, `created_on`) VALUES
	(3, 9, '1000001', 'INV3657', 2161.28, 0.00, 0.00, 106.54, 0.00, 0, '2020-10-13 19:57:10'),
	(6, 9, '1000001', 'INV8814', 333714.97, -30.00, 0.00, 14749.17, 0.00, 0, '2020-12-05 10:42:22'),
	(9, 21, '1000002', 'INV1093', 40017.17, -30.00, 0.00, 1755.86, 0.00, 0, '2020-12-05 10:45:41'),
	(12, 9, '1000001', 'INV4634', 169746.23, 333684.97, 14749.17, 22197.23, 0.00, 0, '2021-01-05 06:44:35'),
	(15, 21, '1000002', 'INV9875', 63488.01, 39987.17, 1755.86, 4541.56, 0.00, 0, '2021-01-05 07:25:11');
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.invoice_items
DROP TABLE IF EXISTS `invoice_items`;
CREATE TABLE IF NOT EXISTS `invoice_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` int(10) unsigned NOT NULL DEFAULT 0,
  `invoice_number` varchar(50) NOT NULL,
  `total_fixed_charge` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total_energy_charge` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total_fuel_cost_charge` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total_forex_adj` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total_inflation_adj` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total_warma_levy` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total_erc_levy` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total_rep_levy` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total_power_factor_surcharge` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total_vat` int(11) NOT NULL DEFAULT 0,
  `total_invoice_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.invoice_items: ~5 rows (approximately)
/*!40000 ALTER TABLE `invoice_items` DISABLE KEYS */;
INSERT INTO `invoice_items` (`id`, `invoice_id`, `invoice_number`, `total_fixed_charge`, `total_energy_charge`, `total_fuel_cost_charge`, `total_forex_adj`, `total_inflation_adj`, `total_warma_levy`, `total_erc_levy`, `total_rep_levy`, `total_power_factor_surcharge`, `total_vat`, `total_invoice_amount`, `created_on`) VALUES
	(30, 3, 'INV3657', 0.00, 1258.16, 428.71, 113.70, 48.46, 2.80, 2.80, 8.56, 0.00, 298, 0.00, '2020-10-13 19:57:10'),
	(33, 6, 'INV8814', 0.00, 197675.44, 67356.08, 17864.00, 7614.17, 439.28, 439.28, 1344.19, 0.00, 40276, 0.00, '2020-12-05 10:42:22'),
	(36, 9, 'INV1093', 0.00, 23704.10, 8076.95, 2142.15, 913.05, 52.68, 52.68, 161.19, 0.00, 4830, 0.00, '2020-12-05 10:45:41'),
	(39, 12, 'INV4634', 0.00, 100548.87, 34261.10, 9086.64, 3872.99, 223.44, 223.44, 683.73, 0.00, 20487, 0.00, '2021-01-05 06:44:35'),
	(42, 15, 'INV9875', 0.00, 37607.00, 12814.24, 3398.56, 1448.57, 83.57, 83.57, 255.73, 0.00, 7662, 0.00, '2021-01-05 07:25:11');
/*!40000 ALTER TABLE `invoice_items` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.meter_details
DROP TABLE IF EXISTS `meter_details`;
CREATE TABLE IF NOT EXISTS `meter_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tariff_id` int(10) unsigned NOT NULL DEFAULT 0,
  `gateway_id` int(10) unsigned DEFAULT 0,
  `company_id` int(10) unsigned DEFAULT 0,
  `meter_number` varchar(100) NOT NULL,
  `modbus_id` int(11) NOT NULL,
  `meter_short_number` varchar(100) NOT NULL,
  `meter_type` varchar(20) NOT NULL DEFAULT 'elec',
  `meter_type_id` int(10) unsigned NOT NULL DEFAULT 2,
  `last_read` datetime NOT NULL DEFAULT current_timestamp(),
  `previous_reading` decimal(10,3) NOT NULL DEFAULT 0.000,
  `current_consumption` decimal(10,3) NOT NULL DEFAULT 0.000,
  `current_reading` decimal(10,3) NOT NULL DEFAULT 0.000,
  `remaining_units` decimal(10,3) NOT NULL DEFAULT 0.000,
  `balance` decimal(10,2) NOT NULL DEFAULT 0.00,
  `amount_payable` decimal(10,2) NOT NULL DEFAULT 0.00,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_seen` datetime NOT NULL DEFAULT current_timestamp(),
  `added_by` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.meter_details: ~5 rows (approximately)
/*!40000 ALTER TABLE `meter_details` DISABLE KEYS */;
INSERT INTO `meter_details` (`id`, `tariff_id`, `gateway_id`, `company_id`, `meter_number`, `modbus_id`, `meter_short_number`, `meter_type`, `meter_type_id`, `last_read`, `previous_reading`, `current_consumption`, `current_reading`, `remaining_units`, `balance`, `amount_payable`, `created_on`, `last_seen`, `added_by`) VALUES
	(9, 1, 2, 4, '1000001', 1000001, 'STP/Admin Block', 'elec', 2, '2020-10-01 13:21:37', 22197.232, 0.000, 31179.844, 0.000, 503390.19, 0.00, '2020-10-01 10:21:37', '2021-02-09 08:33:30', 13),
	(21, 1, 6, 4, '1000002', 1000002, 'Reverse Osmosis(RO) Plant', 'elec', 2, '2020-11-18 16:07:19', 4541.563, 0.000, 7959.672, 0.000, 103434.19, 0.00, '2020-11-18 16:07:19', '2021-02-09 07:46:00', 13),
	(24, 1, 6, 4, '1000003', 1000003, 'Fire Hydrant- Pump', 'elec', 2, '2020-11-19 18:48:17', 0.000, 0.000, 0.094, 0.000, -71.00, 0.00, '2020-11-19 18:48:17', '2021-02-09 08:30:03', 13),
	(30, 1, 9, 1, '6000001', 6000001, 'test1', 'elec', 2, '2021-01-11 07:38:15', 26.930, 0.000, 137.921, 0.000, -41.00, 0.00, '2021-01-11 07:38:15', '2021-01-29 17:10:58', 16),
	(33, 1, 9, 1, '6000002', 6000002, 'test2', 'elec', 2, '2021-01-11 07:41:24', 0.000, 0.000, 0.103, 0.000, -41.00, 0.00, '2021-01-11 07:41:24', '2021-01-29 17:11:05', 16);
/*!40000 ALTER TABLE `meter_details` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.meter_types
DROP TABLE IF EXISTS `meter_types`;
CREATE TABLE IF NOT EXISTS `meter_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `price_plan_id` int(10) unsigned NOT NULL,
  `type_name` varchar(45) DEFAULT NULL,
  `type_description` varchar(45) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.meter_types: ~2 rows (approximately)
/*!40000 ALTER TABLE `meter_types` DISABLE KEYS */;
INSERT INTO `meter_types` (`id`, `price_plan_id`, `type_name`, `type_description`, `created_on`) VALUES
	(1, 1, 'Prepaid Electricity', 'Prepaid Electricity', '2020-07-13 06:20:13'),
	(2, 1, 'Postpaid Electricity', 'Postpaid Electricity', '2020-07-13 06:21:59');
/*!40000 ALTER TABLE `meter_types` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.payments
DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `meter_id` int(10) unsigned NOT NULL DEFAULT 0,
  `transaction_type` varchar(30) NOT NULL,
  `trans_id` varchar(100) NOT NULL,
  `trans_time` varchar(30) NOT NULL,
  `trans_amount` decimal(10,2) NOT NULL,
  `business_short_code` varchar(30) NOT NULL,
  `bill_ref_number` varchar(100) NOT NULL,
  `trans_desc` varchar(250) NOT NULL DEFAULT 'Bill Payment',
  `invoice_number` varchar(30) NOT NULL,
  `org_account_bance` varchar(30) NOT NULL,
  `third_party_trans_id` varchar(100) NOT NULL,
  `msisdn` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `middle_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Pending',
  `merchant_request_id` varchar(100) DEFAULT NULL,
  `result_code` int(11) NOT NULL DEFAULT 1,
  `result_desc` varchar(500) DEFAULT NULL,
  `company_id` int(11) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.payments: ~12 rows (approximately)
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` (`id`, `meter_id`, `transaction_type`, `trans_id`, `trans_time`, `trans_amount`, `business_short_code`, `bill_ref_number`, `trans_desc`, `invoice_number`, `org_account_bance`, `third_party_trans_id`, `msisdn`, `first_name`, `middle_name`, `last_name`, `status`, `merchant_request_id`, `result_code`, `result_desc`, `company_id`, `added_by`) VALUES
	(3, 0, 'Mpesa', 'VLMIR6HNZN', '10-13-2020 20:12:32', 20.00, '174379', '9', 'Bill Payment', 'VLMIR6HNZN', '0', 'ws_CO_131020202312352327', '254722380945', '254722380945', '254722380945', '254722380945', 'Paid', '16458-212934019-2', 0, 'The service request is processed successfully.', 0, 0),
	(6, 0, 'Mpesa', 'NGWI7BCXQX', '11-07-2020 18:44:14', 10.00, '174379', '9', 'Bill Payment', 'NGWI7BCXQX', '0', 'ws_CO_071120202155004694', '254720172816', '254720172816', '254720172816', '254720172816', 'Pending', NULL, 1, NULL, 0, 0),
	(9, 9, 'Mpesa', '4GYIQ2T8P8', '11-19-2020 06:15:13', 10.00, '174379', '9', 'Bill Payment', '4GYIQ2T8P8', '0', 'ws_CO_191120200915134133', '254722380945', '254722380945', '254722380945', '254722380945', 'Pending', NULL, 1, NULL, 4, 0),
	(12, 9, 'Cash', 'EX4IL2TLJL', '11-19-2020 10:49:00', 100.00, '174379', '9', 'Bill Payment', 'EX4IL2TLJL', '0', '', '2540', '2540', '2540', '2540', 'Paid', NULL, 0, 'The service request is processed successfully.', 4, 13),
	(15, 0, 'Mpesa', '6WNIWQU6D6', '11-19-2020 10:49:33', 100.00, '174379', '9', 'Bill Payment', '6WNIWQU6D6', '0', 'ws_CO_191120201049363939', '254723436438', '254723436438', '254723436438', '254723436438', 'Paid', '7428-3035555-2', 0, 'The service request is processed successfully.', 0, 0),
	(18, 9, 'Mpesa', 'MNDIBET3D3', '11-20-2020 13:21:41', 10.00, '174379', '9', 'Bill Payment', 'MNDIBET3D3', '0', 'ws_CO_201120201621442792', '254722380945', '254722380945', '254722380945', '254722380945', 'Paid', '4239-1031961-2', 0, 'The service request is processed successfully.', 4, 13),
	(21, 21, 'Cash', '4BYI1ZC8Q8', '11-20-2020 13:24:37', 10.00, '174379', '21', 'Bill Payment', '4BYI1ZC8Q8', '0', '', '00', '00', '00', '00', 'Paid', NULL, 0, 'The service request is processed successfully.', 4, 13),
	(24, 9, 'Mpesa', 'B4QIKQSYQY', '11-20-2020 13:34:00', 10.00, '174379', '9', 'Bill Payment', 'B4QIKQSYQY', '0', 'ws_CO_201120201634029794', '254722831184', '254722831184', '254722831184', '254722831184', 'Paid', '28964-1038456-2', 0, 'The service request is processed successfully.', 4, 13),
	(27, 9, 'Mpesa', 'Y2GF6LUKBK', '01-18-2021 14:44:34', 20.00, '174379', '9', 'Bill Payment', 'Y2GF6LUKBK', '0', 'ws_CO_180120211744363893', '254724932640', '254724932640', '254724932640', '254724932640', 'Cancelled', '23659-12474189-2', 1032, 'Request cancelled by user', 4, 13),
	(30, 9, 'Mpesa', 'X5EFWXIQYQ', '01-18-2021 14:45:20', 1.00, '174379', '9', 'Bill Payment', 'X5EFWXIQYQ', '0', 'ws_CO_180120211745209254', '254722380945', '254722380945', '254722380945', '254722380945', 'Cancelled', '23659-12474189-6', 1032, 'Request cancelled by user', 4, 13),
	(33, 9, 'Mpesa', 'Q7BF48TDLD', '01-22-2021 13:08:15', 20.00, '174379', '9', 'Bill Payment', 'Q7BF48TDLD', '0', 'ws_CO_220120211608183020', '254721716447', '254721716447', '254721716447', '254721716447', 'Cancelled', '4660-1863736-2', 1032, 'Request cancelled by user', 4, 13),
	(36, 2, 'Mpesa', 'YN5F3XTJZJ', '02-08-2021 14:21:02', 500.00, '174379', '2', 'Bill Payment', 'YN5F3XTJZJ', '0', 'ws_CO_080220211721020366', '254723436438', '254723436438', '254723436438', '254723436438', 'Pending', NULL, 0, 'Incomplete Payment', 1, 6);
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.price_plan
DROP TABLE IF EXISTS `price_plan`;
CREATE TABLE IF NOT EXISTS `price_plan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tariff_id` int(10) unsigned NOT NULL DEFAULT 0,
  `fixed_charge` decimal(10,2) NOT NULL DEFAULT 0.00,
  `energy_charge` decimal(10,2) NOT NULL DEFAULT 0.00,
  `fuel_cost_charge` decimal(10,2) NOT NULL DEFAULT 0.00,
  `forex_adj` decimal(10,2) NOT NULL DEFAULT 0.00,
  `inflation_adj` decimal(10,2) NOT NULL DEFAULT 0.00,
  `warma_levy` decimal(10,2) NOT NULL DEFAULT 0.00,
  `erc_levy` decimal(10,2) NOT NULL DEFAULT 0.00,
  `rep_levy` decimal(10,2) NOT NULL DEFAULT 0.00,
  `power_factor_surcharge` decimal(10,2) NOT NULL DEFAULT 0.00,
  `vat` int(11) NOT NULL DEFAULT 0,
  `updated_on` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `tariff_id_UNIQUE` (`tariff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.price_plan: ~1 rows (approximately)
/*!40000 ALTER TABLE `price_plan` DISABLE KEYS */;
INSERT INTO `price_plan` (`id`, `tariff_id`, `fixed_charge`, `energy_charge`, `fuel_cost_charge`, `forex_adj`, `inflation_adj`, `warma_levy`, `erc_levy`, `rep_levy`, `power_factor_surcharge`, `vat`, `updated_on`) VALUES
	(3, 1, 0.00, 13.50, 4.60, 1.22, 0.52, 0.03, 0.03, 0.68, 0.00, 14, '2020-07-15 07:20:30');
/*!40000 ALTER TABLE `price_plan` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.price_plan_tariff
DROP TABLE IF EXISTS `price_plan_tariff`;
CREATE TABLE IF NOT EXISTS `price_plan_tariff` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plan_tariff_name` varchar(45) NOT NULL,
  `plan_tariff_type` varchar(45) NOT NULL DEFAULT 'electricity',
  `plan_tariff_description` varchar(450) DEFAULT '',
  `plan_tariff_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `plan_status` tinyint(1) NOT NULL DEFAULT 1,
  `update_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.price_plan_tariff: ~1 rows (approximately)
/*!40000 ALTER TABLE `price_plan_tariff` DISABLE KEYS */;
INSERT INTO `price_plan_tariff` (`id`, `plan_tariff_name`, `plan_tariff_type`, `plan_tariff_description`, `plan_tariff_amount`, `plan_status`, `update_on`) VALUES
	(1, 'Postpaid Electricity', 'electricity', 'Postpaid Electricity', 0.00, 1, '2020-10-04 19:52:12');
/*!40000 ALTER TABLE `price_plan_tariff` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.price_plan_water
DROP TABLE IF EXISTS `price_plan_water`;
CREATE TABLE IF NOT EXISTS `price_plan_water` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tariff_id` int(11) NOT NULL,
  `fixed_charge` decimal(10,2) NOT NULL,
  `charge_per_unit` decimal(10,2) NOT NULL,
  `vat` int(11) NOT NULL DEFAULT 0,
  `sewage` decimal(10,2) NOT NULL DEFAULT 0.00,
  `regulatory_levy` decimal(10,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.price_plan_water: ~1 rows (approximately)
/*!40000 ALTER TABLE `price_plan_water` DISABLE KEYS */;
INSERT INTO `price_plan_water` (`id`, `tariff_id`, `fixed_charge`, `charge_per_unit`, `vat`, `sewage`, `regulatory_levy`) VALUES
	(1, 3, 200.00, 110.00, 0, 0.00, 0.00);
/*!40000 ALTER TABLE `price_plan_water` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.request_token
DROP TABLE IF EXISTS `request_token`;
CREATE TABLE IF NOT EXISTS `request_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` varchar(20) NOT NULL,
  `total_units_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `trans_id` varchar(20) NOT NULL,
  `meter_id` varchar(50) NOT NULL,
  `units` decimal(10,2) NOT NULL,
  `token_type` varchar(50) NOT NULL,
  `serial_id` varchar(50) NOT NULL,
  `result_code` varchar(20) NOT NULL,
  `result` varchar(100) NOT NULL,
  `reason` varchar(250) NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.request_token: ~122 rows (approximately)
/*!40000 ALTER TABLE `request_token` DISABLE KEYS */;
INSERT INTO `request_token` (`id`, `amount`, `total_units_amount`, `trans_id`, `meter_id`, `units`, `token_type`, `serial_id`, `result_code`, `result`, `reason`, `created_on`) VALUES
	(1, '500', 120.00, '', '2', 1.09, 'CreditTokenT1', 'D87H62UX4X', '00', 'Success', 'ok', '2019-10-03 00:06:24'),
	(2, '500', 120.00, 'Q1GHMNIZRZ', '2', 1.09, 'CreditTokenT1', '86WH3DCNXN', '00', '54512575840333718248', 'ok', '2019-10-03 00:10:06'),
	(3, '500', 120.00, 'BV1H59CWRW', '2', 1.09, 'CreditTokenT1', '3XLHW6C959', '00', '31592330712780923412', 'ok', '2019-10-03 00:11:05'),
	(4, '1000', 540.00, 'X2WH93ILZL', '2', 4.91, 'CreditTokenT1', 'PRQHYKU8X8', '00', '35615567046910599302', 'ok', '2019-10-03 01:46:16'),
	(5, '477', 100.68, 'EPBHVJS2J2', '2', 0.92, 'CreditTokenT1', 'DWXH5LU9E9', '00', '70541745367064207306', 'ok', '2019-10-03 02:02:17'),
	(6, '1000', 540.00, 'NZEH5DULVL', '2', 4.91, 'CreditTokenT1', '8BMHY8UVXV', '00', '02841708424318077460', 'ok', '2019-10-03 02:06:29'),
	(7, '1000', 540.00, '9NYHK9SBEB', '2', 4.91, 'CreditTokenT1', 'XY9HNXA4X4', '00', '09154911590677604779', 'ok', '2019-10-03 02:08:04'),
	(8, '900', 456.00, 'RPLH29UJYJ', '2', 4.15, 'CreditTokenT1', '27ZHZGS4P4', '00', '56983019465045011295', 'ok', '2019-10-03 02:43:34'),
	(9, '700', 288.00, 'JNDHVKSNLN', '2', 2.62, 'CreditTokenT1', '9WQHW3U383', '00', '47041797187943121828', 'ok', '2019-10-03 02:46:12'),
	(10, '550', 162.00, 'NL3HJ8CPBP', '2', 1.47, 'CreditTokenT1', 'EGJHBEF5K5', '00', '44303142994306998147', 'ok', '2019-10-03 02:47:25'),
	(11, '50', 49.88, '3GNHPQH2N2', '1', 2.06, 'Prepaid Electricity', '', '0', 'Success', 'Success', '2019-10-03 23:10:36'),
	(12, '55', 54.97, 'NQMHBWU2M2', '1', 2.27, 'Prepaid Electricity', '', '0', 'ok', 'Success', '2019-10-03 23:20:43'),
	(13, '50', 49.88, 'WWXH5DILNL', '1', 2.06, 'Prepaid Electricity', 'ws_CO_DMZ_1479382011_03102019232326676', '0', 'Success', 'ok', '2019-10-03 23:23:57'),
	(14, '500', 120.00, 'GX8HJ3CB2B', '2', 1.09, 'CreditTokenT1', 'E9QHPEHXWX', '00', '57556083702476610869', 'ok', '2019-10-03 23:48:53'),
	(15, '600', 204.00, 'DZYHK3SV3V', '2', 1.85, 'CreditTokenT1', '34BHJNCN5N', '00', '56767750115403016893', 'ok', '2019-10-03 23:52:38'),
	(16, '500', 120.00, 'ZLEH5NUNBN', '2', 1.09, 'CreditTokenT1', 'J5VH4ZUM8M', '00', '23012182084584172773', 'ok', '2019-10-03 23:58:48'),
	(17, '20', 20.10, '9BJH9QF757', '1', 0.83, 'Prepaid Electricity', '', '0', 'Pending', 'Pending', '2019-10-04 00:55:27'),
	(18, '20', 20.10, 'ZGLHJ6TDYD', '1', 0.83, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-10-04 00:55:53'),
	(19, '50', 49.88, '19BHEKSK4K', '1', 2.06, 'Prepaid Electricity', '', '0', 'Pending', 'Pending', '2019-10-04 08:40:14'),
	(20, '50', 49.88, '7LBHJ4UENE', '1', 2.06, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-10-04 08:40:47'),
	(21, '50', 49.88, 'P47H8JTJLJ', '1', 2.06, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-10-06 13:22:40'),
	(22, '20', 20.10, '4DMHM6IPNP', '1', 0.83, 'Prepaid Electricity', '', '0', 'Pending', 'Pending', '2019-10-06 13:32:28'),
	(23, '20', 20.10, 'WMZHDJIEME', '1', 0.83, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-10-06 13:33:53'),
	(24, '500', 120.00, '77NH7QFRNR', '2', 1.09, 'CreditTokenT1', '4JGHG8F161', '00', '50711130566149170226', 'ok', '2019-10-06 13:38:15'),
	(25, '20', 20.10, 'QQ7H84TQVQ', '1', 0.83, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-10-06 13:40:00'),
	(26, '20', 20.10, 'GX2HKMSGLG', '1', 0.83, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-10-06 13:41:12'),
	(27, '19', 18.89, 'JZWH18FRXR', '1', 0.78, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-10-06 13:44:44'),
	(28, '20', 20.10, '12JHYBFDRD', '1', 0.83, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-10-06 13:46:54'),
	(29, '477', 100.68, 'PQDH8NTM4M', '2', 0.92, 'CreditTokenT1', 'MRVHJKTZYZ', '00', '14838352137257892115', 'ok', '2019-10-06 13:47:52'),
	(30, '20', 20.10, 'WN2H4PC545', '1', 0.83, 'Prepaid Electricity', 'ws_CO_DMZ_1573525860_07102019151915249', '0', 'Pending', 'Pending', '2019-10-07 12:19:43'),
	(31, '20', -283.20, 'N3WH4JUL6L', '2', -2.57, 'CreditTokenT1', 'GEJHZRSEGE', '04', '', 'invalid amount(0.1~10000.0)', '2019-10-07 12:33:58'),
	(32, '20', -283.20, 'E93HPMH636', '2', -2.57, 'CreditTokenT1', 'G5PHM9F6J6', '04', '', 'invalid amount(0.1~10000.0)', '2019-10-07 12:34:18'),
	(33, '20', -283.20, '1KNH79U474', '2', -2.57, 'CreditTokenT1', '6P2HPLHG7G', '04', '', 'invalid amount(0.1~10000.0)', '2019-10-07 12:34:29'),
	(34, '450', 78.00, '13VH4DC272', '2', 0.71, 'CreditTokenT1', 'Q5GHXYS3B3', '00', '07976076024653844559', 'ok', '2019-10-07 14:21:51'),
	(35, '20', 20.10, 'R4LHBRT9M9', '1', 0.83, 'Prepaid Electricity', 'ws_CO_DMZ_1577116376_07102019183640804', '0', 'Pending', 'Pending', '2019-10-07 18:37:09'),
	(36, '49', 48.91, 'V8PH8WU191', '1', 2.02, 'Prepaid Electricity', 'ws_CO_DMZ_1577146738_07102019183818007', '0', 'Success', 'ok', '2019-10-07 18:38:46'),
	(37, '48', 47.95, '9M6HV5SN5N', '1', 1.98, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-10-07 15:51:45'),
	(38, '44', 44.07, 'BZMHK4SYZY', '1', 1.82, 'Prepaid Electricity', 'ws_CO_DMZ_1578779602_07102019200755295', '0', 'Pending', 'Pending', '2019-10-07 20:08:24'),
	(39, '50', 49.88, 'ZGJH32F5P5', '1', 2.06, 'Prepaid Electricity', 'ws_CO_DMZ_1593905607_08102019100318822', '0', 'Success', 'ok', '2019-10-08 07:03:47'),
	(40, '400', 100.00, '6P4H3JFX5X', '2', 0.91, 'CreditTokenT1', 'N2GHX1SD1D', '00', '06306947361959077240', 'ok', '2019-10-08 09:32:24'),
	(41, '22', 22.04, 'GDEHZGSDPD', '1', 0.91, 'Prepaid Electricity', 'ws_CO_DMZ_405033484_08102019140218458', '0', 'Success', 'ok', '2019-10-08 14:01:36'),
	(42, '24', 23.97, 'VREH98IX2X', '1', 0.99, 'Prepaid Electricity', 'ws_CO_DMZ_405033796_08102019140322294', '0', 'Success', 'ok', '2019-10-08 14:02:39'),
	(43, '26', 25.91, 'Q5VHZNSDKD', '1', 1.07, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-10-08 14:08:33'),
	(44, '27', 27.12, 'YG9HYBC1Y1', '1', 1.12, 'Prepaid Electricity', 'ws_CO_DMZ_405035140_08102019141010225', '0', 'Success', 'ok', '2019-10-08 14:09:27'),
	(45, '29', 29.06, '6B4HK6SYGY', '1', 1.20, 'Prepaid Electricity', 'ws_CO_DMZ_1598501915_08102019141606238', '0', 'Pending', 'Pending', '2019-10-08 11:16:34'),
	(46, '28', 28.09, 'W94H5LURMR', '1', 1.16, 'Prepaid Electricity', 'ws_CO_DMZ_405037452_08102019142515187', '0', 'Pending', 'Pending', '2019-10-08 11:24:31'),
	(47, '22', 22.04, '9EWH7VU6P6', '1', 0.91, 'Prepaid Electricity', 'ws_CO_DMZ_405039755_08102019143856760', '0', 'Success', 'ok', '2019-10-08 14:38:13'),
	(48, '20', 20.10, 'Y2JH4ZUGPG', '1', 0.83, 'Prepaid Electricity', 'ws_CO_DMZ_1599017295_08102019144431051', '0', 'Success', 'ok', '2019-10-08 11:44:59'),
	(49, '500', 200.00, '2G8HYVUY3Y', '2', 1.82, 'CreditTokenT1', '7VPHRGFZVZ', '00', '03714341508958819166', 'ok', '2019-10-08 12:03:42'),
	(50, '600', 300.00, '97GHVKSZXZ', '2', 2.73, 'CreditTokenT1', 'PBYH17UNRN', '00', '71254042293579982819', 'ok', '2019-10-08 12:32:35'),
	(51, '20', 20.10, '22MH6DUYLY', '1', 0.83, 'Prepaid Electricity', 'ws_CO_DMZ_1616325733_09102019063928643', '0', 'Success', 'ok', '2019-10-09 03:39:56'),
	(52, '20', 20.10, '3JYH17C131', '1', 0.83, 'Prepaid Electricity', 'ws_CO_DMZ_405222213_09102019135511473', '0', 'Success', 'ok', '2019-10-09 10:54:31'),
	(53, '500', 200.00, 'WMDHP8H6P6', '2', 1.82, 'CreditTokenT1', 'EMGHWPCJMJ', '00', '29241104995017572390', 'ok', '2019-10-11 06:41:54'),
	(54, '200', 200.02, 'G43HD5I4G4', '1', 8.26, 'Prepaid Electricity', 'ws_CO_DMZ_405576498_11102019095609191', '0', 'Success', 'ok', '2019-10-11 06:55:34'),
	(55, '20', 20.10, 'ZWLHMZI686', '1', 0.83, 'Prepaid Electricity', 'ws_CO_DMZ_1906610072_20102019221102189', '0', 'Success', 'ok', '2019-10-20 19:11:28'),
	(56, '20', 20.10, 'PBMHDXFMVM', '1', 0.83, 'Prepaid Electricity', 'ws_CO_DMZ_2029045639_25102019224902610', '0', 'Success', 'ok', '2019-10-25 19:49:29'),
	(57, '500', 200.00, 'Q9LUZ9SZ1Z', '2', 1.82, 'CreditTokenT1', 'E96U5MIEJE', '00', '44482408810073550584', 'ok', '2019-11-05 07:37:13'),
	(58, '100', 100.01, '1JKUQ1F8E8', '1', 4.13, 'Prepaid Electricity', 'ws_CO_DMZ_2299733129_05112019115034070', '0', 'Success', 'ok', '2019-11-05 08:50:59'),
	(59, '100', 100.01, 'Y23U4LCBLB', '1', 4.13, 'Prepaid Electricity', 'ws_CO_DMZ_2301688734_05112019133752476', '0', 'Success', 'ok', '2019-11-05 13:38:23'),
	(60, '100', 100.01, 'GNPU7KCVRV', '1', 4.13, 'Prepaid Electricity', 'ws_CO_DMZ_2301989742_05112019135424276', '0', 'Success', 'ok', '2019-11-05 13:54:53'),
	(61, '100', 100.01, 'Z46UQJILJL', '1', 4.13, 'Prepaid Electricity', 'ws_CO_DMZ_2302687333_05112019143241821', '0', 'Success', 'ok', '2019-11-05 14:33:10'),
	(62, '300', 300.02, '6L8UD2HX1X', '1', 12.39, 'Prepaid Electricity', 'ws_CO_DMZ_2304214554_05112019155630474', '0', 'Success', 'ok', '2019-11-05 12:56:58'),
	(63, '500', 500.04, 'KKXU89TMEM', '1', 20.65, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 13:02:46'),
	(64, '500', 500.04, 'DLXU2QC343', '1', 20.65, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 13:13:54'),
	(65, '10', 9.93, 'D72UE4S2P2', '1', 0.41, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 13:19:17'),
	(66, '300', 300.02, 'YBVU9KHP2P', '1', 12.39, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 16:20:28'),
	(67, '200', 200.02, '2PEU1XCP3P', '1', 8.26, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 16:31:35'),
	(68, '500', 500.04, 'G36UQ4FDLD', '1', 20.65, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 16:36:19'),
	(69, '100', 100.01, 'YEVUKZS4Q4', '1', 4.13, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 16:43:47'),
	(70, '200', 200.02, 'DN2UYEC5R5', '1', 8.26, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 16:45:46'),
	(71, '20', 20.10, 'DKGUELS9B9', '1', 0.83, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 16:53:33'),
	(72, '30', 30.03, '1PXU95HQZQ', '1', 1.24, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 16:59:28'),
	(73, '100', 100.01, 'JNBUBJTWZW', '1', 4.13, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 14:09:32'),
	(74, '100', 100.01, '2JLUXMS2W2', '1', 4.13, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 14:12:44'),
	(75, '200', 200.02, 'EBGUZ3S4E4', '1', 8.26, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 14:14:50'),
	(76, '100', 100.01, '2N7U6WC5Q5', '1', 4.13, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 14:24:14'),
	(77, '200', 200.02, '78NU14I6M6', '1', 8.26, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 16:05:39'),
	(78, '100', 100.01, 'Q4WUK4SR7R', '1', 4.13, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 16:07:13'),
	(79, '100', 100.01, 'X9PU81C2L2', '1', 4.13, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 16:10:38'),
	(80, '200', 200.02, 'RXYU73IQPQ', '1', 8.26, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 16:12:22'),
	(81, '100', 100.01, 'YXLUYGIM4M', '1', 4.13, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 16:14:21'),
	(82, '200', 200.02, 'WW8UQ1IRBR', '1', 8.26, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 16:15:58'),
	(83, '100', 100.01, 'NNPU67C8M8', '1', 4.13, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 16:28:27'),
	(84, '100', 100.01, 'K9BUZDS5B5', '1', 4.13, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 16:30:16'),
	(85, '200', 200.02, '1W4U9BHEZE', '1', 8.26, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 16:30:36'),
	(86, '100', 100.01, 'Q16UM1H6L6', '1', 4.13, 'Prepaid Electricity', 'ws_CO_DMZ_2308115833_05112019193018664', '0', 'Success', 'ok', '2019-11-05 16:30:44'),
	(87, '100', 100.01, 'N8ZUD9HE8E', '1', 4.13, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 17:11:03'),
	(88, '200', 200.02, '23QU8JTW3W', '1', 8.26, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 17:12:26'),
	(89, '100', 100.01, 'P5XUL2CL8L', '1', 4.13, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-05 17:15:19'),
	(90, '100', 100.01, '9MNU5XIZPZ', '1', 4.13, 'Prepaid Electricity', 'ws_CO_DMZ_411270259_06112019084405953', '0', 'Success', 'ok', '2019-11-06 05:44:40'),
	(91, '100', 100.01, 'JGQUZXSNDN', '1', 4.13, 'Prepaid Electricity', 'ws_CO_DMZ_411287785_06112019101115035', '0', 'Success', 'ok', '2019-11-06 07:11:49'),
	(92, '20', 20.10, 'K26UJ8T7R7', '1', 0.83, 'Prepaid Electricity', 'ws_CO_DMZ_2322515884_06112019111819392', '0', 'Success', 'ok', '2019-11-06 08:18:45'),
	(93, '20', 20.10, 'J17ULEIW7W', '1', 0.83, 'Prepaid Electricity', 'ws_CO_DMZ_2322613702_06112019112340801', '0', 'Success', 'ok', '2019-11-06 08:24:07'),
	(94, '2', 1.94, 'VEBUE1SJ6J', '1', 0.08, 'Prepaid Electricity', 'ws_CO_DMZ_411346595_06112019142855663', '0', 'Success', 'ok', '2019-11-06 11:29:32'),
	(95, '3700', 3699.96, '6ERUDLHG9G', '1', 151.49, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-07 17:11:09'),
	(96, '5', 4.88, 'DL7UBZI212', '1', 0.20, 'Prepaid Electricity', 'ws_CO_DMZ_411723785_07112019231300567', '0', 'Success', 'ok', '2019-11-07 20:13:35'),
	(97, '100', 99.89, '44VUVLS787', '1', 4.09, 'Prepaid Electricity', 'ws_CO_DMZ_2372867051_08112019092822294', '0', 'Success', 'ok', '2019-11-08 06:28:48'),
	(98, '500', 300.00, '8X3U4NIW7W', '2', 2.73, 'CreditToken', '9M6UQ9FWGW', '00', '44650421800066635114', 'ok', '2019-11-09 08:43:29'),
	(99, '20', 20.10, '8D1U4ZI9W9', '1', 0.83, 'Prepaid Electricity', 'ws_CO_131120192033360998', '0', 'Success', 'ok', '2019-11-13 17:34:11'),
	(100, '400', 200.00, 'NNPUZ6SYXY', '2', 1.82, 'CreditTokenT1', 'QBZUEBS1N1', '00', '52070555686434326713', 'ok', '2019-11-22 12:03:57'),
	(101, '500', 300.00, '66YUN1AE4E', '2', 2.73, 'CreditTokenT1', 'PPNUMGH4V4', '00', '37446968584402070928', 'ok', '2019-11-22 12:13:17'),
	(102, '500', 300.00, '2PLUJ7TLBL', '2', 2.73, 'CreditTokenT1', 'GP9U3LCDJD', '00', '71882382323502697466', 'ok', '2019-11-22 15:20:18'),
	(103, '400', 400.03, 'B16U69IJ6J', '1', 16.52, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2019-11-22 18:30:19'),
	(104, '400', 200.00, '936UZ9S3P3', '2', 1.82, 'CreditTokenT1', 'VEXUB3HLNL', '00', '43888470193518185194', 'ok', '2019-11-22 18:35:38'),
	(105, '499', 299.00, 'WV1UPGU9L9', '2', 2.72, 'CreditTokenT1', '6J1ULXTQVQ', '00', '00183288717214518934', 'ok', '2019-11-22 18:37:12'),
	(106, '500', 300.00, '7WBUPMUKWK', '2', 2.73, 'CreditTokenT1', '4MLUYDI242', '00', '38683344493156917345', 'ok', '2019-12-03 16:33:33'),
	(107, '500', 300.00, 'JXYU8NTNVN', '2', 2.73, 'CreditTokenT1', 'KV1UD5H9X9', '00', '11409937616831274795', 'ok', '2019-12-09 12:26:47'),
	(108, '500', 300.00, 'W3ZUMYH757', '2', 2.73, 'CreditTokenT1', 'GJLUGRFB2B', '02', '', 'invalid user_id', '2019-12-09 12:27:47'),
	(109, '500', 300.00, 'RW6UP9UKRK', '2', 2.73, 'CreditTokenT1', '8XBUYJFMKM', '00', '35386531035950540652', 'ok', '2019-12-13 07:42:43'),
	(110, '50', 49.88, '7VLFXDS151', '1', 2.06, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2020-01-18 09:03:48'),
	(111, '100', 100.01, 'QV6FMGIBGB', '1', 4.13, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2020-01-18 09:08:21'),
	(112, '500', 500.04, '3B6IDPHLYL', '1', 20.65, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2020-03-02 07:34:57'),
	(113, '500', 500.04, '9WPIBWU5X5', '1', 20.65, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2020-03-02 07:37:20'),
	(114, '20', 20.10, '2B9I7DCZ5Z', '1', 0.83, 'Prepaid Electricity', 'ws_CO_020320201633518794', '0', 'Success', 'ok', '2020-03-02 13:34:22'),
	(115, '5', 5.09, 'PBJIMZH464', '1', 0.21, 'Prepaid Electricity', 'ws_CO_020320201643234092', '0', 'Success', 'ok', '2020-03-02 13:43:57'),
	(116, '15', 15.01, 'M6DIDZHQDQ', '1', 0.62, 'Prepaid Electricity', 'ws_CO_060320200954203759', '0', 'Success', 'ok', '2020-03-06 06:54:51'),
	(117, '30', 29.92, 'GGVU2GFP5P', '1', 1.29, 'Electricity', 'ws_CO_170720201052255976', '0', 'Pending', 'Pending', '2020-07-17 10:52:59'),
	(120, '50', 50.09, 'NVLUQ1T242', '1', 2.16, 'Prepaid Electricity', '', '0', 'Pending', 'Pending', '2020-08-04 14:51:50'),
	(123, '50', 50.09, 'XY2UWVIDBD', '1', 2.16, 'Prepaid Electricity', '', '0', 'Pending', 'Pending', '2020-08-04 14:52:07'),
	(126, '50', 50.09, '99QUBLTX6X', '1', 2.16, 'Prepaid Electricity', '', '0', 'Pending', 'Pending', '2020-08-04 14:52:22'),
	(129, '1000', 999.97, 'DXWUVNSNKN', '1', 43.12, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2020-08-05 17:31:45'),
	(132, '1000', 999.97, 'Z7BUYET6D6', '1', 43.12, 'Prepaid Electricity', '', '0', 'Success', 'ok', '2020-08-07 12:21:48');
/*!40000 ALTER TABLE `request_token` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(45) NOT NULL,
  `description` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `role_name`, `description`) VALUES
	(10001, 'Admin', 'System Admin'),
	(20001, 'User', 'User');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.saved_messages
DROP TABLE IF EXISTS `saved_messages`;
CREATE TABLE IF NOT EXISTS `saved_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL,
  `message` varchar(200) NOT NULL,
  `date_time` datetime NOT NULL,
  `sid` varchar(50) NOT NULL,
  `status` varchar(45) NOT NULL,
  `date_created` varchar(45) NOT NULL,
  `from` varchar(45) DEFAULT NULL,
  `to` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table lectrotel_portal.saved_messages: ~0 rows (approximately)
/*!40000 ALTER TABLE `saved_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `saved_messages` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.section_details
DROP TABLE IF EXISTS `section_details`;
CREATE TABLE IF NOT EXISTS `section_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `section_name` varchar(50) NOT NULL,
  `section_description` varchar(450) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.section_details: ~1 rows (approximately)
/*!40000 ALTER TABLE `section_details` DISABLE KEYS */;
INSERT INTO `section_details` (`id`, `section_name`, `section_description`, `created_on`) VALUES
	(6, 'Default', 'Default', '2020-09-28 10:18:33');
/*!40000 ALTER TABLE `section_details` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.user_details
DROP TABLE IF EXISTS `user_details`;
CREATE TABLE IF NOT EXISTS `user_details` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL DEFAULT 20001,
  `company_id` int(10) unsigned NOT NULL DEFAULT 0,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(300) DEFAULT NULL,
  `salt` varchar(100) DEFAULT NULL,
  `verification_code` varchar(205) DEFAULT NULL,
  `is_verified` tinyint(1) NOT NULL DEFAULT 0,
  `mobile_number` varchar(45) DEFAULT NULL,
  `dob` varchar(15) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1 COMMENT='Table Users';

-- Dumping data for table lectrotel_portal.user_details: ~4 rows (approximately)
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;
INSERT INTO `user_details` (`user_id`, `first_name`, `last_name`, `role_id`, `company_id`, `username`, `email`, `password`, `salt`, `verification_code`, `is_verified`, `mobile_number`, `dob`, `created_on`) VALUES
	(6, 'mimi', 'mimi', 10001, 1, 'mimi', 'admin@admin.com', '41c083710c2573af32ef681de70dd12243b892f2', '24ec1792e67907c5f19430c98673cd85219e9b4d', 'b8507fb9-a662-4b37-6e11-3e0041764e33', 1, '+254723436438', '08-01-1990', '2020-07-05 22:06:34'),
	(13, 'Rovas', 'LTD', 20001, 4, 'rovas', 'info@rovas.com', '393f3a517e33ff7d746da3f5eb322d10f0a3511f', '08567ec361d74b0c71bc8158d5ee74573cc95232', '73531327-cca7-4a5b-5ac4-22571f1dd3aa', 1, '+254723436438', '08-01-1990', '2020-07-07 20:01:01'),
	(16, 'Lectrotel', 'LTD', 20001, 1, 'rovas', 'info@lectrotel.com', '393f3a517e33ff7d746da3f5eb322d10f0a3511f', '08567ec361d74b0c71bc8158d5ee74573cc95232', '73531327-cca7-4a5b-5ac4-22571f1dd3aa', 1, '+254723436438', '08-01-1990', '2020-07-07 20:01:01'),
	(18, 'Rovas', 'LTD', 20001, 4, 'rovas', 'info@rovas.com', 'b2f58eb18d693a33a72b25de915489d3531d0ca4', '5ed1173807d4da0c602f46dcc51294b60639fe73', 'be0ca631-883b-4e65-6ca0-df3f19d11ea2', 0, '+254723436438', '08-01-1990', '2021-02-08 16:09:02');
/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;

-- Dumping structure for table lectrotel_portal.user_roles
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
  `ur_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`ur_id`),
  KEY `fk_idx` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table lectrotel_portal.user_roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
