module github.com/lectrotel-api

go 1.13

require (
	github.com/Sirupsen/logrus v1.0.5
	github.com/asaskevich/govalidator v0.0.0-20200428143746-21a406dcc535 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/go-ozzo/ozzo-dbx v1.0.16-0.20180615163742-e984ee904f1b
	github.com/go-ozzo/ozzo-routing v2.1.4+incompatible
	github.com/go-ozzo/ozzo-validation v3.4.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/gddo v0.0.0-20180703174436-daffe1f90ec5 // indirect
	github.com/hashicorp/hcl v0.0.0-20180404174102-ef8a98b0bbce // indirect
	github.com/lib/pq v0.0.0-20180523175426-90697d60dd84
	github.com/magiconair/properties v1.8.0 // indirect
	github.com/mitchellh/mapstructure v0.0.0-20180511142126-bb74f1db0675 // indirect
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d
	github.com/onsi/ginkgo v1.15.0 // indirect
	github.com/onsi/gomega v1.10.5 // indirect
	github.com/rs/xid v1.2.1
	github.com/sirupsen/logrus v1.6.0 // indirect
	github.com/sony/sonyflake v1.0.0
	github.com/speps/go-hashids v2.0.0+incompatible
	github.com/spf13/afero v1.1.1 // indirect
	github.com/spf13/cast v1.2.0 // indirect
	github.com/spf13/jwalterweatherman v0.0.0-20180109140146-7c0cea34c8ec // indirect
	github.com/spf13/viper v1.0.2
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.4.6
	gopkg.in/airbrake/gobrake.v2 v2.0.9 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gemnasium/logrus-airbrake-hook.v2 v2.1.2 // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gopkg.in/yaml.v2 v2.3.0
)
