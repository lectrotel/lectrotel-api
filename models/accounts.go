package models

import validation "github.com/go-ozzo/ozzo-validation"

// AccountDetails represents an meter record.
type AccountDetails struct {
	ID                 int    `json:"id" db:"id"`
	AccountName        string `json:"account_name" db:"account_name"`
	AccountDescription string `json:"account_description" db:"account_description"`
}

// ValidateAccountDetails validates the account fields.
func (m AccountDetails) ValidateAccountDetails() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.AccountName, validation.Required, validation.Length(0, 120)),
	)
}
