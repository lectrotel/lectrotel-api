package models

// PricePlanWater ...
type PricePlanWater struct {
	ID            int     `json:"id" db:"id"`
	TariffID      int     `json:"tariff_id" db:"tariff_id"`
	TariffName    string  `json:"tariff_name" db:"tariff_name"`
	FixedCharge   float64 `json:"fixed_charge" db:"fixed_charge"`
	ChargePerUnit float64 `json:"charge_per_unit" db:"charge_per_unit"`
	VAT           float64 `json:"vat" db:"vat"`
	Sewage           float64 `json:"sewage" db:"sewage"`
	RegulatoryLevy           float64 `json:"regulatory_levy" db:"regulatory_levy"`
}
