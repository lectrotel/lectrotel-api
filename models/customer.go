package models

import (
	"time"

	validation "github.com/go-ozzo/ozzo-validation"
)

// Customers represents an customer record.
type Customers struct {
	ID          int       `json:"id" db:"id"`
	Name        string    `json:"name" db:"full_name"`
	IDNo        string    `json:"id_no" db:"id_no"`
	PhoneNo     string    `json:"phone_no" db:"phone_no"`
	Email       string    `json:"email" db:"email"`
	CreatedOn   time.Time `json:"created_on" db:"created_on"`
	Status      int8      `json:"status" db:"status"`
	MeterNo     string    `json:"meter_no,omitempty" db:"meter_no"`
	Description string    `json:"desc,omitempty" db:"desc"`
}

// ValidateCustomer validates the Customer fields.
func (m Customers) ValidateCustomer() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.Name, validation.Required, validation.Length(0, 120)),
		validation.Field(&m.MeterNo, validation.Required),
	)
}
