package models

// Identity ...
type Identity interface {
	GetID() string
	GetName() string
}

// User ...
type User struct {
	ID   string
	Name string
}

// GetID ...
func (u User) GetID() string {
	return u.ID
}

// GetName ...
func (u User) GetName() string {
	return u.Name
}

// MessageDetails ...
type MessageDetails struct {
	MessageID string
	Message   string
	ToNumber  string
}

// SaveMessageDetails ...
type SaveMessageDetails struct {
	MessageID   string
	Message     string
	DateTime    string
	SID         string
	Status      string
	DateCreated string
	From        string
	To          string
}
