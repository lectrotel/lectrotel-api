package models

import validation "github.com/go-ozzo/ozzo-validation"

// GatewayDetails ...
type GatewayDetails struct {
	ID                 int    `json:"id" db:"id"`
	SectionID          int    `json:"section_id"`
	GatewayName        string `json:"gateway_name" db:"gateway_name"`
	GatewayDescription string `json:"gateway_description" db:"gateway_description"`
	CompanyID          int    `json:"company_id" db:"company_id"`
	AddedBy            int    `json:"added_by" db:"added_by"`
	SectionName        string `json:"section_name"`
}

// ValidateGatewayDetails validates the section fields.
func (g GatewayDetails) ValidateGatewayDetails() error {
	return validation.ValidateStruct(&g,
		validation.Field(&g.SectionID, validation.Required),
		validation.Field(&g.GatewayName, validation.Required, validation.Length(0, 120)),
	)
}
