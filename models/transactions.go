package models

import validation "github.com/go-ozzo/ozzo-validation"

// TokenTopup ...
type TokenTopup struct {
	MessageID    int     `json:"message_id"`
	ByteCount    int     `json:"byte_count"`
	GatewayID    int     `json:"gateway_id"`
	MeterNumber  string  `json:"meter_number"`
	ModbusID     int     `json:"modbus_id"`
	RechargeUnit float32 `json:"recharge_unit"`
	CRC          int     `json:"crc"`
	TransID      string  `json:"trans_id,omitempty"`
}

// Validate validates the rquest.
func (t TokenTopup) Validate() error {
	return validation.ValidateStruct(&t,
		validation.Field(&t.MessageID, validation.Required),
		validation.Field(&t.MeterNumber, validation.Required),
		validation.Field(&t.RechargeUnit, validation.Required),
	)
}

// NewTokenTopup ...
func NewTokenTopup(transid string, gatewayid int, rechargeunit float32, messageid, bytecount, modbus, crc int, meterno string) TokenTopup {
	return TokenTopup{messageid, bytecount, gatewayid, meterno, modbus, rechargeunit, crc, transid}
}

// TokenTopupJob ...
type TokenTopupJob struct {
	TokenTopup       TokenTopup
	TransInvoices    TransInvoices
	UnitsTotalAmount float64
}

// TransInvoices ...
type TransInvoices struct {
	ID                int     `json:"id" db:"id"`
	TransID           string  `json:"trans_id" db:"trans_id"`
	MeterID           int32   `json:"meter_id" db:"meter_id"`
	CompanyID         int     `json:"company_id" db:"company_id"`
	AddedBy           int     `json:"added_by" db:"added_by"`
	PaymentOption     string  `json:"payment_option" db:"payment_option"`
	PhoneNumber       string  `json:"phone_number" db:"phone_number"`
	Amount            float64 `json:"amount" db:"amount"`
	TransDescription  string  `json:"trans_description" db:"trans_description"`
	RequestCheckOutID string  `json:"request_checkout_id" db:"third_party_trans_id"`
	MeterType         int     `json:"meter_type"`
}

// Validate validates the rquest.
func (t TransInvoices) Validate() error {
	return validation.ValidateStruct(&t,
		validation.Field(&t.MeterID, validation.Required),
		validation.Field(&t.Amount, validation.Required),
		validation.Field(&t.PaymentOption, validation.Required),
	)
}

// NewTransInvoices ...
func NewTransInvoices(id int, meterid int32, companyid, addedby int, amount float64, transid, paymentoption, phone, transdescription, checkoutid string, metertype int) TransInvoices {
	return TransInvoices{id, transid, meterid, companyid, addedby, paymentoption, phone, amount, transdescription, checkoutid, metertype}
}

// ProcessTransJobs ...
type ProcessTransJobs struct {
	ProcessJobs TransInvoices
}

// PaymentDetails ...
type PaymentDetails struct {
	ID               int     `json:"id" db:"id"`
	TransID          string  `json:"trans_id" db:"trans_id"`
	MeterNumber      string  `json:"meter_number" db:"meter_number"`
	MeterShortNumber string  `json:"meter_short_number" db:"meter_short_number"`
	MeterType        int     `json:"meter_type" db:"meter_type"`
	Units            float64 `json:"units" db:"units"`
	FullName         string  `json:"full_name" db:"full_name"`
	Phone            string  `json:"phone" db:"phone"`
	TransAmount      float64 `json:"trans_amount" db:"trans_amount"`
	VAT              float64 `json:"vat" db:"vat"`
	FixedCharge      float64 `json:"fixed_charge" db:"fixed_charge"`
	TransTime        string  `json:"trans_time" db:"trans_time"`
	Status           string  `json:"status" db:"status"`
	Balance          string  `json:"balance" db:"balance"`

	EnergyCharge         float64 `json:"energy_charge,omitempty" db:"energy_charge"`
	FuelCostCharge       float64 `json:"fuel_cost_charge,omitempty" db:"fuel_cost_charge"`
	ForexAdj             float64 `json:"forex_adj,omitempty" db:"forex_adj"`
	ERCLevy              float64 `json:"erc_levy,omitempty" db:"erc_levy"`
	InflationAdj         float64 `json:"inflation_adj,omitempty" db:"inflation_adj"`
	WarmaLevy            float64 `json:"warma_levy,omitempty" db:"warma_levy"`
	REPLevy              float64 `json:"rep_levy,omitempty" db:"rep_levy"`
	PowerFactorSurcharge float64 `json:"power_factor_surcharge,omitempty" db:"power_factor_surcharge"`

	ChargePerUnit  float64 `json:"charge_per_unit,omitempty" db:"charge_per_unit"`
	Sewage         float64 `json:"sewage,omitempty" db:"sewage"`
	RegulatoryLevy float64 `json:"regulatory_levy,omitempty" db:"regulatory_levy"`
}

// PublishedMQTTData
type PublishedMessages struct {
	ID           int     `json:"id" db:"id"`
	SystemCode   string  `json:"system_code" db:"system_code"`
	MessageID    int     `json:"message_id" db:"message_id"`
	ModbusID     int     `json:"modbus_id" db:"modbus_id"`
	GatewayID    int     `json:"gateway_id" db:"gateway_id"`
	MeterNumber  int     `json:"meter_number" db:"meter_number"`
	RechargeUnit float32 `json:"recharge_unit" db:"recharge_unit"`
	TokenRandID  int     `json:"token_rand_id" db:"token_rand_id"`
	ByteLength   int     `json:"byte_length" db:"byte_length"`
	Checksum     int     `json:"checksum" db:"checksum"`
}
