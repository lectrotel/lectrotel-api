package models

// Roles ...
type Roles struct {
	ID              uint32 `json:"role_id" db:"pk,role_id"`
	RoleName        string `json:"role_name" db:"role_name"`
	RoleDescription string `json:"role_description" db:"description"`
}
