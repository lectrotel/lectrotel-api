package models

import (
	"math/rand"
	"strconv"
	"time"

	validation "github.com/go-ozzo/ozzo-validation"
)

// Invoices ...
type Invoices struct {
	ID              int          `json:"id" db:"id"`
	MeterID         int          `json:"meter_id" db:"meter_id"`
	MeterNumber     string       `json:"meter_number" db:"meter_number"`
	InvoiceNumber   string       `json:"invoice_number" db:"invoice_number"`
	InvoiceAmount   float32      `json:"invoice_amount" db:"invoice_amount"`
	PreviousBalance float32      `json:"previous_balance" db:"previous_balance"`
	PreviousReading float32      `json:"previous_reading" db:"previous_reading"`
	CurrentReading  float32      `json:"current_reading" db:"current_reading"`
	InvoiceStatus   int8         `json:"invoice_status" db:"invoice_status"`
	CreatedOn       time.Time    `json:"created_on" db:"created_on"`
	InvoiceItems    InvoiceItems `json:"invoice_items" db:"-"`
}

// Prepare ...
func (i *Invoices) Prepare() {
	i.ID = 0
	i.CreatedOn = time.Now()
	i.InvoiceNumber = "INV" + strconv.Itoa(rangeIn(1000, 9999))
}

// Validate validates the invoice fields.
func (i Invoices) Validate() error {
	return validation.ValidateStruct(&i,
		validation.Field(&i.MeterID, validation.Required),
		validation.Field(&i.MeterNumber, validation.Required, validation.Length(0, 120)),
		validation.Field(&i.InvoiceAmount, validation.Required),
		validation.Field(&i.CurrentReading, validation.Required),
	)
}

func rangeIn(low, hi int) int {
	return low + rand.Intn(hi-low)
}

// InvoiceDetails ...
type InvoiceDetails struct {
	ID              int          `json:"id" db:"id"`
	MeterID         int          `json:"meter_id" db:"meter_id"`
	MeterNumber     string       `json:"meter_number" db:"meter_number"`
	InvoiceNumber   string       `json:"invoice_number" db:"invoice_number"`
	InvoiceAmount   float32      `json:"invoice_amount" db:"invoice_amount"`
	PreviousBalance float32      `json:"previous_balance" db:"previous_balance"`
	PreviousReading float32      `json:"previous_reading" db:"previous_reading"`
	CurrentReading  float32      `json:"current_reading" db:"current_reading"`
	InvoiceStatus   int8         `json:"invoice_status" db:"invoice_status"`
	CreatedOn       time.Time    `json:"created_on" db:"created_on"`
	MeterDetails    MeterDetails `json:"meter_details" db:"-"`
	InvoiceItems    InvoiceItems `json:"invoice_items" db:"-"`
}

// InvoiceItems ...
type InvoiceItems struct {
	ID                        int       `json:"id" db:"id"`
	InvoiceID                 int       `json:"invoice_id" db:"invoice_id"`
	InvoiceNumber             string    `json:"invoice_number" db:"invoice_number"`
	TotalFixedCharge          float64   `json:"total_fixed_charge" db:"total_fixed_charge"`
	TotalEnergyCharge         float64   `json:"total_energy_charge" db:"total_energy_charge"`
	TotalFuelCostCharge       float64   `json:"total_fuel_cost_charge" db:"total_fuel_cost_charge"`
	TotalForexAdj             float64   `json:"total_forex_adj" db:"total_forex_adj"`
	TotalERCLevy              float64   `json:"total_erc_levy" db:"total_erc_levy"`
	TotalInflationAdj         float64   `json:"total_inflation_adj" db:"total_inflation_adj"`
	TotalWarmaLevy            float64   `json:"total_warma_levy" db:"total_warma_levy"`
	TotalREPLevy              float64   `json:"total_rep_levy" db:"total_rep_levy"`
	TotalPowerFactorSurcharge float64   `json:"total_power_factor_surcharge" db:"total_power_factor_surcharge"`
	TotalVAT                  float64   `json:"total_vat" db:"total_vat"`
	CreatedOn                 time.Time `json:"created_on" db:"created_on"`
}

// Prepare ...
func (i *InvoiceItems) Prepare() {
	i.ID = 0
	i.CreatedOn = time.Now()
}

// Validate validates the invoice fields.
func (i InvoiceItems) Validate() error {
	return validation.ValidateStruct(&i,
		validation.Field(&i.InvoiceNumber, validation.Required),
		validation.Field(&i.TotalFixedCharge, validation.Required),
		validation.Field(&i.TotalEnergyCharge, validation.Required),
		validation.Field(&i.TotalFuelCostCharge, validation.Required),
		validation.Field(&i.TotalForexAdj, validation.Required),
		validation.Field(&i.TotalERCLevy, validation.Required),
		validation.Field(&i.TotalInflationAdj, validation.Required),
		validation.Field(&i.TotalWarmaLevy, validation.Required),
		validation.Field(&i.TotalREPLevy, validation.Required),
		validation.Field(&i.TotalPowerFactorSurcharge, validation.Required),
		validation.Field(&i.TotalVAT, validation.Required),
	)
}
