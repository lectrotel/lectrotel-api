package models

import validation "github.com/go-ozzo/ozzo-validation"

// PricingTariff ...
type PricingTariff struct {
	ID                int     `json:"id" db:"id"`
	TariffName        string  `json:"tariff_name" db:"plan_tariff_name"`
	TariffType        string  `json:"tariff_type" db:"plan_tariff_type"`
	TariffDescription string  `json:"tariff_description" db:"plan_tariff_description"`
	TariffAmount      float64 `json:"tariff_amount" db:"plan_tariff_amount"`
}

// ValidatePricingTariff validates the pricing tariff fields.
func (p PricingTariff) ValidatePricingTariff() error {
	return validation.ValidateStruct(&p,
		validation.Field(&p.TariffName, validation.Required, validation.Length(0, 120)),
	)
}

// PricePlan ...
type PricePlan struct {
	ID                   int     `json:"id" db:"id"`
	TariffID             int     `json:"tariff_id" db:"tariff_id"`
	TariffName           string  `json:"tariff_name" db:"tariff_name"`
	PalnTariffType       string  `json:"plan_tariff_type" db:"plan_tariff_type"`
	FixedCharge          float64 `json:"fixed_charge" db:"fixed_charge"`
	FixedChargeType      string  `json:"fixed_charge_type,omitempty" db:"fixed_charge_type"`
	EnergyCharge         float64 `json:"energy_charge" db:"energy_charge"`
	FuelCostCharge       float64 `json:"fuel_cost_charge" db:"fuel_cost_charge"`
	ForexAdj             float64 `json:"forex_adj" db:"forex_adj"`
	ERCLevy              float64 `json:"erc_levy" db:"erc_levy"`
	InflationAdj         float64 `json:"inflation_adj" db:"inflation_adj"`
	WarmaLevy            float64 `json:"warma_levy" db:"warma_levy"`
	REPLevy              float64 `json:"rep_levy" db:"rep_levy"`
	PowerFactorSurcharge float64 `json:"power_factor_surcharge" db:"power_factor_surcharge"`
	VAT                  float64 `json:"vat" db:"vat"`
	ChargePerUnit        float64 `json:"charge_per_unit" db:"charge_per_unit"`
	RegulatoryLevy       float64 `json:"regulatory_levy" db:"regulatory_levy"`
	Sewage               float64 `json:"sewage" db:"sewage"`
	MeterNumber          string  `json:"meter_number,omitempty" db:"meter_number"`
	ModbusID             int     `json:"modbus_id,omitempty" db:"modbus_id"`
	GatewayID            int     `json:"gateway_id,omitempty"`
	MeterType            int     `json:"meter_type,omitempty"`
}

// ValidatePricePlan validates the pricing plan fields.
func (p PricePlan) ValidatePricePlan() error {
	return validation.ValidateStruct(&p,
		validation.Field(&p.TariffName, validation.Required),
	)
}

// WaterPricePlan ...
type WaterPricePlan struct {
	ID             int     `json:"id" db:"id"`
	TariffID       int     `json:"tariff_id" db:"tariff_id"`
	TariffName     string  `json:"tariff_name" db:"tariff_name"`
	FixedCharge    float64 `json:"fixed_charge" db:"fixed_charge"`
	ChargePerUnit  float64 `json:"charge_per_unit" db:"charge_per_unit"`
	RegulatoryLevy float64 `json:"regulatory_levy" db:"regulatory_levy"`
	Sewage         float64 `json:"sewage" db:"sewage"`
	VAT            float64 `json:"vat" db:"vat"`
	MeterNumber    string  `json:"meter_number" db:"meter_number"`
	ModbusID       int     `json:"modbus_id" db:"modbus_id"`
	MeterType      int     `json:"meter_type" db:"meter_type"`
}
