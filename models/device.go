package models

import (
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
)

// ClientJob ...
type ClientJob struct {
	DeviceData DeviceData
	Client     MQTT.Client
	Topic      string
}

// DeviceData ...
type DeviceData struct {
	SystemCode           string    `json:"system_code"`
	MessageID            int       `json:"message_id"`
	ByteCount            int       `json:"byte_count"`
	GatewayID            uint32    `json:"gateway_id"`
	MeterID              uint32    `json:"meter_id"`
	ModbusID             int       `json:"modbus_id"`
	EnergyReading        float32   `json:"energy_reading"`
	RemainingUnits       float32   `json:"remaining_units"`
	VoltageLine1         float32   `json:"voltage_line_1"`
	VoltageLine2         float32   `json:"voltage_line_2"`
	VoltageLine3         float32   `json:"voltage_line_3"`
	TotalLineCurrent     float32   `json:"total_line_current"`
	SystemPower          float32   `json:"system_power"`
	SystemPowerFactor    float32   `json:"system_power_factor"`
	SystemFrequency      float32   `json:"system_frequency"`
	ImportActiveEnergy   float32   `json:"import_active_energy"`
	ImportReactiveEnergy float32   `json:"import_reactive_energy"`
	UTCTimeSeconds       int       `json:"utc_time_seconds,omitempty"` // 1 byte
	UTCTimeMinutes       int       `json:"utc_time_minutes,omitempty"` // 1 byte
	UTCTimeHours         int       `json:"utc_time_hours,omitempty"`   // 1 byte
	UTCTimeDay           int       `json:"utc_time_day,omitempty"`     // 1 byte
	UTCTimeMonth         int       `json:"utc_time_month,omitempty"`   // 1 byte
	UTCTimeYear          int       `json:"utc_time_year,omitempty"`    // 1 byte
	DateTime             time.Time `json:"date_time,omitempty"`
	DateTimeStamp        int64     `json:"date_time_stamp,omitempty"`
	Checksum             int       `json:"checksum,omitempty"`
	Online               int8      `json:"online"`
	RelayStatus          int8      `json:"relay_status"`
	CreatedOn            time.Time `json:"created_on"`
}
