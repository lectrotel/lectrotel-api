package models

import validation "github.com/go-ozzo/ozzo-validation"

// SectionDetails ...
type SectionDetails struct {
	ID                 int    `json:"id" db:"id"`
	SectionName        string `json:"section_name" db:"section_name"`
	SectionDescription string `json:"section_description" db:"section_description"`
}

// ValidateSectionDetails validates the section fields.
func (m SectionDetails) ValidateSectionDetails() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.SectionName, validation.Required, validation.Length(0, 120)),
	)
}