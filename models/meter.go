package models

import (
	"time"

	validation "github.com/go-ozzo/ozzo-validation"
)

// MeterDetails represents an meter record.
type MeterDetails struct {
	ID                 int       `json:"id" db:"id"`
	TariffID           int       `json:"tariff_id" db:"tariff_id"`
	GatewayID          int       `json:"gateway_id" db:"gateway_id"`
	CompanyID          int       `json:"company_id" db:"company_id"`
	MeterNumber        string    `json:"meter_number" db:"meter_number"`
	MeterShortNumber   string    `json:"meter_short_number" db:"meter_short_number"`
	MeterType          int       `json:"meter_type" db:"meter_type"`
	MeterTypeName      string    `json:"meter_type_name" db:"meter_type_name"`
	GatewayName        string    `json:"gateway_name" db:"gateway_name"`
	TariffName         string    `json:"tariff_name" db:"plan_tariff_name"`
	ModbusID           int       `json:"modbus_id" db:"modbus_id"`
	CurrentReading     float32   `json:"current_reading"`
	PreviousReading    float32   `json:"previous_reading"`
	RemainingUnits     float32   `json:"remaining_units"`
	LastSeen           time.Time `json:"last_seen"`
	AddedBy            int       `json:"added_by"`
	Balance            float32   `json:"balance"`
	AmountPayable      float64   `json:"amount_payable"`
	CurrentConsumption float32   `json:"current_consumption"`
	PricePlan          PricePlan `json:"price_plan" db:"-"`
}

// Validate validates the meter fields.
func (m MeterDetails) Validate() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.TariffID, validation.Required),
		validation.Field(&m.MeterNumber, validation.Required, validation.Length(0, 120)),
	)
}

// ResponseData ...
type ResponseData struct {
	SerialID   string `json:"serial_id"`
	ResultCode string `json:"result_code"`
	Reason     string `json:"reason"`
	Result     string `json:"result"`
}

// CalinResponseData ...
type CalinResponseData struct {
	ResultCode int         `json:"result_code"`
	Reason     string      `json:"reason"`
	Result     calinResult `json:"result"`
}

// calinResult ...
type calinResult struct {
	TotalPaid      float32 `json:"total_paid"`
	TotalUnit      float32 `json:"total_unit"`
	Token          string  `json:"token"`
	CustomerNumber string  `json:"customer_number"`
	CustomerName   string  `json:"customer_name"`
	CustomerAddr   string  `json:"customer_addr"`
	MeterNumber    string  `json:"meter_number"`
	GenDatetime    string  `json:"gen_datetime"`
	GenUser        string  `json:"gen_user"`
	Company        string  `json:"company"`
	Price          float32 `json:"price"`
	VAT            float32 `json:"vat"`
	TidDatetime    string  `json:"tid_datetime"`
	Currency       string  `json:"currency"`
	Unit           string  `json:"unit"`
	TaskNo         string  `json:"TaskNo"`
}

// NewResponseData ...
func NewResponseData(serialid, resultcode, reason, result string) (m ResponseData) {
	return ResponseData{serialid, resultcode, reason, result}
}

// ListTokens list of tokens generated
type ListTokens struct {
	ID               int32  `json:"id" db:"id"`
	SerialID         string `json:"serial_id" db:"serial_id"`
	TransID          string `json:"trans_id" db:"trans_id"`
	MeterID          string `json:"meter_id" db:"meter_id"`
	Units            string `json:"units" db:"units"`
	TokenType        string `json:"token_type" db:"token_type"`
	Amount           string `json:"amount" db:"amount"`
	TotalUnitsAmount string `json:"total_units_amount" db:"total_units_amount"`
	ResultCode       string `json:"result_code" db:"result_code"`
	Reason           string `json:"reason" db:"reason"`
	Result           string `json:"result" db:"result"`
	CreatedOn        string `json:"created_on" db:"created_on"`
	MeterNumber      string `json:"meter_number" db:"meter_number"`
}

// CalinRequestData ...
type CalinRequestData struct {
	CompanyName  string  `json:"company_name"`
	Username     string  `json:"user_name"`
	Password     string  `json:"password"`
	PasswordVend string  `json:"password_vend"`
	Amount       float32 `json:"amount"`
	MeterNumber  string  `json:"meter_number"`
	IsVendByUnit bool    `json:"is_vend_by_unit"`
}

// RequestData ...
type RequestData struct {
	SerialID   string `json:"serial_id"`
	UserID     string `json:"user_id"`
	MeterID    string `json:"meter_id"`
	TokenType  string `json:"token_type"`
	Amount     string `json:"amount"`
	Timestamp  string `json:"timestamp"`
	Ciphertext string `json:"ciphertext"`
}

// RequestToken request for token generation
type RequestToken struct {
	ID               int32   `json:"id,omitempty"`
	MeterID          int32   `json:"meter_id,omitempty"`
	Amount           float64 `json:"amount,omitempty"`
	Units            float32 `json:"units,omitempty"`
	TotalUnitsAmount float64 `json:"total_units_amount,omitempty"`
	ToNumber         string  `json:"phone_number,omitempty"`
	PaymentID        string  `json:"payment_id,omitempty"`
	PaymentOption    string  `json:"payment_option,omitempty"`
	MeterType        int     `json:"meter_type,omitempty"`
}

// NewRequestToken ...
func NewRequestToken(id int32, meterid int32, amount float64, units float32, unitsamount float64, number, transid, paymentoption string, metertype int) (m RequestToken) {
	return RequestToken{id, meterid, amount, units, unitsamount, number, transid, paymentoption, metertype}
}

// ValidateTokenRequest validates the rquest token fields.
func (m RequestToken) ValidateTokenRequest() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.MeterID, validation.Required),
		validation.Field(&m.Amount, validation.Required),
	)
}

// TokenDetails ...
type TokenDetails struct {
	UnitsAmount float64
	Units       float32
	MeterNumber string
	ModBusID    int `json:"modbus_id" db:"modbus_id"`
	GatewayID   int `json:"gateway_id,omitempty"`
	MeterType   int `json:"meter_type,omitempty"`
}
