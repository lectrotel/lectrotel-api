package models

// UserDashboardDetails ...
type UserDashboardDetails struct {
	NumberOfMeters       int `json:"no_of_meters"`
	NumberOfGateways     int `json:"no_of_gateways"`
	ConsumptionLast24Hrs int `json:"consumption_last_24_hrs"`
	ConsumptionLast7Days int `json:"consumption_last_7_days"`
}
