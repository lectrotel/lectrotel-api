package models

import (
	"html"
	"strings"
	"time"

	validation "github.com/go-ozzo/ozzo-validation"
)

// Companies structure
type Companies struct {
	ID              uint32    `json:"id"`
	CompanyName     string    `json:"company_name"`
	CompanyAlias    string    `json:"company_alias"`
	CompanyPhone    string    `json:"company_phone"`
	CompanyAddress  string    `json:"company_address"`
	CompanyEmail    string    `json:"company_email"`
	CompanyLocation string    `json:"company_location"`
	CompanyLogoURL  string    `json:"company_logo" db:"company_logo_url"`
	UpdatedAt       time.Time `json:"updated_at"`
	CreatedAt       time.Time `json:"created_at"`
	AddedBy         uint32    `json:"added_by"`
}

// Prepare ...
func (c *Companies) Prepare() {
	c.ID = 0
	c.CompanyName = html.EscapeString(strings.ToUpper(c.CompanyName))
	c.CompanyAlias = html.EscapeString(strings.ToUpper(c.CompanyAlias))
	c.CompanyLocation = html.EscapeString(strings.TrimSpace(c.CompanyLocation))
	c.CompanyEmail = html.EscapeString(strings.ToLower(c.CompanyEmail))
	c.CreatedAt = time.Now()
	c.UpdatedAt = time.Now()
}

// Validate ...
func (c Companies) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.CompanyName, validation.Required, validation.Length(0, 120)),
		validation.Field(&c.CompanyAlias, validation.Required),
		validation.Field(&c.CompanyPhone, validation.Required),
		validation.Field(&c.CompanyAddress, validation.Required),
		validation.Field(&c.CompanyEmail, validation.Required),
		validation.Field(&c.CompanyLocation, validation.Required),
	)
}
