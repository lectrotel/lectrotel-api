
# FROM golang:latest
FROM golang:alpine AS build-env

LABEL maintainer "ericotieno99@gmail.com"
LABEL vendor="Ekas Technologies"

RUN apk update && apk add --no-cache git ca-certificates && update-ca-certificates
# Create appuser
RUN adduser -D -g '' appuser

WORKDIR /go/src/github.com/lectrotel-api

ENV GOOS=linux
ENV GOARCH=386
ENV CGO_ENABLED=0

# Copy the project in to the container
ADD . /go/src/github.com/lectrotel-api

# Go get the project deps
RUN go get github.com/lectrotel-api

# Go install the project
# RUN go install github.com/lectrotel-api
RUN go build

# Set the working environment.
ENV GO_ENV production

# Run the lectrotel-api command by default when the container starts.
# ENTRYPOINT /go/bin/lectrotel-api

FROM alpine:latest
WORKDIR /go/

COPY --from=build-env /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build-env /etc/passwd /etc/passwd
COPY --from=build-env /go/src/github.com/lectrotel-api/lectrotel-api /go/lectrotel-api
COPY --from=build-env /go/src/github.com/lectrotel-api/config/app.yaml /go/config/app.yaml
COPY --from=build-env /go/src/github.com/lectrotel-api/config/errors.yaml /go/config/errors.yaml
COPY --from=build-env /go/src/github.com/lectrotel-api/cert.pem /go/cert.pem
COPY --from=build-env /go/src/github.com/lectrotel-api/key.pem /go/key.pem

# Use an unprivileged user.
USER appuser

# Set the working environment.
ENV GO_ENV production

ENTRYPOINT ./lectrotel-api

#Expose the port specific to the ekas API Application.
EXPOSE 8086
EXPOSE 8088
