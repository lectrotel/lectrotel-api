package apis

import (
	"strconv"

	routing "github.com/go-ozzo/ozzo-routing"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

type (
	// accountService specifies the interface for the account service needed by accountResource.
	accountService interface {
		Get(rs app.RequestScope, id int) (*models.AccountDetails, error)
		Query(rs app.RequestScope, offset, limit int) ([]models.AccountDetails, error)
		Count(rs app.RequestScope) (int, error)
		Create(rs app.RequestScope, model *models.AccountDetails) (*models.AccountDetails, error)
		Update(rs app.RequestScope, id int, model *models.AccountDetails) (*models.AccountDetails, error)
		Delete(rs app.RequestScope, id int) (*models.AccountDetails, error)
	}

	// accountResource defines the handlers for the CRUD APIs.
	accountResource struct {
		service accountService
	}
)

// ServeAccountResource sets up the routing of account endpoints and the corresponding handlers.
func ServeAccountResource(rg *routing.RouteGroup, service accountService) {
	r := &accountResource{service}
	rg.Get("/account/get/<id>", r.get)
	rg.Get("/accounts/list", r.query)
	rg.Post("/account/create", r.create)
	rg.Put("/account/update/<id>", r.update)
	rg.Delete("/account/delete/<id>", r.delete)
}

func (r *accountResource) get(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	response, err := r.service.Get(app.GetRequestScope(c), id)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *accountResource) query(c *routing.Context) error {
	rs := app.GetRequestScope(c)
	count, err := r.service.Count(rs)
	if err != nil {
		return err
	}
	paginatedList := getPaginatedListFromRequest(c, count)
	items, err := r.service.Query(app.GetRequestScope(c), paginatedList.Offset(), paginatedList.Limit())
	if err != nil {
		return err
	}
	paginatedList.Items = items
	return c.Write(paginatedList)
}

func (r *accountResource) create(c *routing.Context) error {
	var model models.AccountDetails
	if err := c.Read(&model); err != nil {
		return err
	}
	response, err := r.service.Create(app.GetRequestScope(c), &model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *accountResource) update(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	rs := app.GetRequestScope(c)

	model, err := r.service.Get(rs, id)
	if err != nil {
		return err
	}

	if err := c.Read(model); err != nil {
		return err
	}

	response, err := r.service.Update(rs, id, model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *accountResource) delete(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	response, err := r.service.Delete(app.GetRequestScope(c), id)
	if err != nil {
		return err
	}

	return c.Write(response)
}
