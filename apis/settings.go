package apis

import (
	"strconv"

	routing "github.com/go-ozzo/ozzo-routing"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

type (
	// settingService specifies the interface for the setting service needed by settingResource.
	settingService interface {
		QueryPricingTariff(rs app.RequestScope, offset, limit int) ([]models.PricingTariff, error)
		CountPricingTariff(rs app.RequestScope) (int, error)
		CreatePricingTariff(rs app.RequestScope, model *models.PricingTariff) (*models.PricingTariff, error)

		QueryPricePlan(rs app.RequestScope, offset, limit, id int) ([]models.PricePlan, error)
		CountPricePlan(rs app.RequestScope) (int, error)
		CreatePricePlan(rs app.RequestScope, model *models.PricePlan) (*models.PricePlan, error)
		UpdatePricePlan(rs app.RequestScope, model *models.PricePlan) (*models.PricePlan, error)
	}

	// settingResource defines the handlers for the CRUD APIs.
	settingResource struct {
		service settingService
	}
)

// ServeSettingResource sets up the routing of setting endpoints and the corresponding handlers.
func ServeSettingResource(rg *routing.RouteGroup, service settingService) {
	r := &settingResource{service}
	rg.Get("/pricing/tariff", r.queryPricingTariff)
	rg.Post("/pricing/tariff", r.createPricingTariff)

	rg.Get("/price/plan", r.queryPricePlan)
	rg.Post("/price/plan", r.createPricePlan)
	rg.Put("/price/plan", r.updatePricePlan)
}

func (r *settingResource) queryPricingTariff(c *routing.Context) error {
	rs := app.GetRequestScope(c)
	count, err := r.service.CountPricingTariff(rs)
	if err != nil {
		return err
	}
	paginatedList := getPaginatedListFromRequest(c, count)
	items, err := r.service.QueryPricingTariff(app.GetRequestScope(c), paginatedList.Offset(), paginatedList.Limit())
	if err != nil {
		return err
	}
	paginatedList.Items = items
	return c.Write(paginatedList)
}

func (r *settingResource) createPricingTariff(c *routing.Context) error {
	var model models.PricingTariff
	if err := c.Read(&model); err != nil {
		return err
	}
	response, err := r.service.CreatePricingTariff(app.GetRequestScope(c), &model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *settingResource) queryPricePlan(c *routing.Context) error {
	id, _ := strconv.Atoi(c.Query("id", "0"))
	rs := app.GetRequestScope(c)
	count, err := r.service.CountPricePlan(rs)
	if id > 0 {
		count = 1
	}

	if err != nil {
		return err
	}
	paginatedList := getPaginatedListFromRequest(c, count)
	items, err := r.service.QueryPricePlan(app.GetRequestScope(c), paginatedList.Offset(), paginatedList.Limit(), id)
	if err != nil {
		return err
	}
	paginatedList.Items = items
	return c.Write(paginatedList)
}

func (r *settingResource) createPricePlan(c *routing.Context) error {
	var model models.PricePlan
	if err := c.Read(&model); err != nil {
		return err
	}
	response, err := r.service.CreatePricePlan(app.GetRequestScope(c), &model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *settingResource) updatePricePlan(c *routing.Context) error {
	var model models.PricePlan
	if err := c.Read(&model); err != nil {
		return err
	}
	response, err := r.service.UpdatePricePlan(app.GetRequestScope(c), &model)
	if err != nil {
		return err
	}

	return c.Write(response)
}
