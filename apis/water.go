package apis

import (
	routing "github.com/go-ozzo/ozzo-routing"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

type (
	// waterService specifies the interface for the water service needed by waterResource.
	waterService interface {
		QueryWaterPricePlan(rs app.RequestScope, offset, limit int) ([]models.PricePlanWater, error)
		CountWaterPricePlan(rs app.RequestScope) (int, error)
		UpdateWaterPricePlan(rs app.RequestScope, model *models.PricePlanWater) (*models.PricePlanWater, error)
	}

	// waterResource defines the handlers for the CRUD APIs.
	waterResource struct {
		service waterService
	}
)

// ServeWaterResource sets up the routing of water endpoints and the corresponding handlers.
func ServeWaterResource(rg *routing.RouteGroup, service waterService) {
	r := &waterResource{service}
	rg.Get("/water/priceplan", r.queryWaterPricePlan)
	rg.Put("/water/priceplan", r.updateWaterPricePlan)
}

func (r *waterResource) queryWaterPricePlan(c *routing.Context) error {
	rs := app.GetRequestScope(c)
	count, err := r.service.CountWaterPricePlan(rs)
	if err != nil {
		return err
	}
	paginatedList := getPaginatedListFromRequest(c, count)
	items, err := r.service.QueryWaterPricePlan(app.GetRequestScope(c), paginatedList.Offset(), paginatedList.Limit())
	if err != nil {
		return err
	}
	paginatedList.Items = items
	return c.Write(paginatedList)
}

func (r *waterResource) updateWaterPricePlan(c *routing.Context) error {
	var model models.PricePlanWater
	if err := c.Read(&model); err != nil {
		return err
	}
	response, err := r.service.UpdateWaterPricePlan(app.GetRequestScope(c), &model)
	if err != nil {
		return err
	}

	return c.Write(response)
}
