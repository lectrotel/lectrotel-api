package apis

import (
	"strconv"

	routing "github.com/go-ozzo/ozzo-routing"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/errors"
	"github.com/lectrotel-api/models"
)

type (
	// userService specifies the interface for the user service needed by userResource.
	userService interface {
		// GetUser returns the user with the specified user ID.
		GetUser(rs app.RequestScope, id uint64) (*models.UserDetails, error)
		Count(rs app.RequestScope) (int, error)
		Query(rs app.RequestScope, offset, limit int) ([]models.UserDetails, error)
		Register(rs app.RequestScope, usr *models.UserDetails) (uint64, error)
		Login(rs app.RequestScope, usr *models.Credential) (*models.UserDetails, error)
		SubmitUserRole(rs app.RequestScope, usr *models.UserRoles) (*models.UserRoles, error)
		Delete(rs app.RequestScope, id uint64) error
	}

	// userResource defines the handlers for the CRUD APIs.
	userResource struct {
		service userService
	}
)

// ServeUserResource sets up the routing of user endpoints and the corresponding handlers.
func ServeUserResource(rg *routing.RouteGroup, service userService) {
	r := &userResource{service}
	rg.Get("/user/<id>", r.getuser)
	rg.Get("/users/list", r.query)
	rg.Post("/user/role", r.submitroles)
	rg.Delete("/user/delete/<id>", r.delete)
	rg.Post("/register", r.register)
	rg.Post("/login", r.login)
}

func (r *userResource) getuser(c *routing.Context) error {
	id, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		return err
	}

	response, err := r.service.GetUser(app.GetRequestScope(c), id)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *userResource) query(c *routing.Context) error {
	rs := app.GetRequestScope(c)
	count, err := r.service.Count(rs)
	if err != nil {
		return err
	}

	paginatedList := getPaginatedListFromRequest(c, count)
	if count > 0 {
		items, err := r.service.Query(rs, paginatedList.Offset(), paginatedList.Limit())
		if err != nil {
			return err
		}
		paginatedList.Items = items
	}

	return c.Write(paginatedList)
}

// login -user login
func (r *userResource) login(c *routing.Context) error {
	var credential models.Credential
	if err := c.Read(&credential); err != nil {
		return errors.BadRequest(err.Error())
	}

	identity, err := r.service.Login(app.GetRequestScope(c), &credential)
	if err != nil {
		return errors.Unauthorized(err.Error())
	}

	return c.Write(identity)

}

func (r *userResource) register(c *routing.Context) error {
	var model models.UserDetails
	if err := c.Read(&model); err != nil {
		return err
	}

	response, err := r.service.Register(app.GetRequestScope(c), &model)
	if err != nil {
		return err
	}

	return c.Write(map[string]uint64{
		"last_insert_id": response,
	})
}

func (r *userResource) submitroles(c *routing.Context) error {
	var model models.UserRoles
	if err := c.Read(&model); err != nil {
		return err
	}
	response, err := r.service.SubmitUserRole(app.GetRequestScope(c), &model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *userResource) delete(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	if err := r.service.Delete(app.GetRequestScope(c), uint64(id)); err != nil {
		return err
	}

	return c.Write(map[string]string{
		"message": "Record Deleted Successfully",
	})
}
