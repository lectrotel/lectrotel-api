package apis

import (
	"strconv"

	routing "github.com/go-ozzo/ozzo-routing"
	"github.com/lectrotel-api/util"
)

const (
	// DEFAULTPAGESIZE ...
	DEFAULTPAGESIZE int = 100
	// MAXPAGESIZE ...
	MAXPAGESIZE int = 1000
)

func getPaginatedListFromRequest(c *routing.Context, count int) *util.PaginatedList {
	page := parseInt(c.Query("page"), 1)
	perPage := parseInt(c.Query("per_page"), DEFAULTPAGESIZE)
	if perPage <= 0 {
		perPage = DEFAULTPAGESIZE
	}
	if perPage > MAXPAGESIZE {
		perPage = MAXPAGESIZE
	}
	return util.NewPaginatedList(page, perPage, count)
}

func parseInt(value string, defaultValue int) int {
	if value == "" {
		return defaultValue
	}
	if result, err := strconv.Atoi(value); err == nil {
		return result
	}
	return defaultValue
}
