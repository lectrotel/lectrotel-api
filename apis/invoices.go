package apis

import (
	"strconv"

	routing "github.com/go-ozzo/ozzo-routing"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

func (r *transactionResource) getInvoice(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	response, err := r.service.GetInvoice(app.GetRequestScope(c), id)
	if err != nil {
		return err
	}

	return c.Write(response)
}

// listInvoices ...
func (r *transactionResource) listInvoices(c *routing.Context) error {
	meterno := c.Query("meter_number", "0")

	rs := app.GetRequestScope(c)
	count, err := r.service.CountInvoices(rs, meterno)
	if err != nil {
		return err
	}
	paginatedList := getPaginatedListFromRequest(c, count)
	items, err := r.service.ListInvoices(app.GetRequestScope(c), paginatedList.Offset(), paginatedList.Limit(), meterno)
	if err != nil {
		return err
	}
	paginatedList.Items = items
	return c.Write(paginatedList)
}

// AddInvoice ...
func (r *transactionResource) AddInvoice(c *routing.Context) error {
	var model models.Invoices
	if err := c.Read(&model); err != nil {
		return err
	}

	err := r.service.AddInvoice(app.GetRequestScope(c), &model)
	if err != nil {
		return err
	}

	return c.Write(map[string]int{
		"last_insert_id": model.ID,
	})
}
