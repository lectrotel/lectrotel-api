package apis

import (
	routing "github.com/go-ozzo/ozzo-routing"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

type (
	// permissionService specifies the interface for the permission service needed by permissionResource.
	permissionService interface {
		QueryRoles(rs app.RequestScope, roleid uint32) ([]models.Roles, error)
	}

	// permissionResource defines the handlers for the CRUD APIs.
	permissionResource struct {
		service permissionService
	}
)

// ServePermissionResource sets up the routing of permission endpoints and the corresponding handlers.
func ServePermissionResource(rg *routing.RouteGroup, service permissionService) {
	r := &permissionResource{service}
	rg.Get("/roles/list", r.queryRoles)
}

func (r *permissionResource) queryRoles(c *routing.Context) error {
	rs := app.GetRequestScope(c)

	items, err := r.service.QueryRoles(rs, 0)
	if err != nil {
		return err
	}

	return c.Write(items)
}
