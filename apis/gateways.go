package apis

import (
	"strconv"

	routing "github.com/go-ozzo/ozzo-routing"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

type (
	// gatewayService specifies the interface for the gateway service needed by gatewayResource.
	gatewayService interface {
		Get(rs app.RequestScope, id int) (*models.GatewayDetails, error)
		Query(rs app.RequestScope, offset, limit int) ([]models.GatewayDetails, error)
		Count(rs app.RequestScope) (int, error)
		Create(rs app.RequestScope, model *models.GatewayDetails) (*models.GatewayDetails, error)
		Update(rs app.RequestScope, id int, model *models.GatewayDetails) (*models.GatewayDetails, error)
		Delete(rs app.RequestScope, id int) (*models.GatewayDetails, error)
	}

	// gatewayResource defines the handlers for the CRUD APIs.
	gatewayResource struct {
		service gatewayService
	}
)

// ServeGatewayResource sets up the routing of gateway endpoints and the corresponding handlers.
func ServeGatewayResource(rg *routing.RouteGroup, service gatewayService) {
	r := &gatewayResource{service}
	rg.Get("/gateway/get/<id>", r.get)
	rg.Get("/gateways/list", r.query)
	rg.Post("/gateway/create", r.create)
	rg.Put("/gateway/update/<id>", r.update)
	rg.Delete("/gateway/delete/<id>", r.delete)
	// rg.Get("/sections", r.listSections)
	// rg.Post("/sections", r.createSections)
}

func (r *gatewayResource) get(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	response, err := r.service.Get(app.GetRequestScope(c), id)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *gatewayResource) query(c *routing.Context) error {
	rs := app.GetRequestScope(c)
	count, err := r.service.Count(rs)
	if err != nil {
		return err
	}
	paginatedList := getPaginatedListFromRequest(c, count)
	items, err := r.service.Query(app.GetRequestScope(c), paginatedList.Offset(), paginatedList.Limit())
	if err != nil {
		return err
	}
	paginatedList.Items = items
	return c.Write(paginatedList)
}

func (r *gatewayResource) create(c *routing.Context) error {
	var model models.GatewayDetails
	if err := c.Read(&model); err != nil {
		return err
	}
	response, err := r.service.Create(app.GetRequestScope(c), &model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *gatewayResource) update(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	rs := app.GetRequestScope(c)

	model, err := r.service.Get(rs, id)
	if err != nil {
		return err
	}

	if err := c.Read(model); err != nil {
		return err
	}

	response, err := r.service.Update(rs, id, model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *gatewayResource) delete(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	response, err := r.service.Delete(app.GetRequestScope(c), id)
	if err != nil {
		return err
	}

	return c.Write(response)
}
