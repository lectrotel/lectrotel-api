package apis

import (
	"strconv"

	routing "github.com/go-ozzo/ozzo-routing"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

type (
	// sectionService specifies the interface for the section service needed by sectionResource.
	sectionService interface {
		Get(rs app.RequestScope, id int) (*models.SectionDetails, error)
		Query(rs app.RequestScope, offset, limit int) ([]models.SectionDetails, error)
		Count(rs app.RequestScope) (int, error)
		Create(rs app.RequestScope, model *models.SectionDetails) (*models.SectionDetails, error)
		Update(rs app.RequestScope, id int, model *models.SectionDetails) (*models.SectionDetails, error)
		Delete(rs app.RequestScope, id int) (*models.SectionDetails, error)
	}

	// sectionResource defines the handlers for the CRUD APIs.
	sectionResource struct {
		service sectionService
	}
)

// ServeSectionResource sets up the routing of section endpoints and the corresponding handlers.
func ServeSectionResource(rg *routing.RouteGroup, service sectionService) {
	r := &sectionResource{service}
	rg.Get("/section/get/<id>", r.get)
	rg.Get("/sections/list", r.query)
	rg.Post("/section/create", r.create)
	rg.Put("/section/update/<id>", r.update)
	rg.Delete("/section/delete/<id>", r.delete)
}

func (r *sectionResource) get(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	response, err := r.service.Get(app.GetRequestScope(c), id)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *sectionResource) query(c *routing.Context) error {
	rs := app.GetRequestScope(c)
	count, err := r.service.Count(rs)
	if err != nil {
		return err
	}
	paginatedList := getPaginatedListFromRequest(c, count)
	items, err := r.service.Query(app.GetRequestScope(c), paginatedList.Offset(), paginatedList.Limit())
	if err != nil {
		return err
	}
	paginatedList.Items = items
	return c.Write(paginatedList)
}

func (r *sectionResource) create(c *routing.Context) error {
	var model models.SectionDetails
	if err := c.Read(&model); err != nil {
		return err
	}
	response, err := r.service.Create(app.GetRequestScope(c), &model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *sectionResource) update(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	rs := app.GetRequestScope(c)

	model, err := r.service.Get(rs, id)
	if err != nil {
		return err
	}

	if err := c.Read(model); err != nil {
		return err
	}

	response, err := r.service.Update(rs, id, model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *sectionResource) delete(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	response, err := r.service.Delete(app.GetRequestScope(c), id)
	if err != nil {
		return err
	}

	return c.Write(response)
}
