package apis

import (
	routing "github.com/go-ozzo/ozzo-routing"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

type (
	// dashboardService specifies the interface for the dashboard service needed by dashboardResource.
	dashboardService interface {
		GetUserDashboardStats(rs app.RequestScope) (*models.UserDashboardDetails, error)
	}

	// dashboardResource defines the handlers for the CRUD APIs.
	dashboardResource struct {
		service dashboardService
	}
)

// ServeDashboardResource sets up the routing of dashboard endpoints and the corresponding handlers.
func ServeDashboardResource(rg *routing.RouteGroup, service dashboardService) {
	r := &dashboardResource{service}
	rg.Get("/user-dashboard-stats", r.getUserDashboardStats)
}

func (r *dashboardResource) getUserDashboardStats(c *routing.Context) error {
	response, err := r.service.GetUserDashboardStats(app.GetRequestScope(c))
	if err != nil {
		return err
	}

	return c.Write(response)
}
