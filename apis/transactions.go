package apis

import (
	"encoding/json"
	"errors"
	"strconv"

	routing "github.com/go-ozzo/ozzo-routing"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

type (
	// transactionService specifies the interface for the transaction service needed by transactionResource.
	transactionService interface {
		AddInvoice(rs app.RequestScope, model *models.Invoices) error
		CountInvoices(rs app.RequestScope, meterno string) (int, error)
		ListInvoices(rs app.RequestScope, offset, limit int, meterno string) ([]models.Invoices, error)
		GetInvoice(rs app.RequestScope, id int) (*models.InvoiceDetails, error)

		Get(rs app.RequestScope, id int) (*models.RequestToken, error)
		GenerateWaterToken(rs app.RequestScope, model *models.RequestToken) (models.ResponseData, error)
		CountTokens(rs app.RequestScope, meterno string) (int, error)
		GetTokenList(rs app.RequestScope, offset, limit int, meterno string) ([]models.ListTokens, error)
		CountPayments(rs app.RequestScope, meterno string) (int, error)
		GetPaymentsList(rs app.RequestScope, offset, limit int, meterno string) ([]models.C2BResponse, error)
		SaveCashPostPaidPayments(rs app.RequestScope, model *models.TransInvoices) error
		SaveMpesaPostPaidPayments(rs app.RequestScope, model *models.TransInvoices) error

		GetPaymentDetails(rs app.RequestScope, transid, meterno string) (models.PaymentDetails, error)
		SaveTransaction(rs app.RequestScope, model *models.C2BResponse) error
		RegisterURL(rs app.RequestScope, model models.C2BRegisterURL) (string, error)
		SimulateMpesaTransaction(rs app.RequestScope, model models.C2B) (models.C2BSimulateResponse, error)
		TokensTopup(rs app.RequestScope, model *models.TokenTopupJob) (bool, error)
		ElecTokensMpesaPurchase(rs app.RequestScope, model *models.TransInvoices) (models.ResponseData, error)
		ElecTokensCashPurchase(rs app.RequestScope, model *models.TransInvoices) (models.ResponseData, error)
		ElecPostPaidCashPurchase(rs app.RequestScope, model *models.TransInvoices) (models.ResponseData, error)
		WaterMpesaTokensPurchase(rs app.RequestScope, model *models.RequestToken) (models.ResponseData, error)
	}

	// transactionResource defines the handlers for the CRUD APIs.
	transactionResource struct {
		service transactionService
	}
)

// ServeTransactionResource sets up the routing of transaction endpoints and the corresponding handlers.
func ServeTransactionResource(rg *routing.RouteGroup, service transactionService) {
	r := &transactionResource{service}
	rg.Get("/invoices/list", r.listInvoices)
	rg.Get("/invoice/get/<id>", r.getInvoice)
	rg.Post("/invoice/generate", r.AddInvoice)

	rg.Get("/tokens", r.getTokenList)
	rg.Get("/payments", r.getPaymentsList)
	rg.Post("/payments", r.savePayments)
	rg.Get("/payment/details", r.getPaymentDetails)
	rg.Post("/registerurl", r.registerURL)
	rg.Post("/simulatempesa", r.simulateMpesa)
	rg.Post("/tokens/topup", r.tokensTopup)
	rg.Post("/tokens/purchase/prepaid/elec", r.elecTokensPurchase)
	rg.Post("/tokens/purchase/prepaid/water", r.generateWaterToken)
	// rg.Post("/tokens/purchase/autotopup/elec", r.elecAutoTopupPurchase)
	rg.To("GET,HEAD,POST", "/confirmation", r.confirmationURL)
	rg.To("GET,HEAD,POST", "/validation", r.validationURL)
}

func (r *transactionResource) getTokenList(c *routing.Context) error {
	meterno := c.Query("meter_number", "0")

	rs := app.GetRequestScope(c)
	count, err := r.service.CountTokens(rs, meterno)
	if err != nil {
		return err
	}
	paginatedList := getPaginatedListFromRequest(c, count)
	items, err := r.service.GetTokenList(app.GetRequestScope(c), paginatedList.Offset(), paginatedList.Limit(), meterno)
	if err != nil {
		return err
	}
	paginatedList.Items = items
	return c.Write(paginatedList)
}

func (r *transactionResource) getPaymentsList(c *routing.Context) error {
	meterno := c.Query("meter_id", "0")

	rs := app.GetRequestScope(c)
	count, err := r.service.CountPayments(rs, meterno)
	if err != nil {
		return err
	}
	paginatedList := getPaginatedListFromRequest(c, count)
	items, err := r.service.GetPaymentsList(app.GetRequestScope(c), paginatedList.Offset(), paginatedList.Limit(), meterno)
	if err != nil {
		return err
	}
	paginatedList.Items = items
	return c.Write(paginatedList)
}

func (r *transactionResource) getPaymentDetails(c *routing.Context) error {
	transid := c.Query("trans_id", "0")
	if transid == "0" {
		return errors.New("Transaction ID is required")
	}

	meterno := c.Query("meter", "0")
	if meterno == "0" {
		return errors.New("Meter Number is required")
	}

	details, err := r.service.GetPaymentDetails(app.GetRequestScope(c), transid, meterno)
	if err != nil {
		return err
	}

	return c.Write(details)
}

func (r *transactionResource) savePayments(c *routing.Context) error {
	var model models.TransInvoices
	if err := c.Read(&model); err != nil {
		return err
	}

	if err := model.Validate(); err != nil {
		return err
	}

	if model.PaymentOption == "Mpesa" {
		err := r.service.SaveMpesaPostPaidPayments(app.GetRequestScope(c), &model)
		if err != nil {
			return err
		}
	} else {
		err := r.service.SaveCashPostPaidPayments(app.GetRequestScope(c), &model)
		if err != nil {
			return err
		}
	}

	return c.Write(struct {
		Response bool `json:"response"`
	}{true})

}

// tokensPurchase
func (r *transactionResource) elecTokensPurchase(c *routing.Context) error {
	var model models.TransInvoices
	if err := c.Read(&model); err != nil {
		return err
	}

	var response models.ResponseData
	var err error
	if model.PaymentOption == "Cash" {
		response, err = r.service.ElecTokensCashPurchase(app.GetRequestScope(c), &model)
	} else if model.PaymentOption == "Mpesa" {
		response, err = r.service.ElecTokensMpesaPurchase(app.GetRequestScope(c), &model)
	}
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *transactionResource) elecAutoTopupPurchase(c *routing.Context) error {
	var model models.TransInvoices
	if err := c.Read(&model); err != nil {
		return err
	}

	var response models.ResponseData
	var err error
	if model.PaymentOption == "Cash" {
		response, err = r.service.ElecPostPaidCashPurchase(app.GetRequestScope(c), &model)
	} else if model.PaymentOption == "Mpesa" {
		response, err = r.service.ElecTokensMpesaPurchase(app.GetRequestScope(c), &model)
	}
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *transactionResource) generateWaterToken(c *routing.Context) error {
	var model models.RequestToken
	if err := c.Read(&model); err != nil {
		return err
	}

	if model.PaymentOption == "Cash" {
		response, err := r.service.GenerateWaterToken(app.GetRequestScope(c), &model)
		if err != nil {
			return err
		}
		return c.Write(response)
	} else if model.PaymentOption == "Mpesa" {
		// validate struct
		if err := model.ValidateTokenRequest(); err != nil {
			return err
		}
		response, err := r.service.WaterMpesaTokensPurchase(app.GetRequestScope(c), &model)
		if err != nil {
			return err
		}
		return c.Write(response)
	}
	return nil
}

// Send data to topup tokens
func (r *transactionResource) tokensTopup(c *routing.Context) error {
	var model models.TokenTopup
	if err := c.Read(&model); err != nil {
		return err
	}
	var ti models.TransInvoices
	m := models.TokenTopupJob{
		TokenTopup:       model,
		TransInvoices:    ti,
		UnitsTotalAmount: 0,
	}
	response, err := r.service.TokensTopup(app.GetRequestScope(c), &m)
	if err != nil {
		return err
	}

	return c.Write(response)
}

// Register C2B Confirmation and Validation URLs
func (r *transactionResource) registerURL(c *routing.Context) error {
	var model models.C2BRegisterURL
	if err := c.Read(&model); err != nil {
		return err
	}
	response, err := r.service.RegisterURL(app.GetRequestScope(c), model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

// simulateMpesa simulate C2B transaction
func (r *transactionResource) simulateMpesa(c *routing.Context) error {
	var model models.C2B
	if err := c.Read(&model); err != nil {
		return err
	}
	response, err := r.service.SimulateMpesaTransaction(app.GetRequestScope(c), model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

// ValidationURL ...
func (r *transactionResource) validationURL(c *routing.Context) error {
	return c.Write(successResponse())
}

// ConfirmationURL ...
func (r *transactionResource) confirmationURL(c *routing.Context) error {
	var model string
	if err := c.Read(&model); err != nil {
		return err
	}

	var m models.C2BResponse
	err := json.Unmarshal([]byte(model), &m)
	if err != nil {
		return err
	}

	// save transaction
	err = r.service.SaveTransaction(app.GetRequestScope(c), &m)
	if err != nil {
		return err
	}

	// generate tokens
	billRefNumber, _ := strconv.Atoi(m.BillRefNumber)
	transAmount, _ := strconv.ParseFloat(m.TransAmount, 64)
	var tokenModel = models.RequestToken{
		MeterID: int32(billRefNumber),
		// TokenType: "CreditTokenT1",
		Amount:    transAmount,
		ToNumber:  "+254723436438",
		PaymentID: m.TransID,
	}
	r.service.GenerateWaterToken(app.GetRequestScope(c), &tokenModel)

	return c.Write(successResponse())
}

func successResponse() *models.VResponse {
	return &models.VResponse{
		ResultCode: 0,
		ResultDesc: "Completed",
	}
}

func erorResponse() *models.VResponse {
	return &models.VResponse{
		ResultCode: 1,
		ResultDesc: "Rejected",
	}
}
