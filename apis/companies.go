package apis

import (
	"strconv"

	routing "github.com/go-ozzo/ozzo-routing"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/errors"
	"github.com/lectrotel-api/models"
)

type (
	// companyService specifies the interface for the company service needed by companyResource.
	companyService interface {
		Get(rs app.RequestScope, id int) (*models.Companies, error)
		Query(rs app.RequestScope, offset, limit int) ([]models.Companies, error)
		Count(rs app.RequestScope) (int, error)
		Create(rs app.RequestScope, model *models.Companies) (*models.Companies, error)
		Update(rs app.RequestScope, id int, model *models.Companies) (*models.Companies, error)
		Delete(rs app.RequestScope, id int) (*models.Companies, error)
	}

	// companyResource defines the handlers for the CRUD APIs.
	companyResource struct {
		service companyService
	}
)

// ServeCompanyResource sets up the routing of company endpoints and the corresponding handlers.
func ServeCompanyResource(rg *routing.RouteGroup, service companyService) {
	r := &companyResource{service}
	rg.Get("/company/get/<id>", r.get)
	rg.Get("/companies", r.query)
	rg.Post("/company/add", r.create)
	rg.Put("/companies/<id>", r.update)
	rg.Delete("/companies/<id>", r.delete)
}

func (r *companyResource) get(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	response, err := r.service.Get(app.GetRequestScope(c), id)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *companyResource) query(c *routing.Context) error {
	rs := app.GetRequestScope(c)
	count, err := r.service.Count(rs)
	if err != nil {
		return err
	}
	paginatedList := getPaginatedListFromRequest(c, count)
	if count > 0 {
		items, err := r.service.Query(app.GetRequestScope(c), paginatedList.Offset(), paginatedList.Limit())
		if err != nil {
			return err
		}
		paginatedList.Items = items
	} else {
		return errors.NoContentFound("No content found")
	}

	return c.Write(paginatedList)
}

func (r *companyResource) create(c *routing.Context) error {
	var model models.Companies
	if err := c.Read(&model); err != nil {
		return err
	}
	response, err := r.service.Create(app.GetRequestScope(c), &model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *companyResource) update(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	rs := app.GetRequestScope(c)

	model, err := r.service.Get(rs, id)
	if err != nil {
		return err
	}

	if err := c.Read(model); err != nil {
		return err
	}

	response, err := r.service.Update(rs, id, model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *companyResource) delete(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	response, err := r.service.Delete(app.GetRequestScope(c), id)
	if err != nil {
		return err
	}

	return c.Write(response)
}
