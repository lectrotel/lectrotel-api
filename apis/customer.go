package apis

import (
	"strconv"

	routing "github.com/go-ozzo/ozzo-routing"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
)

type (
	// customerService specifies the interface for the customer service needed by customerResource.
	customerService interface {
		Get(rs app.RequestScope, id int) (*models.Customers, error)
		Query(rs app.RequestScope, offset, limit int) ([]models.Customers, error)
		Count(rs app.RequestScope) (int, error)
		Create(rs app.RequestScope, model *models.Customers) (*models.Customers, error)
		Update(rs app.RequestScope, id int, model *models.Customers) (*models.Customers, error)
		Delete(rs app.RequestScope, id int) (*models.Customers, error)
	}

	// customerResource defines the handlers for the CRUD APIs.
	customerResource struct {
		service customerService
	}
)

// ServeCustomerResource sets up the routing of customer endpoints and the corresponding handlers.
func ServeCustomerResource(rg *routing.RouteGroup, service customerService) {
	r := &customerResource{service}
	rg.Get("/customers/<id>", r.get)
	rg.Get("/customers", r.query)
	rg.Post("/customer", r.create)
	rg.Put("/customers/<id>", r.update)
	rg.Delete("/customers/<id>", r.delete)
}

func (r *customerResource) get(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	response, err := r.service.Get(app.GetRequestScope(c), id)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *customerResource) query(c *routing.Context) error {
	rs := app.GetRequestScope(c)
	count, err := r.service.Count(rs)
	if err != nil {
		return err
	}
	paginatedList := getPaginatedListFromRequest(c, count)
	items, err := r.service.Query(app.GetRequestScope(c), paginatedList.Offset(), paginatedList.Limit())
	if err != nil {
		return err
	}
	paginatedList.Items = items
	return c.Write(paginatedList)
}

func (r *customerResource) create(c *routing.Context) error {
	var model models.Customers
	if err := c.Read(&model); err != nil {
		return err
	}
	response, err := r.service.Create(app.GetRequestScope(c), &model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *customerResource) update(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	rs := app.GetRequestScope(c)

	model, err := r.service.Get(rs, id)
	if err != nil {
		return err
	}

	if err := c.Read(model); err != nil {
		return err
	}

	response, err := r.service.Update(rs, id, model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *customerResource) delete(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	response, err := r.service.Delete(app.GetRequestScope(c), id)
	if err != nil {
		return err
	}

	return c.Write(response)
}
