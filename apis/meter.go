package apis

import (
	"errors"
	"strconv"

	routing "github.com/go-ozzo/ozzo-routing"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/models"
	"go.mongodb.org/mongo-driver/mongo"
)

type (
	// meterService specifies the interface for the meter service needed by meterResource.
	meterService interface {
		Get(rs app.RequestScope, id int) (*models.MeterDetails, error)
		GetMeterDetails(rs app.RequestScope, meternumber string) (*models.MeterDetails, error)
		Query(rs app.RequestScope, offset, limit int) ([]models.MeterDetails, error)
		Count(rs app.RequestScope) (int, error)
		Create(rs app.RequestScope, model *models.MeterDetails) (*models.MeterDetails, error)
		Update(rs app.RequestScope, id int, model *models.MeterDetails) (*models.MeterDetails, error)
		Delete(rs app.RequestScope, id int) (*models.MeterDetails, error)
		GetMeterLogs(db *mongo.Database, id int, offset, limit int) ([]models.DeviceData, error)
	}

	// meterResource defines the handlers for the CRUD APIs.
	meterResource struct {
		service meterService
	}
)

// ServeMeterResource sets up the routing of meter endpoints and the corresponding handlers.
func ServeMeterResource(rg *routing.RouteGroup, service meterService) {
	r := &meterResource{service}
	rg.Get("/meter/get/<id>", r.get)
	rg.Get("/meter/details", r.getMeterDetails)
	rg.Get("/meters", r.query)
	rg.Post("/meter", r.create)
	rg.Put("/meter/<id>", r.update)
	rg.Delete("/meter/<id>", r.delete)
	rg.Get("/meter/logs/<id>", r.getLogs)
	// rg.Get("/sections", r.listSections)
	// rg.Post("/sections", r.createSections)
}

func (r *meterResource) get(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	response, err := r.service.Get(app.GetRequestScope(c), id)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *meterResource) getMeterDetails(c *routing.Context) error {
	meternumber := c.Query("number", "0")
	if meternumber == "0" {
		return errors.New("Meter number is required")
	}

	response, err := r.service.GetMeterDetails(app.GetRequestScope(c), meternumber)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *meterResource) query(c *routing.Context) error {
	rs := app.GetRequestScope(c)
	count, err := r.service.Count(rs)
	if err != nil {
		return err
	}
	paginatedList := getPaginatedListFromRequest(c, count)
	items, err := r.service.Query(app.GetRequestScope(c), paginatedList.Offset(), paginatedList.Limit())
	if err != nil {
		return err
	}
	paginatedList.Items = items
	return c.Write(paginatedList)
}

func (r *meterResource) create(c *routing.Context) error {
	var model models.MeterDetails
	if err := c.Read(&model); err != nil {
		return err
	}
	response, err := r.service.Create(app.GetRequestScope(c), &model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *meterResource) update(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	rs := app.GetRequestScope(c)

	model, err := r.service.Get(rs, id)
	if err != nil {
		return err
	}

	if err := c.Read(model); err != nil {
		return err
	}

	response, err := r.service.Update(rs, id, model)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *meterResource) delete(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	response, err := r.service.Delete(app.GetRequestScope(c), id)
	if err != nil {
		return err
	}

	return c.Write(response)
}

func (r *meterResource) getLogs(c *routing.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	items, err := r.service.GetMeterLogs(app.MongoDB, id, 0, 25)
	if err != nil {
		return err
	}

	return c.Write(items)

}
