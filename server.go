package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/Sirupsen/logrus"
	dbx "github.com/go-ozzo/ozzo-dbx"
	routing "github.com/go-ozzo/ozzo-routing"
	"github.com/go-ozzo/ozzo-routing/auth"
	"github.com/go-ozzo/ozzo-routing/content"
	"github.com/go-ozzo/ozzo-routing/cors"
	_ "github.com/go-sql-driver/mysql"
	"github.com/lectrotel-api/apis"
	"github.com/lectrotel-api/app"
	"github.com/lectrotel-api/daos"
	"github.com/lectrotel-api/errors"
	"github.com/lectrotel-api/services"
)

func main() {
	// load application configurations
	if err := app.LoadConfig("./config"); err != nil {
		panic(fmt.Errorf("Invalid application configuration: %s", err))
	}

	// load error messages
	if err := errors.LoadMessages(app.Config.ErrorFile); err != nil {
		panic(fmt.Errorf("Failed to read the error message file: %s", err))
	}

	// create the logger
	logger := logrus.New()

	// connect to the database
	db := app.InitializeDB()
	app.MongoDB = app.InitializeMongoDB(logger)
	db.LogFunc = logger.Infof

	// initialize mqtt server
	go func() {
		app.SubscriptionMQTTServer()
	}()

	// wire up API routing
	http.Handle("/", buildRouter(logger, db))

	// start the server
	address := fmt.Sprintf(":%v", app.Config.ServerPort)
	// panic(http.ListenAndServe(address, nil))

	//  Start HTTP
	go func() {
		panic(http.ListenAndServe(address, nil))
	}()

	// Create a CA certificate pool and add cert.pem to it
	caCert, err := ioutil.ReadFile("cert.pem")
	if err != nil {
		log.Fatal(err)
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	// Create the TLS Config with the CA pool and enable Client certificate validation
	tlsConfig := &tls.Config{
		ClientCAs: caCertPool,
		// ClientAuth: tls.RequireAndVerifyClientCert,
	}
	tlsConfig.BuildNameToCertificate()

	// Create a Server instance to listen on port 8443 with the TLS config
	httpsaddress := fmt.Sprintf(":%v", app.Config.ServerPortHTTPS)
	server := &http.Server{
		Addr: httpsaddress,
		// TLSConfig: tlsConfig,
		Handler: buildRouter(logger, db),
	}

	// Listen to HTTPS connections with the server certificate and wait
	log.Fatal(server.ListenAndServeTLS("cert.pem", "key.pem"))
	logger.Infof("server %v is started at %v (http) and %v (https)\n", app.Version, address, httpsaddress)

}

func buildRouter(logger *logrus.Logger, db *dbx.DB) *routing.Router {
	router := routing.New()

	router.To("GET,HEAD", "/ping", func(c *routing.Context) error {
		c.Abort() // skip all other middlewares/handlers
		return c.Write("OK " + app.Version)
	})

	router.Use(
		app.Init(logger),
		content.TypeNegotiator(content.JSON),
		cors.Handler(cors.Options{
			AllowOrigins: "*",
			AllowHeaders: "*",
			AllowMethods: "*",
		}),
		app.Transactional(db),
	)

	rg := router.Group("/api/v" + app.Version)

	userDAO := daos.NewUserDAO()
	apis.ServeUserResource(rg, services.NewUserService(userDAO))

	// rg.Post("/auth", apis.Auth(app.Config.JWTSigningKey))
	rg.Use(auth.JWT(app.Config.JWTVerificationKey, auth.JWTOptions{
		SigningMethod: app.Config.JWTSigningMethod,
		TokenHandler:  apis.JWTHandler,
	}))

	dashboardDAO := daos.NewDashboardDAO()
	apis.ServeDashboardResource(rg, services.NewDashboardService(dashboardDAO))

	customerDAO := daos.NewCustomerDAO()
	apis.ServeCustomerResource(rg, services.NewCustomerService(customerDAO))

	companyDAO := daos.NewCompanyDAO()
	apis.ServeCompanyResource(rg, services.NewCompanyService(companyDAO))

	permissionDAO := daos.NewPermissionDAO()
	apis.ServePermissionResource(rg, services.NewPermissionService(permissionDAO))

	meterDAO := daos.NewMeterDAO()
	apis.ServeMeterResource(rg, services.NewMeterService(meterDAO))

	transactionDAO := daos.NewTransactionDAO()
	apis.ServeTransactionResource(rg, services.NewTransactionService(transactionDAO))

	settingDAO := daos.NewSettingDAO()
	apis.ServeSettingResource(rg, services.NewSettingService(settingDAO))

	sectionDAO := daos.NewSectionDAO()
	apis.ServeSectionResource(rg, services.NewSectionService(sectionDAO))

	gatewayDAO := daos.NewGatewayDAO()
	apis.ServeGatewayResource(rg, services.NewGatewayService(gatewayDAO))

	waterDAO := daos.NewWaterDAO()
	apis.ServeWaterResource(rg, services.NewWaterService(waterDAO))

	// wire up more resource APIs here

	return router
}
